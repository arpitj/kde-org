<ul>

<!-- ASP LINUX -->
<!--
<li><a href="http://www.asplinux.ru/">ASP Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/ASPLinux/README">README</a>)
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/ASPLinux/9.2/noarch/">Language
        packages</a> (all versions and architectures)</li>
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/ASPLinux/9.2/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- CONECTIVA LINUX -->
<!--<li><a href="http://www.conectiva.com/">Conectiva Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Conectiva/README">README</a>
  ) :
  <ul type="disc">
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Conectiva/CL9/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Conectiva/CL9/SRPMS.kdeorg/">SRPMs</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--  DEBIAN -->
<!--
<li><a href="http://www.debian.org/">Debian</a>
  (<a href="http://download.kde.org/stable/3.2.3/Debian/README">README</a>) :
    <ul type="disc">
      <li>
         Debian stable (woody) (Intel i386) : <tt>deb http://download.kde.org/stable/3.2.3/Debian stable main</tt>
      </li>
    </ul>
  <p />
</li>
-->

<!--   FREEBSD -->
<li><a href="http://www.freebsd.org/">FreeBSD</a>
  (<a href="http://download.kde.org/stable/3.2.3/FreeBSD/README">README</a>)
  <p />
</li>

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>
<!--  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Mandrake/README">README</a>)-->
  :
  <ul type="disc">
    <li>
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Mandrake/RPMS/">Intel
      i586</a> and <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Mandrake/SRPMS/">SRPMs</a>
    </li>
  </ul>
  <p />
</li>

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/RedHat/Fedora2/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      Fedora 2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/RedHat/Fedora2/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- KDE RedHat -->
<li>
 <a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>:
 <ul type="disc">
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/7.3/">Red Hat 7.3</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/8.0/">Red Hat 8.0</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/9/">Red Hat 9</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/1/">Fedora Core 1</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/2/">Fedora Core 2</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/3.0/">Red Hat Enterprise 3</a>
 </li>
 </ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/Slackware/noarch/">Language
        packages</a> (all versions and architectures)
     </li>
    <li>
       9.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/Slackware/9.1/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- AIX -->
<!--
<li>
  <a href="http://www-1.ibm.com/servers/aix/">IBM AIX</a> (Not IBM endorsed):
   <ul type="disc">
     <li>
       5.1+: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/AIX/5.1/">PowerPC</a>
     </li>
   </ul>
  <p />
</li>
-->

<!-- SOLARIS -->
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/Solaris/9/kde-i18n/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/Solaris/9/">Sparc</a>
    </li>
  </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/README">README</a>)
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/ix86/9.1/">Intel
      i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/ix86/9.0/">Intel
      i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>

<!-- TURBO LINUX -->
<!--
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
  <p />
</li>
-->

<!--   TRU64 -->
<!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/YellowDog">2.2</a>
-->

<!-- YOPER LINUX -->
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/yoper/">Intel i686 rpm</a>
    </li>
  </ul>
  <p />
</li>

<!-- VECTOR LINUX -->
<!--
<li>
  <a href="http://www.vectorlinux.com/">VECTORLINUX</a>:
  (<a href="http://download.kde.org/stable/3.2.3/contrib/VectorLinux/3.0/README">README</a>):
  <ul type="disc">
    <li>
      3.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/VectorLinux/3.2/">Intel i386</a>
    </li>
    <li>
      3.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/contrib/VectorLinux/3.0/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

</ul>
