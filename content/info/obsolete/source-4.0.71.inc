<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdeaccessibility-4.0.71.tar.bz2">kdeaccessibility-4.0.71</a></td><td align="right">6.1MB</td><td><tt>e9eaa49dd613420b5997ea3742efcc11</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdeadmin-4.0.71.tar.bz2">kdeadmin-4.0.71</a></td><td align="right">2.0MB</td><td><tt>a15ead1802dbc1258df1670e0913f358</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdeartwork-4.0.71.tar.bz2">kdeartwork-4.0.71</a></td><td align="right">41MB</td><td><tt>f2be4071f866d55a88b3ae391085d300</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdebase-4.0.71.tar.bz2">kdebase-4.0.71</a></td><td align="right">4.2MB</td><td><tt>a438e0b487e34774462ab0868e4c86b6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdebase-runtime-4.0.71.tar.bz2">kdebase-runtime-4.0.71</a></td><td align="right">48MB</td><td><tt>44b07b1959b7a18299a30236c6c05b68</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdebase-workspace-4.0.71.tar.bz2">kdebase-workspace-4.0.71</a></td><td align="right">29MB</td><td><tt>78d1a18d42ff65184c1d2c9da065414c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdebindings-4.0.71.tar.bz2">kdebindings-4.0.71</a></td><td align="right">3.8MB</td><td><tt>2a22e57cb48264cbf42b20c0cc7798f1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdeedu-4.0.71.tar.bz2">kdeedu-4.0.71</a></td><td align="right">51MB</td><td><tt>116123bb0c930ebf50784d41e8a4d116</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdegames-4.0.71.tar.bz2">kdegames-4.0.71</a></td><td align="right">21MB</td><td><tt>516e2b2603cb54ec3890d9eac38b8a85</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdegraphics-4.0.71.tar.bz2">kdegraphics-4.0.71</a></td><td align="right">2.4MB</td><td><tt>7ff95bbfeb0fa372222b478196c56642</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdelibs-4.0.71.tar.bz2">kdelibs-4.0.71</a></td><td align="right">8.9MB</td><td><tt>64bb00fa4e4c04aded11e549b1f73203</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdemultimedia-4.0.71.tar.bz2">kdemultimedia-4.0.71</a></td><td align="right">1.4MB</td><td><tt>5947d1891296394e7b470a8898e974f0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdenetwork-4.0.71.tar.bz2">kdenetwork-4.0.71</a></td><td align="right">7.0MB</td><td><tt>739e5c4415e3d13a1855a64bb765094f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdepim-4.0.71.tar.bz2">kdepim-4.0.71</a></td><td align="right">13MB</td><td><tt>257d4f7b41cb7a935b2d1b0df066754a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdepimlibs-4.0.71.tar.bz2">kdepimlibs-4.0.71</a></td><td align="right">1.8MB</td><td><tt>e18890fda7c0a4844eeec544f90b1ccf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdesdk-4.0.71.tar.bz2">kdesdk-4.0.71</a></td><td align="right">4.3MB</td><td><tt>f71e46e1f02372fc43e5763893728ebb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdetoys-4.0.71.tar.bz2">kdetoys-4.0.71</a></td><td align="right">4.2MB</td><td><tt>696943828ab33298eead10b5d8e14771</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdeutils-4.0.71.tar.bz2">kdeutils-4.0.71</a></td><td align="right">2.4MB</td><td><tt>b8b228f5fc1f2e037a5d63747502a8d0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdevelop-4.0.71.tar.bz2">kdevelop-4.0.71</a></td><td align="right">2.4MB</td><td><tt>96a88b2ac6a77931062618892db5cf75</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdevplatform-4.0.71.tar.bz2">kdevplatform-4.0.71</a></td><td align="right">588KB</td><td><tt>212ce8a81ce980b1ab7faa36b32f5ce0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.71/src/kdewebdev-4.0.71.tar.bz2">kdewebdev-4.0.71</a></td><td align="right">2.4MB</td><td><tt>4fcb28837dc25ac3e76388f0cd79beac</tt></td></tr>
</table>
