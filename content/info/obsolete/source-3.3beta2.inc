<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/arts-1.2.92.tar.bz2">arts-1.2.92</a></td>
   <td align="right">956kB</td>
   <td><tt>16f828a3c182475ba69744e18e888992</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdeaccessibility-3.2.92.tar.bz2">kdeaccessibility-3.2.92</a></td>
   <td align="right">1.6MB</td>
   <td><tt>41a278b93f673ae649f98abf183a5e45</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdeaddons-3.2.92.tar.bz2">kdeaddons-3.2.92</a></td>
   <td align="right">1.9MB</td>
   <td><tt>93c084a369335e99e26472287eedeaac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdeadmin-3.2.92.tar.bz2">kdeadmin-3.2.92</a></td>
   <td align="right">2.0MB</td>
   <td><tt>a1e39191d3e6a824f870d6c12969f7aa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdeartwork-3.2.92.tar.bz2">kdeartwork-3.2.92</a></td>
   <td align="right">17MB</td>
   <td><tt>046e6683f63880b500f16d97efc568cb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdebase-3.2.92.tar.bz2">kdebase-3.2.92</a></td>
   <td align="right">17MB</td>
   <td><tt>550f3e52c733c842f645f0480cedc60e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdebindings-3.2.92.tar.bz2">kdebindings-3.2.92</a></td>
   <td align="right">7.2MB</td>
   <td><tt>f2fdd41c166b237df036fc6b2070e5a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdeedu-3.2.92.tar.bz2">kdeedu-3.2.92</a></td>
   <td align="right">21MB</td>
   <td><tt>5ba7306c65f1dcece41dca94e4f13e5c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdegames-3.2.92.tar.bz2">kdegames-3.2.92</a></td>
   <td align="right">9.5MB</td>
   <td><tt>9c80de6ebd27fc975e9822ef4e6fc763</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdegraphics-3.2.92.tar.bz2">kdegraphics-3.2.92</a></td>
   <td align="right">6.3MB</td>
   <td><tt>80362e37d3d076b08d3b53c0ac908bb2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kde-i18n-3.2.92.tar.bz2">kde-i18n-3.2.92</a></td>
   <td align="right">178MB</td>
   <td><tt>ca9862c00c81266bd84b4d0bd4a848fc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdelibs-3.2.92.tar.bz2">kdelibs-3.2.92</a></td>
   <td align="right">15MB</td>
   <td><tt>0f4a20ef8c38f3300fba5d1624cca0c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdemultimedia-3.2.92.tar.bz2">kdemultimedia-3.2.92</a></td>
   <td align="right">5.5MB</td>
   <td><tt>226cd47b41097d3503d2f4aa76a4ec6d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdenetwork-3.2.92.tar.bz2">kdenetwork-3.2.92</a></td>
   <td align="right">7.1MB</td>
   <td><tt>5174eecb675328350334b539f19b906a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdepim-3.2.92.tar.bz2">kdepim-3.2.92</a></td>
   <td align="right">9.1MB</td>
   <td><tt>2bd3c4404947be37510fa349673dd0b1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdesdk-3.2.92.tar.bz2">kdesdk-3.2.92</a></td>
   <td align="right">4.6MB</td>
   <td><tt>96a9ea71cf65da05aa5c4147ec3e55f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdetoys-3.2.92.tar.bz2">kdetoys-3.2.92</a></td>
   <td align="right">3.1MB</td>
   <td><tt>6047cb7930782fd02c9f13d9636e6eee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdeutils-3.2.92.tar.bz2">kdeutils-3.2.92</a></td>
   <td align="right">2.6MB</td>
   <td><tt>394b104c6d701ef9731e07ddbf0c52f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdevelop-3.0.92.tar.bz2">kdevelop-3.0.92</a></td>
   <td align="right">8.0MB</td>
   <td><tt>097b841c2ab1f0355b056ea1a49e4da7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.92/src/kdewebdev-3.2.92.tar.bz2">kdewebdev-3.2.92</a></td>
   <td align="right">4.9MB</td>
   <td><tt>eb6c0e00d02339ace62f577811bb992f</tt></td>
</tr>

</table>
