---
version: "5.2.2"
title: "Plasma 5.2.2 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.2_Errata
    name: 5.2 Errata
type: info/plasma5
---

This is a bugfix release of Plasma, featuring Plasma Desktop and
other essential software for your desktop computer.  Details in the <a
href="/announcements/plasma-5.2.2">Plasma 5.2.2 announcement</a>.

For an overview of the differences from Plasma 4 read the initial <a
href="/announcements/plasma5.0/">Plasma 5.0 announcement</a>.
