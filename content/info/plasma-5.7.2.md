---
version: "5.7.2"
title: "KDE Plasma 5.7.2, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.7_Errata
    name: 5.è Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: 506e6d4dd4d5088
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the
[Plasma 5.7.2 announcement](/announcements/plasma-5.7.2).

