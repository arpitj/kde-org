KDE Project Security Advisory
=============================

Title:           kdesu: Displayed command truncated by unicode string terminator
Risk Rating:     Important
CVE:             CVE-2016-7787
Versions:        kde-cli-tools < 5.7.5
Author:          Albert Astals Cid aacid@kde.org
Date:            30 September 2016

Overview
========

A maliciously crafted command line for kdesu can result in the user
only seeing part of the commands that will actually get executed as super user.

Impact
======

Users can unwillingly run commands as root.

Workaround
==========

Users should be careful when running kdesu with a command line they have not written themselves.

Solution
========

kde-cli-tools 5.7.5, released as part of KDE Plasma does not allow the
execution of commands with such characters.

Alternatively, commit 5eda179a099ba68a20dc21dc0da63e85a565a171 in kde-cli-tools.git
can be applied to previous releases.

Thanks to Fabian Vogt for reporting this issue.
Thanks to Martin Sandsmark for fixing this issue.

