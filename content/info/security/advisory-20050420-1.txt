-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Kommander untrusted code execution
Original Release Date: 2005-04-20
URL: http://www.kde.org/info/security/advisory-20050420-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0754

1. Systems affected:

        Quanta 3.1.x, KDE 3.2 and new up to including KDE 3.4.0.


2. Overview:

        Kommander is a visual editor and interpreter to edit and
        interpret visual dialogs and execute scripts attached to
        dialog actions. 

        Kommander executes without user confirmation data files
        from possibly untrusted locations. As they contain 
        scripts, the user might accidentally run arbitrary code.


3. Impact:

        Remotly supplied kommander files from untrusted sources
        are executed without confirmation. 


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.4.0 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        c388b21d91c8326fc9757cd8786713db  post-3.4.0-kdewebdev-kommander.diff

        A patch for KDE 3.3.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        d210c07121c1ba3a97660a6e166738e6  post-3.3.2-kdewebdev-kommander.diff


6. Time line and credits:

        13/03/2005 Notification of KDE security by Eckhart Wörner
        20/04/2005 Public Disclosure


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFCaDuGvsXr+iuy1UoRAkcnAKCYcVj8QTLzJzDv7EARsmxvqzmgjACgu5c7
IhPMjvATQUIHdQev3Pj9Db4=
=xneq
-----END PGP SIGNATURE-----
