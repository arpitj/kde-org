-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: kpdf/xpdf heap based buffer overflow
Original Release Date: 2006-02-02
URL: http://www.kde.org/info/security/advisory-20060202-1.txt

0. References
        CVE-2006-0301


1. Systems affected:

        KDE 3.3.0 up to including KDE 3.5.1


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        a heap based buffer overflow in the splash rasterizer engine
        that can crash kpdf or even execute arbitrary code.


3. Impact:

        Remotely supplied pdf files can be used to execute arbitrary
	code on the client machine.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.4.3 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        bc7dc2a5235f95a41fc1d7ab885899da  post-3.5.1-kdegraphics-CVE-2006-0301.diff

        Patch for KDE 3.3.0 - 3.4.3 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        ebbce0a49537b694932b3c0efcf18261  post-3.4.3-kdegraphics-CVE-2006-0301.diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFD53blvsXr+iuy1UoRApn4AKDIoOSczo3D62U3W5+TpQL4aBJoWwCeO+6m
HLqVJB16IdFBrifGxdbSp74=
=6CKh
-----END PGP SIGNATURE-----
