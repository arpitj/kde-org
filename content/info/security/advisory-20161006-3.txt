KDE Project Security Advisory
=============================

Title:          KMail: JavaScript execution in HTML Mails
Risk Rating:    Normal
CVE:            CVE-2016-7968
Platforms:      All
Versions:       kmail 5.3.0
Author:         Andre Heinecke <aheinecke@intevation.de>
Date:           6 October 2016

Overview
========

KMail since version 5.3.0 used a QWebEngine based viewer
that had JavaScript enabled. HTML Mail contents were not sanitized for
JavaScript and included code was executed.

Impact
======

An unauthenticated attacker can send out mails with Javascript to manipulate
the display of messages. The JavaScript executed might be used as an entry
point for further exploits.

Workaround
==========

Assuming a version with CVE-2016-7966 fixed a user is protected
from this by only viewing plain text mails.

Solution
========

The full solution disables JavaScript in the Mailviewer of KMail. This
requires API introduced in Qt 5.7.0 so KMail needs to be built with
Qt 5.7.0 and the following patch:
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=f601f9ffb706f7d3a5893b04f067a1f75da62c99

For versions previous to 5.7.0 the following patches partly sanitize mails
but still make it possible to inject code:
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=3503b75e9c79c3861e182588a0737baf165abd23
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=a8744798dfdf8e41dd6a378e48662c66302b0019
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=77976584a4ed2797437a2423704abdd7ece7834a
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=fb1be09360c812d24355076da544030a67b736fc
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=0402c17a8ead92188971cb604d905b3072d56a73

Credits
=======

Thanks to Roland Tapken for reporting this issue, Andre Heinecke from
Intevation GmbH for analysing and the problems and reviewing the fix
and Laurent Montel for fixing the issues.
