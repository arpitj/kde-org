---
aliases:
- ../../fulllog_releases-20.12.3
title: Release Service 20.12.3 Full Log Page
type: fulllog
version: 20.12.3
release: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ MySQL server settings: Make use of 'loose_' option prefix. [Commit.](http://commits.kde.org/akonadi/9c666d0d6039a87f6286014c7d9c7281a5bd9dd1) Fixes bug [#421922](https://bugs.kde.org/421922)
+ Fix 37 pixels offset for the context menu when dropping. [Commit.](http://commits.kde.org/akonadi/7b6a8c845af84976cd275bed9fb7b257a9944eae) 
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Switch to an approved license. [Commit.](http://commits.kde.org/calendarsupport/f6d662ef8b5f12c37b5949d3062f7188889b5012) 
+ Find all items that must shift when a new item is placed. [Commit.](http://commits.kde.org/calendarsupport/34aaaa969bb3613a414fbb8da4930eed4d8a85e3) Fixes bug [#64603](https://bugs.kde.org/64603)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix sort by duration to not use string sort. [Commit.](http://commits.kde.org/elisa/3de6de27acdebe9ff08cc8b677dc4243c4237e37) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Use placeHolderText for the priority quick filter. [Commit.](http://commits.kde.org/eventviews/d8d4225ad4910add9de6e06be3fcd3d62be2e1c9) 
+ Fix agenda view's display of end-of-day events. [Commit.](http://commits.kde.org/eventviews/c55d4c54d5624e48adb9197ec184f33b9e2d44cc) 
+ Fix month view's display of end-of-day instances. [Commit.](http://commits.kde.org/eventviews/2324098a8ffcf59d4e88c2c84ba2a288b8e9e783) Fixes bug [#165212](https://bugs.kde.org/165212)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Switch from QGLWidget to QOpenGLWidget. [Commit.](http://commits.kde.org/gwenview/7ed543b53a9fb6d11ef95e439425ed71f952e66b) Fixes bug [#403323](https://bugs.kde.org/403323). Fixes bug [#410083](https://bugs.kde.org/410083). Fixes bug [#420672](https://bugs.kde.org/420672)
+ Use Quality level when saving JPEG images. [Commit.](http://commits.kde.org/gwenview/a401e66621bcffbdc75048d9eaded1a5f5a67137) 
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Fix clazy warnings. [Commit.](http://commits.kde.org/incidenceeditor/f2f1973d255368add0e46d9edc3b715874cbd96e) 
+ Preserve tags when creating instances. [Commit.](http://commits.kde.org/incidenceeditor/e5c62ff698cbeb275f8664dc6821c4380774d0b2) 
{{< /details >}}
{{< details id="juk" title="juk" link="https://commits.kde.org/juk" >}}
+ Filerenamer: Prevent creating track name folders. [Commit.](http://commits.kde.org/juk/276d30f275c52089c2df70e16cebf1c6ceb5edb3) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ If command line action fails, quit application if not previously running. [Commit.](http://commits.kde.org/kalarm/5ca740ebcbd8299c3c53720707385b49dad8e669) 
+ GitLab CI: use qt5.15 docker image. [Commit.](http://commits.kde.org/kalarm/a82408666bb5674288df90065dfafb35324a76ac) 
+ Bug 417108: Fix error creating alarm from command line. [Commit.](http://commits.kde.org/kalarm/05bf384f67c59b6375726ddf53ca4298c095e376) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Don't escape notification title text. [Commit.](http://commits.kde.org/kdeconnect-kde/893ede696038ce52f0c9ada3999b0d40bbe04692) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Remove some debug output. [Commit.](http://commits.kde.org/kdenlive/d948801db962bac3e751bff3a6ed2728d8b63c80) 
+ Fix various selection issues. [Commit.](http://commits.kde.org/kdenlive/5297ef5953a26aaa41d43e54a6ebbbef1a7f2352) 
+ Fix recursive search broken on cancel. [Commit.](http://commits.kde.org/kdenlive/c49d68af077321d097af5e67c9b240ed93292f4c) Fixes bug [#433773](https://bugs.kde.org/433773)
+ Change shortcut for Loop Zone to avoid conflict with windows system. [Commit.](http://commits.kde.org/kdenlive/44a9f4d8842a549ce3377e179ea0e4b32184d01a) 
+ Comment out recent lost timeline focus that made things worse. [Commit.](http://commits.kde.org/kdenlive/3fe09bd05f2de3573c9aa5b67d077e37db7d43d7) 
+ Improve focus handling when switching to fullscreen monitor. [Commit.](http://commits.kde.org/kdenlive/728d2a4ac0386fbc2dab6a643132211c75f893d6) 
+ Do not allow keyframe edit if keyframes are hidden. [Commit.](http://commits.kde.org/kdenlive/ed8eaeef4e6f4bfe9b7cbf68216cdabdc58d6a57) 
+ Ensure we use an UTF-8 encoding for markers. [Commit.](http://commits.kde.org/kdenlive/4c8ef3d52ae4d1dccb0697dfa7d0099b84f5c6c1) See bug [#433615](https://bugs.kde.org/433615)
+ Don't mark document modified when opening a project file with guides. [Commit.](http://commits.kde.org/kdenlive/74fb541eadcfff7da405efc71a7bacc8565d576a) See bug [#433615](https://bugs.kde.org/433615)
+ Fix animated param view when keyframes are hidden. [Commit.](http://commits.kde.org/kdenlive/c7b453ae2b4efd7f09b81d78c645464456d49b77) 
+ Make timeline tracks separator slightly more visible. [Commit.](http://commits.kde.org/kdenlive/dc9e361318a2ad99324482dd4859355e9dfd86cc) 
+ When focusing the app, ensure we have the correct timeline item under mouse referenced. [Commit.](http://commits.kde.org/kdenlive/eaea42c6a73aa37cff48afd48a7a9923d3628761) 
+ Apply !171 (typewriter effect in latest MLT, only for title clips...). [Commit.](http://commits.kde.org/kdenlive/b221dba585eaa4b88b2767fe1eefae32a1a627c6) 
+ Titler: update tab order. [Commit.](http://commits.kde.org/kdenlive/6e6f36e942dd0a710eaa84046115c596725a030b) Fixes bug [#433590](https://bugs.kde.org/433590)
+ Keyframes: Shift+drag now allows selecting last keyframe, fix corruption / crash on group keyframe move. [Commit.](http://commits.kde.org/kdenlive/4be760b98c93cf0813fe464403d4170fc9349a67) 
+ Transcode job: don't silently overwrite exported files. [Commit.](http://commits.kde.org/kdenlive/f1cd744c5237f890bd779f37f73031ed38959199) Fixes bug [#433623](https://bugs.kde.org/433623)
+ Don't enforce profile width multiple of 8. [Commit.](http://commits.kde.org/kdenlive/32c27894850512ab3e195ef1904317b3d730ff27) 
+ Profile width in MLT can be a multiple of 2, not 8. [Commit.](http://commits.kde.org/kdenlive/c320c2f1184abcf05cf3389a1c0ce27ce1f60a86) 
+ Don't rebuild existing audio thumbs for playlist on project opening, display playlist audio thumbs in clip monitor. [Commit.](http://commits.kde.org/kdenlive/84e2b4ee38d59d72f35402fe82a071be8610af2b) 
+ Fix subtitle selection by keyboard shortcut. [Commit.](http://commits.kde.org/kdenlive/c661c68031d7510f203d36b005ef586aca5b31fe) 
+ Subtitle edit: switch to KTextEdit so we get spell check. [Commit.](http://commits.kde.org/kdenlive/5026ce2b3c8230b57b4ed0e3ae946b957dff6613) 
+ Automatically highlight text for editing when adding a subtitle. [Commit.](http://commits.kde.org/kdenlive/ee13f34ba7525a4bc0ef448e94740ca578803466) 
+ FIx possible crash on subtitle resize, and allow cutting unselected subtitle. [Commit.](http://commits.kde.org/kdenlive/8e417866b8229fbfc2dc7b6d81e9c2b4316e96e8) 
+ Allow keyboard grab of subtitles. [Commit.](http://commits.kde.org/kdenlive/5c13969815b0820b2fca96b3a6db8116ef0cc729) 
+ Do not allow zero for values of a project profile (framrate, framesize,…). [Commit.](http://commits.kde.org/kdenlive/303c2471de0ff0eb463bd3edfce30680b307801b) Fixes bug [#432016](https://bugs.kde.org/432016)
+ Remove "Create Region" menu item (not re-implemented yet) #82. [Commit.](http://commits.kde.org/kdenlive/3081a236883f9b752f667bbbbfd57b4903fa962d) 
+ Titler: show correct outline color on first use. [Commit.](http://commits.kde.org/kdenlive/3f496c2a92ce07f2848a757ab2ce8c73f9d23fb2) 
+ Fix color picker corruption. [Commit.](http://commits.kde.org/kdenlive/52a4aeabf89d98a653de5b4edd6e8f0a31abc03f) 
+ Hide keyframe mode for rotoscoping (only linear supported). [Commit.](http://commits.kde.org/kdenlive/9b015768b6e66e7b091f245fe7211e2192598460) 
+ Subtitles: fix crash on "select clip" shortcut. [Commit.](http://commits.kde.org/kdenlive/095b5ca9f8b38fb08f411a6707ed4ec4c37c8e32) 
+ Fix reset config on windows #931. [Commit.](http://commits.kde.org/kdenlive/d1e71017b08fd9dafbc876288903ca1454b87118) 
+ Expanded track tag width only if >10 audio OR video tracks, not sum of. [Commit.](http://commits.kde.org/kdenlive/ed751688dbd42f3298a99b73eb9aa991a3a70ca3) 
+ Fix downloaded template titles and lumas not found on Windows. [Commit.](http://commits.kde.org/kdenlive/a322674079551ce1bf3a34261c9da8c0fffebb0f) 
+ Keep title text item editable if even if it is empty. [Commit.](http://commits.kde.org/kdenlive/7f303be53fac4793c820f893a71e8eb961e8f98c) 
+ Fix invisible text cursor in title editor #165 and other minor tweaks. [Commit.](http://commits.kde.org/kdenlive/1e02108e265207ff9faf432c1bdc4938796a05a9) Fixes bug [#403941](https://bugs.kde.org/403941). Fixes bug [#407497](https://bugs.kde.org/407497)
+ Fix subtitle text not updated on go to next/prev and related crash. [Commit.](http://commits.kde.org/kdenlive/86218927c7b1a3994ccad7d01f495174c466d424) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Add info about kaddressbook dependancy. [Commit.](http://commits.kde.org/kdepim-addons/455d97802dc84d412d3221e354c51063021412a1) See bug [#432228](https://bugs.kde.org/432228)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Delete client builder before d-pointer of KXMLGUIClient is killed. [Commit.](http://commits.kde.org/konsole/392ce42f5e6a1fdbaf398945b271e2d5b93ee255) Fixes bug [#432421](https://bugs.kde.org/432421)
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Use enter to activate next button too. [Commit.](http://commits.kde.org/kpimtextedit/71443f71e9deafc78770324c01b1049326292fdc) 
+ Add parent. [Commit.](http://commits.kde.org/kpimtextedit/23925184febe015e4cab64b298351cff47d94229) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix out of bounds read when parsing fstab. [Commit.](http://commits.kde.org/kpmcore/2d397bb2d2eb28adc147d267fa79ed10057e7868) 
+ Add initial support for dosfstools 4.2. [Commit.](http://commits.kde.org/kpmcore/00de13088995ae96cf660911196fceda096e5b19) Fixes bug [#432941](https://bugs.kde.org/432941)
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Fix low quality connections. [Commit.](http://commits.kde.org/krdc/bf7e0cb90395021f1f7ac59ce2cb497834cfd46e) 
+ VNC: accept the wheel event. [Commit.](http://commits.kde.org/krdc/d66ffeff28e5789d3085fbb50dbdabaa918c104b) Fixes bug [#432484](https://bugs.kde.org/432484)
+ Unpress modifiers on focusOutEvent in VncView. [Commit.](http://commits.kde.org/krdc/a02b82fc67114f296c9b05234615c5be086a8206) Fixes bug [#329951](https://bugs.kde.org/329951)
{{< /details >}}
{{< details id="marble" title="marble" link="https://commits.kde.org/marble" >}}
+ Fix GeoNames web service URL, is now api.geonames.org (#432598). [Commit.](http://commits.kde.org/marble/86d22b6f9735673cb87fbfc6e246c23718463cba) Fixes bug [#432598](https://bugs.kde.org/432598)
+ Provide BUILD_TOUCH option to force install of touch variant. [Commit.](http://commits.kde.org/marble/8e5bfbc0b4a9333b00ad77c43a0deffdb69b91c2) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Mail-thread-ignored and mail-thread-watched exist in breeze-icons. [Commit.](http://commits.kde.org/messagelib/4d472ca06c6448ae029a91f63266242dd316d745) 
+ Use "enter" for searching next element. [Commit.](http://commits.kde.org/messagelib/13c4cb5e530135e7a13e608acf8215c06e4d542d) 
+ Exclude loading stylesheet. [Commit.](http://commits.kde.org/messagelib/d5f4154db7c492de3f4cfa6105f7d5be949da308) 
+ Fix Bug 431218 - mail viewer loads external fonts even with external refs disabled. [Commit.](http://commits.kde.org/messagelib/f8557851f5e43f991aaeecd5bcbb9597d65b2005) Fixes bug [#431218](https://bugs.kde.org/431218)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Presentation: Show better the status of the "playing" button. [Commit.](http://commits.kde.org/okular/6f5b23290fdd2ada8e0e7f44ffada02c978d0c6a) Fixes bug [#432416](https://bugs.kde.org/432416)
+ Pdf: Fix InPlace text annotations being loaded as linked. [Commit.](http://commits.kde.org/okular/f8fa27e218153958140915b8e5448740b2d43a6e) Fixes bug [#432009](https://bugs.kde.org/432009)
{{< /details >}}
{{< details id="palapeli" title="palapeli" link="https://commits.kde.org/palapeli" >}}
+ Make the cache work. [Commit.](http://commits.kde.org/palapeli/e73bce0854c12dbb25a494670845da7c6363a7df) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Fix compression quality range. [Commit.](http://commits.kde.org/spectacle/01f0cdcde587d97d620cda90cb751f89f260c6b5) 
{{< /details >}}
