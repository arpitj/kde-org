---
aliases:
- /announcements/plasma-5.20.2-5.20.3-changelog
hidden: true
plasma: true
title: Plasma 5.20.3 complete changelog
type: fulllog
version: 5.20.3
---

{{< details href="https://commits.kde.org/bluedevil" title="bluedevil" >}}
+ Fix listitems on mobile. [Commit.](http://commits.kde.org/bluedevil/b22f227e8b84e9b1aabfa8ed911949db23dbe695) 
{{< /details >}}



{{< details href="https://commits.kde.org/discover" title="discover" >}}
+ Disable session management properly. [Commit.](http://commits.kde.org/discover/d513a424dc7f35246e712359e0807d01431da579) Fixes bug [#415874](https://bugs.kde.org/415874)
{{< /details >}}



{{< details href="https://commits.kde.org/khotkeys" title="khotkeys" >}}
+ Fix import of files. [Commit.](http://commits.kde.org/khotkeys/1d02c5cc840ed811bbe3b29f8de4368d4ce7a92a) Fixes bug [#428536](https://bugs.kde.org/428536)
{{< /details >}}



{{< details href="https://commits.kde.org/kscreen" title="kscreen" >}}
+ Daemon: improve consistency of the lid behaviour. [Commit.](http://commits.kde.org/kscreen/420c60adfcb95b09ab352b614fdd2dc2a66dcfa8) 
{{< /details >}}



{{< details href="https://commits.kde.org/ksysguard" title="ksysguard" >}}
+ Stop the daemon if we have no client. [Commit.](http://commits.kde.org/ksysguard/beb9dc895a8417529902d6fae79c1402c47632b1) Fixes bug [#427204](https://bugs.kde.org/427204)
+ Do not load multiple versions of the same plugin. [Commit.](http://commits.kde.org/ksysguard/0e8b80373990876f19ee46bbbbf2caf6e1310e9d) 
+ Only look for libnl. [Commit.](http://commits.kde.org/ksysguard/ad3911b4a51474dba96fc8c27f7de6317054a5e7) 
{{< /details >}}



{{< details href="https://commits.kde.org/kwayland-server" title="kwayland-server" >}}
+ Don't use Qt::UniqueConnection on a lambda. [Commit.](http://commits.kde.org/kwayland-server/1012541ce644ac3e2dd630077bbcb5d036babb4d) 
+ Only connect to BufferInterface::aboutToBeDestroyed once. [Commit.](http://commits.kde.org/kwayland-server/8ab52255442223c5ce43d60f99c8ce8787969120) See bug [#428361](https://bugs.kde.org/428361)
{{< /details >}}



{{< details href="https://commits.kde.org/kwin" title="kwin" >}}
+ Restore empty filename check. [Commit.](http://commits.kde.org/kwin/0b5cbe4016bc69e4f15e437838983beacae3eab8) Fixes bug [#427979](https://bugs.kde.org/427979)
+ Drm: Make sure the screens are turned on when we come from suspend. [Commit.](http://commits.kde.org/kwin/191bef73ac00cdb3907ca4be33be6049ac40fa7c) Fixes bug [#428424](https://bugs.kde.org/428424)
+ Move some connects from Scene::addToplevel() to Window constructor. [Commit.](http://commits.kde.org/kwin/405a0360fb3df6d8508325df997fadfaed21e986) 
+ Scene: Stop monitoring changes for unmapped surfaces. [Commit.](http://commits.kde.org/kwin/575cc46a8a05c4667bad758eddfa052d44424cf5) 
+ Effects/screenshot: fix the screenshot on GLES. [Commit.](http://commits.kde.org/kwin/7b4925f761cdadb2d19d5d7ee2c3ac0cc7156280) 
+ [scene] Fix segfault in KWin::WindowPixmap::shape (BUG: 426567). [Commit.](http://commits.kde.org/kwin/00570eb741fde070054fd18eae6c69496f6d1949) 
+ Kwinrules: Limit hightlight scrolling duration. [Commit.](http://commits.kde.org/kwin/7465dcc9de58973da932a47449d81ef6668e0f72) Fixes bug [#428139](https://bugs.kde.org/428139)
+ Transform a pending repaint into a workspace repaint before destroying Deleted. [Commit.](http://commits.kde.org/kwin/3d40db866c04ac4121a625d0212c1cf41e499f3f) 
{{< /details >}}



{{< details href="https://commits.kde.org/libksysguard" title="libksysguard" >}}
+ Autodelete smapsRunnable. [Commit.](http://commits.kde.org/libksysguard/5e48a74d5f2b0cd271b4edc8308dabd8d2a6fd79) Fixes bug [#428048](https://bugs.kde.org/428048)
{{< /details >}}



{{< details href="https://commits.kde.org/plasma-desktop" title="plasma-desktop" >}}
+ Kimpanel: Fix action in menu. [Commit.](http://commits.kde.org/plasma-desktop/b96575a9156f2a801f958e8e1051ccea179e7b0b) 
+ Only allow multi key shortcuts for standard shortcuts. [Commit.](http://commits.kde.org/plasma-desktop/6354f61fd5503752619a30128646da41943889b5) 
{{< /details >}}



{{< details href="https://commits.kde.org/plasma-disks" title="plasma-disks" >}}
+ Actually erase devices. [Commit.](http://commits.kde.org/plasma-disks/916f0081b2334759b3d92ba2ca5c042d2df85535) Fixes bug [#428746](https://bugs.kde.org/428746)
{{< /details >}}



{{< details href="https://commits.kde.org/plasma-nm" title="plasma-nm" >}}
+ Do not show absurdedly high speeds on first update. [Commit.](http://commits.kde.org/plasma-nm/08efc47603e59ae702e4cf181eb26374d0af97af) 
{{< /details >}}



{{< details href="https://commits.kde.org/plasma-workspace" title="plasma-workspace" >}}
+ Fix missing "Switch User" button on lockscreen with systemd 246. [Commit.](http://commits.kde.org/plasma-workspace/aaa7a59a6710a89f21ebd441616df13be5ba8fef) Fixes bug [#427777](https://bugs.kde.org/427777)
+ [applets/devicenotifier] Fix regression with expand. [Commit.](http://commits.kde.org/plasma-workspace/640082f0623279e0aacdf599a38ed0ebf9be04f8) Fixes bug [#428379](https://bugs.kde.org/428379)
+ Fix edge case for tilde and ENV command. [Commit.](http://commits.kde.org/plasma-workspace/cc91a6c40777eaae3f981a9e63e826c1c4b9c722) 
+ [lookandfeel] Fix switching to a different user session. [Commit.](http://commits.kde.org/plasma-workspace/9a78614d4bbd9852a88bbecadc48a3f856e0214b) See bug [#423526](https://bugs.kde.org/423526)
+ [libkworkspace] Fix if getCurrentSeat needs to fallback to old approach. [Commit.](http://commits.kde.org/plasma-workspace/b4ef966790c35cc3cf14f9953b8b83be0bc0f821) Fixes bug [#423526](https://bugs.kde.org/423526)
+ Fix SystemEntries not updating correctly. [Commit.](http://commits.kde.org/plasma-workspace/d3c0c394d4673198574ec6c5ab2f668e4a6621da) See bug [#423526](https://bugs.kde.org/423526). Fixes bug [#427779](https://bugs.kde.org/427779)
+ [applets/icon] Handle non-Applications URLs again. [Commit.](http://commits.kde.org/plasma-workspace/2ff78e97ccf2763f6f4933cc32a6c95d5642c87f) Fixes bug [#427797](https://bugs.kde.org/427797)
+ [System Tray] Fall back to tool tip title if no title is set. [Commit.](http://commits.kde.org/plasma-workspace/2ea45bcf5157bac99d4cc16921735eafadd2c257) 
{{< /details >}}



{{< details href="https://commits.kde.org/systemsettings" title="systemsettings" >}}
+ Save highlight default state. [Commit.](http://commits.kde.org/systemsettings/35db75d5618282c39d5ed17fcb8937969761a670) Fixes bug [#427806](https://bugs.kde.org/427806)
+ Fix highlight settings tool button use onToggled to prevent bad state. [Commit.](http://commits.kde.org/systemsettings/cee4ba93c659aa7e5bc62cac5a1ca959fcc5241c) Fixes bug [#427869](https://bugs.kde.org/427869)
+ Fix call to KWorkSpace::detectPlatform. [Commit.](http://commits.kde.org/systemsettings/ca5a4e1439bb7cb11afcf6b386bb937971a185b0) Fixes bug [#428339](https://bugs.kde.org/428339)
{{< /details >}}