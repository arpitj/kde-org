---
title: Plasma 5.26.3 complete changelog
version: 5.26.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Remove top margin caused by empty header on UpdatesPage. [Commit.](http://commits.kde.org/discover/c0880d36f2022e05147cec5798ba7ffd5636f74d) 
+ Notifier: Fix notification logic. [Commit.](http://commits.kde.org/discover/dd7f74581d38a27fd734cdd44d10ca28276d0271) 
+ Pk/?: Fix problem when using faulty URL. [Commit.](http://commits.kde.org/discover/3397ca1238098f274e77e14ce8f8ebbb528f3c58) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Wayland: Prevent matching dnd actions after drop. [Commit.](http://commits.kde.org/kwin/6d72dd44d551c1d21c3b4a0b6cffcd25b9de82b2) 
+ Screencast: Fix inverted screencast on OpenGLES and memfd. [Commit.](http://commits.kde.org/kwin/6fd95cc1c5ebce989e4cfeb5dc44276fcb950d94) 
+ Screencasting: Make sure we are reporting properly scaled damage values. [Commit.](http://commits.kde.org/kwin/1f8efeaa58a224721f7591d2ef7c7b3169be3931) 
+ Plugins/screencast: centralize format querying into the RenderBackend. [Commit.](http://commits.kde.org/kwin/4dada2819334c93a42db160af3d489fa6375abc7) Fixes bug [#460563](https://bugs.kde.org/460563)
+ Backends/drm: fix format sorting. [Commit.](http://commits.kde.org/kwin/c038d699b933584273986644d597a09635eb8170) 
+ Fix blur/contrast for X11 window if Xwayland is not scaled. [Commit.](http://commits.kde.org/kwin/00388f2b8a4fb81515eadf2768d49d09c4e31d6d) Fixes bug [#461021](https://bugs.kde.org/461021)
+ [effects/windowview] Don't allow setting non-global shortcuts. [Commit.](http://commits.kde.org/kwin/33fdd21c5d44d726d0822a81b0c193dec7900154) 
+ [effects/overview] Don't allow setting non-global shortcuts. [Commit.](http://commits.kde.org/kwin/be57235fe39fafd2ab13d2236d57e861a7045668) 
+ Backends/drm: fix common mode generation. [Commit.](http://commits.kde.org/kwin/7db06b00036251cbc3c897aebd26a9faab99f762) 
+ Nightcolormanager: Emit timing change only when they have actually changed. [Commit.](http://commits.kde.org/kwin/2465dfe8ce94e1afeed3e3210adef9019e76383b) 
+ X11: Don't force QT_NO_GLIB=1. [Commit.](http://commits.kde.org/kwin/4c5830ba149ec4462587a95f78624dfc981d281c) Fixes bug [#460980](https://bugs.kde.org/460980)
+ X11: Don't force QT_QPA_PLATFORM=xcb. [Commit.](http://commits.kde.org/kwin/8633f9952507c3e99175a43b4d813cc1669f8db9) 
+ X11window: revert more from 3a28c02f. [Commit.](http://commits.kde.org/kwin/2339f7bfb7872e05fbdbd931850a74a3441b3292) Fixes bug [#461032](https://bugs.kde.org/461032)
+ Fix compile. [Commit.](http://commits.kde.org/kwin/649a573e358f32193715237849ebf5d7e28e14e9) 
+ X11window: partially revert 3a28c02f. [Commit.](http://commits.kde.org/kwin/22931975739a4c050e767e8a2cc0ebd83ba1d59b) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Tablet: Make sure the "lock aspect ratio" stays true. [Commit.](http://commits.kde.org/plasma-desktop/91ff1280a23ace93a00d66fdf078b748d504d087) 
+ Tablet: Fix the reset button for the outputs view. [Commit.](http://commits.kde.org/plasma-desktop/8f5f06fcaf5b77a804308c3c7dc4b16ff80be153) Fixes bug [#458608](https://bugs.kde.org/458608)
+ [kcms/tablet] Delete device when removed. [Commit.](http://commits.kde.org/plasma-desktop/3215f057ace1c888c98fe4f278e97402389a8067) 
+ [kcms/tablet] Fix i18n. [Commit.](http://commits.kde.org/plasma-desktop/88899fa5ac4fb133ce6ce2f906d8df40d6bbd017) 
+ Fix panel configuration to properly limit maximum panel size to half height. [Commit.](http://commits.kde.org/plasma-desktop/97fcbfa54e682a4f4274aae59fdfe4e344e61d5a) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Homescreens/halcyon: Fix horizontal scrollbar being shown on app list. [Commit.](http://commits.kde.org/plasma-mobile/620a86c0d46f6b64f2844637f12a05751e29732b) 
+ Lockscreen: Refactor and cleanup keypad. [Commit.](http://commits.kde.org/plasma-mobile/97aa8e104c8d9421167fa8ed14d03b9f7268f414) 
+ Components: Fix MarqueeLabel behaviour with new line characters. [Commit.](http://commits.kde.org/plasma-mobile/84b7c9333eb704cd23003314fb3ea726ad51281d) 
+ Statusbar: Don't use fractional font sizes. [Commit.](http://commits.kde.org/plasma-mobile/461ce8918ddffae40049e1ef81a2e265107ddf62) 
+ Lockscreen: Use bold text for clock to improve contrast. [Commit.](http://commits.kde.org/plasma-mobile/81372db0bac94aafe8372bed41bc1eeb68a9cc83) 
+ Shell: Fix accent color from wallpaper not working. [Commit.](http://commits.kde.org/plasma-mobile/4c0141f15118d91c6902c838de279427302e7ec6) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ L2TP: Fix detection of Libreswan 4.9 and later. [Commit.](http://commits.kde.org/plasma-nm/a5d622511ac95378614ba4d2ee0b5d262166f25c) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Notifications: Use String "0" for null percent. [Commit.](http://commits.kde.org/plasma-workspace/429abee00a54d2dcbb9a71c9cd3904953b3082b7) 
+ Revert apples/kicker: fix timer in triangular mouse filter. [Commit.](http://commits.kde.org/plasma-workspace/e8746cce1acd8f6b3f94d35b38f9d0f153f43acb) Fixes bug [#461395](https://bugs.kde.org/461395)
+ Runners/services: Do not parse exec of flatpaks. [Commit.](http://commits.kde.org/plasma-workspace/d7a8719d53c798fd2219d98015c034823baa5419) Fixes bug [#461241](https://bugs.kde.org/461241)
+ Kcms/regionandlanguage: convert ASCII unicode symbol to QChar. [Commit.](http://commits.kde.org/plasma-workspace/6271a78f8c2414a5912180192e26476fb52ea313) Fixes bug [#460704](https://bugs.kde.org/460704)
+ Startplasma-wayland: Don't set GDK_SCALE and GDK_DPI_SCALE. [Commit.](http://commits.kde.org/plasma-workspace/9ec6cf47a30fbdc4c64b7b0a0b4fb446f9d2904c) Fixes bug [#443215](https://bugs.kde.org/443215)
+ Runners/services: Do not match exec values of PWAs. [Commit.](http://commits.kde.org/plasma-workspace/994fbffcae9a357d0d3cd4a9b6b64a9c6644ffbd) Fixes bug [#460796](https://bugs.kde.org/460796)
+ Runners/servicerunner: increase match type when query exactly matches service name. [Commit.](http://commits.kde.org/plasma-workspace/d1592ff678e60f7d7a075d3531b53b06401a33a9) Fixes bug [#459516](https://bugs.kde.org/459516)
+ Runners/recentdocuments: more fine-grained relevance and type assignment. [Commit.](http://commits.kde.org/plasma-workspace/49dd3d6528bc675bf3be2b3cc8fb29aaea4697de) 
+ Runners/baloo: increase match type when query exactly matches file name. [Commit.](http://commits.kde.org/plasma-workspace/fc08611965ddf6e9a6228964c5767503f1bddff2) 
+ Runners/locations: decrease match type when not a known existing file. [Commit.](http://commits.kde.org/plasma-workspace/80d9f1c8e5e816c7f2ff5464379a94d177652191) See bug [#340283](https://bugs.kde.org/340283)
+ Wallpapers/image: always try to remove a path in `ImageBackend::removeSlidePath`. [Commit.](http://commits.kde.org/plasma-workspace/0f6cc02da0db3e3831246fc3665001b8965a5325) Fixes bug [#461003](https://bugs.kde.org/461003)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Powerprofileconfig: Use proper signal for detecting user interaction. [Commit.](http://commits.kde.org/powerdevil/892634f68c6577f46c3044f8e86414a583625ade) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Fix kcms inside a category always being indented. [Commit.](http://commits.kde.org/systemsettings/aa828df30208294960d8245acd9d3ba37d11e5ba) 
{{< /details >}}

