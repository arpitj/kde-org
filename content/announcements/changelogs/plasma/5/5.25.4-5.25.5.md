---
title: Plasma 5.25.5 complete changelog
version: 5.25.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="oxygen-sounds" href="https://commits.kde.org/oxygen-sounds" >}}
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/oxygen-sounds/a8786bf95003bde03df699e016a9ce510f86c502) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/kickoff: set initial appsModelRow to 0. [Commit.](http://commits.kde.org/plasma-desktop/035ed9a3571348c815035f77e011a90178ae2a77) 
+ Toolboxes: set default position to topcenter. [Commit.](http://commits.kde.org/plasma-desktop/75855242e38f2ef88f250617ffd7f09f74586b9f) See bug [#457814](https://bugs.kde.org/457814)
+ Fix KCM duplicates in landingpage. [Commit.](http://commits.kde.org/plasma-desktop/44821ceddc9bd14f72bf1e9e4c5f22119f657262) Fixes bug [#449563](https://bugs.kde.org/449563)
+ Disable Drag and Drop for touch, fix drag and drop icons. [Commit.](http://commits.kde.org/plasma-desktop/b31aeb62f739c8e948acc4d687895f32e86aa986) Fixes bug [#450448](https://bugs.kde.org/450448)
+ [kicker] Fix baloo runner name. [Commit.](http://commits.kde.org/plasma-desktop/484514d3a2199aaaa2a20a9ab8236c7966204aa7) Fixes bug [#456562](https://bugs.kde.org/456562)
+ Taskmanager: Use proper QUrls for recent document actions. [Commit.](http://commits.kde.org/plasma-desktop/24e92862c85cdfee7a0fbe66336b7d259acd86a9) Fixes bug [#457685](https://bugs.kde.org/457685)
+ [desktop/package] Fix inconsistent press-to-enter-edit-mode behavior. [Commit.](http://commits.kde.org/plasma-desktop/3e5d3679b809f81952d85fe822a354358fae7164) Fixes bug [#456994](https://bugs.kde.org/456994)
+ Fix emojier displaying a blank window with ibus 1.5.26. [Commit.](http://commits.kde.org/plasma-desktop/58f34488426f041f0b185bfd0fbcf049b2119e64) Fixes bug [#457521](https://bugs.kde.org/457521)
+ Revert "Use onEntered in KickoffItemDelegate". [Commit.](http://commits.kde.org/plasma-desktop/3c87dc68746100960263ca35b400442170513474) Fixes bug [#454349](https://bugs.kde.org/454349). Fixes bug [#455674](https://bugs.kde.org/455674). Fixes bug [#456993](https://bugs.kde.org/456993)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Components: Attempt to fix some cyclic dependencies. [Commit.](http://commits.kde.org/plasma-mobile/7b50367b2715881a973e6bfab99af68dd50396ed) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Kcms/users: don't let "Choose File…" text overflow. [Commit.](http://commits.kde.org/plasma-workspace/15e2e419821873d0cf0135c5e4973b6033173e26) Fixes bug [#458614](https://bugs.kde.org/458614)
+ [kcms/style] Fix setting to default and apply button enablement. [Commit.](http://commits.kde.org/plasma-workspace/df9b8900f5d3e17d6c935bef92b6ac9b619961ef) 
+ Kcms/users : Fix focus for new user input. [Commit.](http://commits.kde.org/plasma-workspace/4ce57753d35c2979d7e54db87522f1d682eb290a) Fixes bug [#458377](https://bugs.kde.org/458377)
+ Kcms/nightcolor: fix checks for manual timing errors. [Commit.](http://commits.kde.org/plasma-workspace/3f4a20ec998b12cd89a701baaf20dbc5b466efa2) 
+ [kcms/style] Consider GTK theme setting when computing default state. [Commit.](http://commits.kde.org/plasma-workspace/c63336c7cd5cf78f86cd0163900c22724a2f80d1) Fixes bug [#458292](https://bugs.kde.org/458292)
+ Applets/kicker: fix app icon loading logic to better handle relative paths. [Commit.](http://commits.kde.org/plasma-workspace/ffd76192f35ddc74fcd63fbe77203894b14068ad) Fixes bug [#457965](https://bugs.kde.org/457965)
+ Kcminit: Allow running modules by their name. [Commit.](http://commits.kde.org/plasma-workspace/bd9be729d8528e29e2f879341a524f96de7d81d7) See bug [#435113](https://bugs.kde.org/435113)
+ Take the pluginId from the kpackage metadata. [Commit.](http://commits.kde.org/plasma-workspace/1768eefda2e4c063f4deead838c4b6410fced341) Fixes bug [#457657](https://bugs.kde.org/457657)
+ Applets/notifications: Fix displaying header progress indicator on different DPI. [Commit.](http://commits.kde.org/plasma-workspace/e52bfaf9e94193a3f966c4539fc21a9f25fac3ca) Fixes bug [#435004](https://bugs.kde.org/435004)
+ [panelview] Avoid assert if max < min. [Commit.](http://commits.kde.org/plasma-workspace/f33070a0d94fe7d62fa19fe9ab8864a719ec0ea1) Fixes bug [#454064](https://bugs.kde.org/454064)
+ ScreenPool: Ensure that primaryScreenChanged is emitted always after screenAdded. [Commit.](http://commits.kde.org/plasma-workspace/ce477ffc6ef3b2de6d90c528fb88007af39b9c37) 
+ [libtaskmanager] Fix null check for window. [Commit.](http://commits.kde.org/plasma-workspace/0acb09809cd700b1571263b94f6f506d2d08a32b) Fixes bug [#456094](https://bugs.kde.org/456094). Fixes bug [#457690](https://bugs.kde.org/457690)
+ Lookandfeelmanager: Write colors before color scheme. [Commit.](http://commits.kde.org/plasma-workspace/864b2b7797ad8ad22a5eb3c90534b80ca1655d4c) Fixes bug [#421745](https://bugs.kde.org/421745)
+ Fix appstream runner results appearing before apps/kcms. [Commit.](http://commits.kde.org/plasma-workspace/65610ce17fb3f4b0e96b7e3444c69ddca793e535) Fixes bug [#457600](https://bugs.kde.org/457600)
+ [dataengines/weather/dwd] Check if jobs failed. [Commit.](http://commits.kde.org/plasma-workspace/1cd873d277a1215d8ca3b672e00933f40808511d) 
+ [dataengines/weather/dwd] Properly detect empty reply. [Commit.](http://commits.kde.org/plasma-workspace/fae8cbac976c933e481991e7c77b4e6f54a2eefa) Fixes bug [#457606](https://bugs.kde.org/457606)
+ Applets/clipboard: press Ctrl+S to save in the edit page. [Commit.](http://commits.kde.org/plasma-workspace/e258a3a6bbc857a08be06e10a2d59bcea50df8de) 
+ Applets/appmenu: do not update menu when panel gets focus. [Commit.](http://commits.kde.org/plasma-workspace/453403958014ae4cdd0fd24a1116f5808127fc59) Fixes bug [#455520](https://bugs.kde.org/455520)
+ Fix systemmonitor preferences being reset. [Commit.](http://commits.kde.org/plasma-workspace/e64c13729bd1e2de43689b310caafac62f748c87) Fixes bug [#454004](https://bugs.kde.org/454004)
+ KRunner: Set location before showing. [Commit.](http://commits.kde.org/plasma-workspace/eb44145d5797037348462b5e0cae309d20e52d3b) Fixes bug [#447096](https://bugs.kde.org/447096)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Fix profile switching in the brightness actions. [Commit.](http://commits.kde.org/powerdevil/0f548c53dd177235eaf5ce8452c9b44839c12e21) Fixes bug [#394945](https://bugs.kde.org/394945)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ [screenchooser] Clip listviews. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/19d633e07fe766f732c2fd8173bfda01221057ab) 
{{< /details >}}

