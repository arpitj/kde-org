2005-05-10 10:32 +0000 [r411876]  binner

	* kdetoys.lsm: update lsm for release

2005-05-14 09:12 +0000 [r413647]  benb

	* debian/control, debian/kdetoys-doc-html.doc-base.kworldclock
	  (added), debian/kteatime.README.Debian, debian/amor.docs (added),
	  debian/kweatherreport.1, debian/changelog, debian/copyright,
	  debian/kmoon.install, debian/kodo.1,
	  debian/kworldclock.README.Debian (added),
	  debian/kdetoys-doc-html.doc-base.kweather, debian/amor.1,
	  debian/kworldclock.1, debian/source.lintian-overrides: The
	  packaging for this module in the 3.4 branch is quite out of date.
	  Bring it up to the current state of packaging in debian unstable.

