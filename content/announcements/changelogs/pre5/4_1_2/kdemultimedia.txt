------------------------------------------------------------------------
r854902 | lueck | 2008-08-30 17:10:30 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r855142 | lueck | 2008-08-31 10:54:50 +0200 (Sun, 31 Aug 2008) | 1 line

fixed mx stupid backported typo
------------------------------------------------------------------------
r855549 | mpyne | 2008-09-01 06:24:05 +0200 (Mon, 01 Sep 2008) | 3 lines

Backport fix for empty now playing bar and track announcement popup after using the search widget
when paused (whew!) to KDE 4.1.  Will be in the 4.1.2 release.

------------------------------------------------------------------------
r863582 | thanngo | 2008-09-22 16:48:52 +0200 (Mon, 22 Sep 2008) | 2 lines

BUG:170941, backport from trunk to fix crash in Dragon Part

------------------------------------------------------------------------
