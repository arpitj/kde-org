------------------------------------------------------------------------
r1120899 | scripty | 2010-04-30 13:57:24 +1200 (Fri, 30 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1121277 | scripty | 2010-05-01 13:51:41 +1200 (Sat, 01 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1121662 | scripty | 2010-05-02 13:51:16 +1200 (Sun, 02 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1122016 | mzanetti | 2010-05-03 11:14:33 +1200 (Mon, 03 May 2010) | 2 lines

backport of bugfix commit 1122012

------------------------------------------------------------------------
r1122860 | lappelhans | 2010-05-05 09:25:09 +1200 (Wed, 05 May 2010) | 2 lines

Fix the external

------------------------------------------------------------------------
r1124974 | mfuchs | 2010-05-11 00:25:03 +1200 (Tue, 11 May 2010) | 2 lines

*Forbids directory traversal attacks by faulty metalinks.
*When closing the metalink-download-dialog the download does not start
------------------------------------------------------------------------
r1125551 | pali | 2010-05-12 05:45:01 +1200 (Wed, 12 May 2010) | 2 lines

Use user defined display name for jabber contacts

------------------------------------------------------------------------
r1125553 | pali | 2010-05-12 05:48:13 +1200 (Wed, 12 May 2010) | 2 lines

Set correct encoding for bonjour contacts

------------------------------------------------------------------------
r1125801 | mfuchs | 2010-05-12 23:49:28 +1200 (Wed, 12 May 2010) | 2 lines

Backport r1125752
Only if DatasourceFactory got started calling deinit will delete the file on m_dest.
------------------------------------------------------------------------
r1125845 | mfuchs | 2010-05-13 01:45:40 +1200 (Thu, 13 May 2010) | 1 line

Segments have 512000 Bytes by default.
------------------------------------------------------------------------
r1125862 | mfuchs | 2010-05-13 02:48:21 +1200 (Thu, 13 May 2010) | 2 lines

Create two jobs in KioDownload. The first is for downloading and finding the filesize, while the second (started after first data is retrieved) is only there to see if resuming is supported and stops after more than 100 Bytes are downloaded.
BUG:235875
------------------------------------------------------------------------
r1125864 | mfuchs | 2010-05-13 02:56:47 +1200 (Thu, 13 May 2010) | 1 line

Deletes the m_resumeJob earlier if possible.
------------------------------------------------------------------------
r1125890 | mfuchs | 2010-05-13 04:07:04 +1200 (Thu, 13 May 2010) | 2 lines

Set a suggestedFileName for downloads from httpserver.
BUG:237282
------------------------------------------------------------------------
r1125944 | pali | 2010-05-13 06:43:41 +1200 (Thu, 13 May 2010) | 2 lines

Fix skype protocol, when username contains char ','

------------------------------------------------------------------------
r1126045 | scripty | 2010-05-13 14:00:41 +1200 (Thu, 13 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1126469 | scripty | 2010-05-14 14:03:35 +1200 (Fri, 14 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1127121 | mfuchs | 2010-05-16 03:20:15 +1200 (Sun, 16 May 2010) | 1 line

Status when downloading the metalink-file is set to Stopped, avoids using a hack later.
------------------------------------------------------------------------
r1127162 | mfuchs | 2010-05-16 04:29:52 +1200 (Sun, 16 May 2010) | 1 line

Add missing filesSelected method.
------------------------------------------------------------------------
r1127588 | scripty | 2010-05-17 14:17:44 +1200 (Mon, 17 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1127601 | muesli | 2010-05-17 16:45:14 +1200 (Mon, 17 May 2010) | 5 lines

* Fix #234002: Correctly calculate the sizeHint (height) of the identity / account list.

BUG: 234002


------------------------------------------------------------------------
r1127666 | mfuchs | 2010-05-17 21:54:51 +1200 (Mon, 17 May 2010) | 2 lines

Backported r1127663
*FileModel and FileItem get a isFile method for convenience.
------------------------------------------------------------------------
r1127667 | mfuchs | 2010-05-17 21:54:52 +1200 (Mon, 17 May 2010) | 4 lines

Backport r1127664
*DataSourceFactory checks the previous doDownload state before setting a new one.
*DataSourceFactory gets a downloadInitialized method that returns true if the download was initialized anytime before, i.e. the files has been created. This is useful to understand if a file was created by the DataSourceFactory or existed before.
*DataSourcefactory also changes the destination of verifier and signature if they existed and the destination of the DataSourceFactory has changed while downloadInitialized returned false.
------------------------------------------------------------------------
r1127798 | mfuchs | 2010-05-18 04:46:56 +1200 (Tue, 18 May 2010) | 4 lines

Backport r1127667
*DataSourceFactory checks the previous doDownload state before setting a new one.
*DataSourceFactory gets a downloadInitialized method that returns true if the download was initialized anytime before, i.e. the files has been created. This is useful to understand if a file was created by the DataSourceFactory or existed before.
*DataSourcefactory also changes the destination of verifier and signature if they existed and the destination of the DataSourceFactory has changed while downloadInitialized returned false.
------------------------------------------------------------------------
r1127887 | mfuchs | 2010-05-18 10:03:32 +1200 (Tue, 18 May 2010) | 2 lines

Backport r1127885
View of verifier-model is not expandable.
------------------------------------------------------------------------
r1127897 | mfuchs | 2010-05-18 10:43:38 +1200 (Tue, 18 May 2010) | 3 lines

Backport r1127895
Remembers position of columns in the main view.
CCBUG:228522
------------------------------------------------------------------------
r1128272 | mfuchs | 2010-05-19 08:40:57 +1200 (Wed, 19 May 2010) | 2 lines

Backport r1128271
Simplyfies KGet::generalDestDir as KGlobalSettings::downloadPath() works better now.
------------------------------------------------------------------------
r1129875 | scripty | 2010-05-24 13:49:51 +1200 (Mon, 24 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130165 | mzanetti | 2010-05-25 04:42:54 +1200 (Tue, 25 May 2010) | 9 lines

backport of rev. 1130163

fix handling of xml tags in tagged plaintext messages.
This, however breaks opportunistic mode when rich text 
formatting is enabled. Currently there seems to be no 
way to support both at the same time. The current
solutions seems to be most useful one.


------------------------------------------------------------------------
r1130301 | scripty | 2010-05-25 14:34:16 +1200 (Tue, 25 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130520 | mfuchs | 2010-05-26 03:25:15 +1200 (Wed, 26 May 2010) | 2 lines

Update the selection in the mainview.
BUG:238790
------------------------------------------------------------------------
r1131325 | murrant | 2010-05-28 16:10:38 +1200 (Fri, 28 May 2010) | 4 lines

Backport r1130864 by murrant from trunk to the 4.4 branch:

Workaround for a common crash when disconnecting a VNC connection.

------------------------------------------------------------------------
