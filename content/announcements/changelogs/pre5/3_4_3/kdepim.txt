2005-07-21 03:35 +0000 [r437160]  vanrijn

	* branches/KDE/3.4/kdepim/kpilot/ChangeLog,
	  branches/KDE/3.4/kdepim/kpilot/lib/options.h: - D'oh! Version
	  number should change from 4.5.2 to 4.5.3 since we actually did do
	  stuff since KDE 3.4.1, and being that 3.4.2 just got tagged 8
	  hours ago, here's hoping we can get this fixed before 3.4.2 is
	  declared officially official. =:/ - updating ChangeLog with the
	  above and one bug fix since 3.4.1

2005-07-21 13:01 +0000 [r437285]  dfaure

	* branches/KDE/3.4/kdepim/kmail/keyresolver.cpp: Key approval
	  dialog fix: store the selected crypto keys into the addressbook
	  contact.

2005-07-21 15:06 +0000 [r437333]  wstephens

	* branches/KDE/3.4/kdepim/kresources/slox/webdavhandler.cpp:
	  Backport fix for offset SLOX appointments

2005-07-21 18:10 +0000 [r437392]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/alarmcalendar.h: Use rawEvents()
	  instead of events()

2005-07-25 11:09 +0000 [r438561]  dkukawka

	* branches/KDE/3.4/kdepim/kmailcvt/filter_pmail.cxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_oe.cxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_pmail.hxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_oe.hxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filters.hxx: - backport from
	  trunk to prevent failure with older gcc (< gcc 3.3) BUG: 106274

2005-07-25 11:18 +0000 [r438564]  dkukawka

	* branches/KDE/3.4/kdepim/kmailcvt/filter_evolution_v2.cxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_outlook.cxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_pmail.cxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_oe.cxx,
	  branches/KDE/3.4/kdepim/kmailcvt/filter_thunderbird.cxx: -
	  backport from trunk: - cleaned up usage of QString

2005-07-25 11:25 +0000 [r438566]  dkukawka

	* branches/KDE/3.4/kdepim/kmailcvt/filter_mbox.cxx: - backport from
	  thrunk: - fixed problem with corrupted mbox, which contains
	  binary data between mails (BUG: 106796).

2005-07-25 16:00 +0000 [r438634]  tilladam

	* branches/KDE/3.4/kdepim/kmail/kmailicalifaceimpl.cpp: Backport
	  of: Respect mResourceQuiet, otherwise the resorce produces
	  spurious conflicts.

2005-07-28 09:15 +0000 [r439498]  tilladam

	* branches/KDE/3.4/kdepim/kresources/kolab/kcal/resourcekolab.cpp:
	  Backport of forward port of: SVN commit 439457 by tilladam: Skip
	  duplicate incidences in read-only folders and don't offer
	  conflict resolution. The user could otherwise put new mails into
	  read-only folder and would later be unable to sync them up again.

2005-07-28 11:10 +0000 [r439529]  tilladam

	* branches/KDE/3.4/kdepim/libemailfunctions/email.cpp: Backport
	  nested parentheses fix.

2005-07-29 12:26 +0000 [r439964]  tilladam

	* branches/KDE/3.4/kdepim/kmail/kmsender.cpp: Crash guards,
	  inspired by a patch by Gustavo Pichorim Boiko. Thanks, dude. BUG:
	  101847

2005-08-01 20:10 +0000 [r442166]  mueller

	* branches/KDE/3.4/kdepim/kmail/partNode.cpp: backport 437867

2005-08-03 17:40 +0000 [r442711]  vanrijn

	* branches/KDE/3.4/kdepim/kpilot/ChangeLog,
	  branches/KDE/3.4/kdepim/kpilot/lib/options.h: - bumping version
	  to 4.5.4 (dreumes) in case of KDE 3.4.3

2005-08-04 20:05 +0000 [r443058]  jriddell

	* branches/KDE/3.4/kdepim/korganizer/Makefile.am: Compile
	  korganiser.la last, it depends on the other two libraries

2005-08-10 12:59 +0000 [r444507]  mlaurent

	* branches/KDE/3.4/kdepim/kpilot/kpilot/addressWidget.cc,
	  branches/KDE/3.4/kdepim/kpilot/kpilot/todoWidget.cc,
	  branches/KDE/3.4/kdepim/kpilot/kpilot/memoWidget.cc: Fix crash
	  when we can open db

2005-08-10 13:07 +0000 [r444518]  mlaurent

	* branches/KDE/3.4/kdepim/kpilot/lib/kpilotlink.cc: Fix crash:
	  timer can be null

2005-08-10 13:36 +0000 [r444767]  mlaurent

	* branches/KDE/3.4/kdepim/kpilot/kpilot/addressWidget.cc,
	  branches/KDE/3.4/kdepim/kpilot/kpilot/memoWidget.cc: Fix crash
	  when we can't open db

2005-08-18 03:48 +0000 [r450410]  emmott

	* branches/KDE/3.4/kdepim/kmail/avscripts/kmail_fprot.sh,
	  branches/KDE/3.4/kdepim/kmail/avscripts/kmail_antivir.sh,
	  branches/KDE/3.4/kdepim/kmail/avscripts/kmail_clamav.sh,
	  branches/KDE/3.4/kdepim/kmail/avscripts/kmail_sav.sh: fixed an
	  unreported bug in the kmail_*.sh antivirus scripts which would
	  almost always result in creating an unused temporary file and not
	  deleting afterwards every time an email was scanned.

2005-08-18 13:01 +0000 [r450549]  mlaurent

	* branches/KDE/3.4/kdepim/akregator/src/pageviewer.cpp: It's not
	  necessary to add new separator here

2005-08-19 15:10 +0000 [r450952]  mlaurent

	* branches/KDE/3.4/kdepim/konsolekalendar/konsolekalendar.cpp:
	  Don't crash when list is empty We can crash it when we launch
	  konsolekalendar --all

2005-08-19 18:40 +0000 [r451025]  kainhofe

	* branches/KDE/3.4/kdepim/libkcal/icalformatimpl.cpp: ARRRGGH! Just
	  when we release kde 3.4.2, a nasty bug in connection with gcc 4.0
	  (NOT with 3.3.x, which I use) creeps up: It seems that gcc 4.0 is
	  no longer initializing all members of a struct, while gcc 3.3.x
	  obviously did something like that. So we can't simply take a new
	  icaltime structure, since its member will be initialized with
	  random values in gcc 4.0. So everything that later on checks the
	  members of the struct will work on uninitialized values! Rather,
	  we need to use a null time (all fields initialized to 0) and set
	  all necessary fields later on. This fixes the end date corruption
	  (only of all-day events) that vanRijn and and Will are observing
	  with their gcc 4.0-compiled kdepim. If any distribution is using
	  kde 3.4.2 compiled with gcc 4.0, I suppose this fix needs to go
	  in! (Backport of commit 451010 from the 3.5 branch) CCMAIL:
	  debian-kde@lists.debian.org

2005-08-21 16:44 +0000 [r451740]  osterfeld

	* branches/KDE/3.4/kdepim/knode/knarticlewidget.cpp: Fix compile
	  problem I have using Kubuntu ( gcc 3.3.5 (Debian
	  1:3.3.5-8ubuntu2)): add #include <cstdlib> for getenv()

2005-08-26 21:44 +0000 [r453705]  osterfeld

	* branches/KDE/3.4/kdepim/akregator/src/librss/article.h,
	  branches/KDE/3.4/kdepim/akregator/src/librss/document.cpp,
	  branches/KDE/3.4/kdepim/akregator/src/librss/article.cpp,
	  branches/KDE/3.4/kdepim/akregator/src/librss/global.h: backport:
	  fix atom 1.0 parsing

2005-08-26 22:05 +0000 [r453709]  osterfeld

	* branches/KDE/3.4/kdepim/akregator/src/akregator_view.cpp: revert
	  quick filter behaviour back to 3.4.0 style, don't reset quick
	  filter when switching feeds CCBUG: 106348

2005-08-26 22:11 +0000 [r453712]  osterfeld

	* branches/KDE/3.4/kdepim/akregator/src/aboutdata.h: bump version
	  number to 1.1.3

2005-08-27 21:18 +0000 [r454078]  osterfeld

	* branches/KDE/3.4/kdepim/akregator/src/akregator.kcfg: backport:
	  enable interval fetching by default

2005-08-30 19:06 +0000 [r455182]  mueller

	* branches/KDE/3.4/kdepim/akregator/src/trayicon.h: fix compile

2005-08-31 19:55 +0000 [r455546]  mueller

	* branches/KDE/3.4/kdepim/kode/kxml_compiler/kxml_compiler.cpp:
	  backport 455505

2005-08-31 21:24 +0000 [r455566]  mueller

	* branches/KDE/3.4/kdepim/kode/kxml_compiler/kxml_compiler.cpp:
	  *sigh*

2005-09-01 15:42 +0000 [r455875]  tstaerk

	* branches/KDE/3.4/kdepim/karm/taskview.h,
	  branches/KDE/3.4/kdepim/karm/taskview.cpp: Avoid race condition
	  that causes a crash when deleting a task. Make code shorter and
	  clearer.

2005-09-03 11:52 +0000 [r456648]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/kalarmd/Makefile.am: Add kalarmd.h
	  to list of header files

2005-09-04 13:08 +0000 [r457023]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/Makefile.am: Add commented-out
	  nofinal for easy reinstatement on systems with too little
	  capacity

2005-09-05 00:03 +0000 [r457201]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/prefdlg.cpp,
	  branches/KDE/3.4/kdepim/kalarm/Changelog,
	  branches/KDE/3.4/kdepim/kalarm/prefdlg.h,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/alarmdaemon.cpp,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/kalarmd.h,
	  branches/KDE/3.4/kdepim/kalarm/kalarm.h,
	  branches/KDE/3.4/kdepim/kalarm/kalarm.tray.desktop,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/alarmdaemon.h: Bug 101877:
	  Prevent session restoration from showing main window which should
	  be hidden

2005-09-07 01:01 +0000 [r457923]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/BUGS (added): Add BUGS file

2005-09-08 23:21 +0000 [r458725]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/kalarmd/admain.cpp,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/alarmdaemon.cpp,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/adapp.cpp,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/alarmdaemon.h,
	  branches/KDE/3.4/kdepim/kalarm/kalarmd/kalarmd.autostart.desktop:
	  Bug 101877: further fixes

2005-09-10 11:22 +0000 [r459202]  vkrause

	* branches/KDE/3.4/kdepim/knode/knnetaccess.cpp: Patch by Will
	  Stephenson to fix a crash
	  (https://bugzilla.novell.com/show_bug.cgi?id=115598). Already
	  fixed differently in 3.5 branch.

2005-09-10 12:25 +0000 [r459224]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/Changelog,
	  branches/KDE/3.4/kdepim/kalarm/kalarmapp.cpp: Fix alarms getting
	  stuck if due during a daylight savings clock change

2005-09-13 20:27 +0000 [r460435]  djarvie

	* branches/KDE/3.4/kdepim/kalarm/functions.cpp: Bug 112233: change
	  New shortcut to the standard one

2005-09-18 10:11 +0000 [r461621]  hasso

	* branches/KDE/3.4/kdepim/libkholidays/holidays/Makefile.am:
	  Backport. CCMAIL:kde-et@linux.ee

2005-09-30 05:55 +0000 [r465538]  osterfeld

	* branches/KDE/3.4/kdepim/akregator/src/feed.cpp: sanatize article
	  expiry code, might be a bit slower, but less error-prone. users
	  of 3.4 branch or upcoming 3.4.3, please give feedback if that
	  fixes the problem. CCBUG: 99926

2005-09-30 10:05 +0000 [r465621]  osterfeld

	* branches/KDE/3.4/kdepim/akregator/src/myarticle.h,
	  branches/KDE/3.4/kdepim/akregator/src/archive.cpp: backport of
	  111908 (archive broken for feed URLs longer than 255 chars)
	  CCBUG: 111908

2005-10-02 10:14 +0000 [r466312]  vkrause

	* branches/KDE/3.4/kdepim/knode/resource.h: Increment version
	  number for 3.4.3.

2005-10-02 10:30 +0000 [r466314]  kloecker

	* branches/KDE/3.4/kdepim/kmail/kmversion.h: for KDE 3.4.3

2005-10-02 14:01 +0000 [r466382]  coolo

	* branches/KDE/3.4/kdepim/kmail/smimeconfiguration.ui,
	  branches/KDE/3.4/kdepim/korn/korncfg.ui,
	  branches/KDE/3.4/kdepim/korn/kornboxcfg.ui: compile with qt 3.3.5

2005-10-03 08:37 +0000 [r466677]  dfaure

	* branches/KDE/3.4/kdepim/libkdepim/addresseelineedit.cpp: Fix
	  redundant commas when pasting multiline text like a, b, c My wife
	  often copy/pastes from a mail which has such a list of email
	  addresses, and all commas ended up doubled.

2005-10-05 13:48 +0000 [r467522]  coolo

	* branches/KDE/3.4/kdepim/kdepim.lsm: 3.4.3

