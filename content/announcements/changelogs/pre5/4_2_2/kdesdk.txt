------------------------------------------------------------------------
r934412 | repinc | 2009-03-03 00:40:59 +0000 (Tue, 03 Mar 2009) | 2 lines

Synced with trunk

------------------------------------------------------------------------
r936464 | shaforo | 2009-03-07 17:55:09 +0000 (Sat, 07 Mar 2009) | 6 lines

congratulations, Azamat: you've managed to reach short int limit.
bumping it to 2^31-1 now

BUG: 186452


------------------------------------------------------------------------
r937029 | lueck | 2009-03-08 21:47:27 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r937238 | scripty | 2009-03-09 08:23:15 +0000 (Mon, 09 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r937295 | mlaurent | 2009-03-09 12:31:19 +0000 (Mon, 09 Mar 2009) | 2 lines

Backport : fix mem leak

------------------------------------------------------------------------
r937628 | scripty | 2009-03-10 07:43:45 +0000 (Tue, 10 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r938543 | scripty | 2009-03-12 07:46:10 +0000 (Thu, 12 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r938663 | shaforo | 2009-03-12 15:56:53 +0000 (Thu, 12 Mar 2009) | 3 lines

forward-port plural form display fix


------------------------------------------------------------------------
r939194 | scripty | 2009-03-14 08:01:39 +0000 (Sat, 14 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r939718 | neundorf | 2009-03-15 15:19:54 +0000 (Sun, 15 Mar 2009) | 5 lines

-backport from trunk: kdepimlibs is only necessary for kbugbuster and kdeaccounts-plugin

Alex


------------------------------------------------------------------------
r940206 | shaforo | 2009-03-16 21:09:00 +0000 (Mon, 16 Mar 2009) | 5 lines

Qt 4.5 bug workaround. It must be fixed upstream when KDE 4.3 arrives so no changes in trunk.
BUG:187219



------------------------------------------------------------------------
r940737 | scripty | 2009-03-18 08:18:15 +0000 (Wed, 18 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r941236 | scripty | 2009-03-19 07:47:34 +0000 (Thu, 19 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r942137 | scripty | 2009-03-21 08:16:53 +0000 (Sat, 21 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r942592 | scripty | 2009-03-22 07:42:45 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943009 | scripty | 2009-03-23 07:45:35 +0000 (Mon, 23 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943414 | pino | 2009-03-23 20:02:23 +0000 (Mon, 23 Mar 2009) | 3 lines

make sure to extract messages from all the .ui files
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r943676 | scripty | 2009-03-24 08:19:57 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944066 | kkofler | 2009-03-24 23:50:41 +0000 (Tue, 24 Mar 2009) | 5 lines

This should fix all remaining saving related bugs and save on exit annoyances.
Tested on Fedora 9, KDE 4.2.1.

Backport revision 938008 by bruggie from trunk.
CCMAIL: bruggie@gmail.com
------------------------------------------------------------------------
r944068 | kkofler | 2009-03-24 23:55:43 +0000 (Tue, 24 Mar 2009) | 3 lines

CCBUG: 139209 This is the best approach to fixing this bug. Thanks to Olivier Trichet for supplying the patch 2 years and almost 3 months ago :(. Sorry for this incredible long time.

Backport revision 938013 by bruggie from trunk.
------------------------------------------------------------------------
r944069 | kkofler | 2009-03-24 23:59:01 +0000 (Tue, 24 Mar 2009) | 3 lines

CCBUG: 186828 Fixed by copying some code from Qt and adapting it to my use case. I should not store pointers in Q3ValueList, this way it is not calling the operator< i defined but it did pointer comparison, that is why the order was so funky. Thanks David for reporting the bug to me on IRC.

Backport revision 938418 by bruggie from trunk.
------------------------------------------------------------------------
r944070 | kkofler | 2009-03-25 00:01:45 +0000 (Wed, 25 Mar 2009) | 4 lines

Fix debug areas in these files to have them output to the right area instead of to default.

Backport revision 938424 by bruggie from trunk.
CCMAIL: bruggie@gmail.com
------------------------------------------------------------------------
r944072 | kkofler | 2009-03-25 00:15:41 +0000 (Wed, 25 Mar 2009) | 2 lines

KUrl::path() -> KUrl::toLocalFile()
Backport Kompare portion of revision 941971 by chehrlic from trunk.
------------------------------------------------------------------------
r944077 | kkofler | 2009-03-25 00:29:11 +0000 (Wed, 25 Mar 2009) | 1 line

Bump Kompare version from 3.5.3 to 3.5.4.
------------------------------------------------------------------------
r944689 | woebbe | 2009-03-25 22:48:02 +0000 (Wed, 25 Mar 2009) | 1 line

bump version number
------------------------------------------------------------------------
r944809 | scripty | 2009-03-26 08:12:46 +0000 (Thu, 26 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
