2006-06-01 18:48 +0000 [r547338]  mueller

	* branches/arts/1.5/arts/flow/gsl/gslmagic.c: fix false error check

2006-06-07 10:08 +0000 [r549058]  mueller

	* branches/arts/1.5/arts/soundserver/artswrapper.c,
	  branches/arts/1.5/arts/soundserver/crashhandler.cc: add return
	  value checks for set*uid() functions

2006-07-23 13:46 +0000 [r565442]  coolo

	* branches/arts/1.5/arts/configure.in.in: preparing KDE 3.5.4

2006-07-23 13:56 +0000 [r565454]  coolo

	* branches/arts/1.5/arts/arts.lsm: preparing KDE 3.5.4

