#authors: Carsten Niehaus, Harald Sitter
#licence: Public Domain
#date: Started on Friday, 4th August 2006

if ARGV.length < 3
    puts " Error, not enough arguments"
    puts " Please use this syntax: create_changelog.rb REV1 REV2 [BRANCHVERSION]\n"
    puts " Example:\n"
    puts "\tcreate_changelog 123456 134567 4.1\n"
    puts " to create a changelog of KDE 4.1 between the revisions 123456 and 134567."

    exit 1
end

require 'rexml/document'

RVERSION  = "4.1.2" #release version
DATE      = Time.now.utc.strftime("%Y%m%d")
REVISION1 = ARGV[0]
REVISION2 = ARGV[1]
BVERSION  = ARGV[2]

@modules = {}

MODULE_BLACKLIST = []

TAGS = ["FEATURE", "IMPROVEMENT", "OPTIMIZE", "BUG"]

#alphabetical order please
NAMES = {
    "kaddressbook" => "KAddressbook",
    "kdiamond"     => "KDiamond",
    "khtml"        => "KHTML",
    "kio"          => "KIO",
    "kinfocenter"  => "KInfoCenter",
    "kmail"        => "KMail",
    "krdc"         => "KRDC",
}
#alphabetical order please
HOMEPAGES = {
    "kdegames" => "games.kde.org",
    "kdepim"   => "pim.kde.org",
    "kopete"   => "kopete.kde.org",
    "okular"   => "okular.kde.org",
    "plasma"   => "plasma.kde.org",
}

puts "Getting the SVN-log of KDE #{BVERSION} between the revisions #{REVISION1} and #{REVISION2}"

#get the changelog-XML from SVN letting the OS do the work
svn = `svn log -v --xml -r#{REVISION1}:#{REVISION2} svn://anonsvn.kde.org/home/kde/branches/KDE/#{BVERSION}`

puts "Parsing the log"

#parse the XML
doc = REXML::Document.new svn

#traverse each log::logentry-Node of the xml
doc.elements.each("log/logentry")  do |element|
	#retrieve the revision from the same-called attribute
	revision = element.attributes["revision"]

	#get the path for retrieving KDE-Module and project out of it afterwards
    subject = element.elements["paths/path"].text

	#retriev KDE-Module and project using a rep-exp on the path
	if subject =~ Regexp.new("branches\\/KDE\\/#{BVERSION}\\/([\\w-]+)\\/([\\w-]+)\\/([\\w-]+)")
		project = $2
		kdemodule = $1
        if kdemodule == "kdebase"
            project = $3
        end
	end

    #try to get proper spelling
    if NAMES[project]
        project = NAMES[project]
    elsif project
        project = project.capitalize
    end

    #skip this entry if the product is not in the whitelist. This makes
    #us skip for example playground or kdeadmin.
    next if MODULE_BLACKLIST.include?(kdemodule)

	#now fetch the msg-Subnode and retrieve the text
    body = element.elements["msg"].text

    #skip commits which are silent. They are not worth to be 
    #mentioned in the changelog anyway
    next if body =~ /SVN_SILENT/

	#change all xml-own chars to there &;-counterpart
    body.gsub!(/&/, "&amp;")
	body.gsub!(/</, "&lt;")
	body.gsub!(/>/, "&gt;")

    body.gsub!(/automatically merged revision [\d]*:/,'')

    type = nil
    bug  = ""
    for tag in TAGS
        next if type
        if body.include?(tag)
            type  = tag.downcase
            lines = body.split("\n")
			body  = ""
			for line in lines
                tmatch = false
                for t in ["BUG", "FEATURE"]
                    if line.include?(t)
                        if line.split(t)[1] =~ /\d/
                            bug = line.split(t)[1]
                            bug.gsub!(/\D/,'')
                            if type = "bug"
                                type = "bugfix"
                            end
                            tmatch = true
                        end
                    end
                end
                if not tmatch                   \
                and not line.include?(tag)      \
                and not line.include?("CCMAIL:") \
                and not line.include?("GUI:")   \
                and not line.empty?
                    body += "    " + line + "\n"
                end
            end
        end
    end

    next unless type and body != "" #leave here if no type matched or body corrupted

    #strip initial \n
	if body.start_with?("    \n")
		body = body.reverse.chomp!("\n    ").reverse
	end

	#add trailing period
	unless body.end_with?(".")
		body = body.chomp!() + ".\n"
	end

    #only create the new hash and array if not already existing
	@modules[kdemodule] = {} unless @modules[kdemodule]
	@modules[kdemodule][project] = [] unless @modules[kdemodule][project]
	
    #save the information retrieved in the corresponding hash
	@modules[kdemodule][project] << {
      :type     => type,
      :revision => revision,
      :content  => body,
      :bugno    => bug
    }
end

puts "Write the retrieved information into the new XML-Format"

File.open("cl.xml", "w+") do |file|
    file << ['<?xml version="1.0"?>','<!DOCTYPE changelog SYSTEM "changelog.dtd" >','<changelog>',''].join("\n")
    file << "<release version='#{RVERSION}' date='#{DATE}'>\n"

	#traverse all the hashes of hashes and create the output
	@modules.each do |modName, modHash|
        modHomepage = "homepage='http://#{HOMEPAGES[modName]}'" if HOMEPAGES[modName]
		file << "<module name='#{modName}' #{modHomepage}>\n" unless modHash.empty?
			modHash.each do |productName, productArray|
                productHomepage = "homepage='http://#{HOMEPAGES[productName]}'" if HOMEPAGES[productName]
				file << "<product name='#{productName}' #{productHomepage}>\n"
					productArray.each do |change|
						file << "<#{change[:type]} rev='#{change[:revision]}' bugno='#{change[:bugno] unless not change[:bugno]}'>\n"
						file << "#{change[:content]}"
						file << "</#{change[:type]}>\n"
					end
				file << "</product>\n"
			end
		file << "</module>\n"
	end

	file << ['</release>','</changelog>',''].join("\n")
end

#that's all folks

