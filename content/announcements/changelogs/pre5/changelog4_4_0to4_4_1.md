---
aliases:
- ../changelog4_4_0to4_4_1
hidden: true
title: KDE 4.4.1 Changelog
---

<h2>Changes in KDE 4.4.1</h2>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_4_1/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konsole">Konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash on removing multiple key bindings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183069">183069</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1095365&amp;view=rev">1095365</a>. </li>
        <li class="bugfix ">Fix issue where history lines had extra spaces at end. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=188528">188528</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094735&amp;view=rev">1094735</a>. </li>
        <li class="bugfix ">Don't copy an empty string to the clipboard. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=188725">188725</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1095367&amp;view=rev">1095367</a>. </li>
        <li class="bugfix ">Add a quit() after a --list-profiles to correctly exit. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=192241">192241</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1096331&amp;view=rev">1096331</a>. </li>
        <li class="bugfix ">Recreate hotspots on resize to fix crashing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=199161">199161</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1095087&amp;view=rev">1095087</a>. </li>
        <li class="bugfix ">Disconnect terminal timers to avoid crashes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=208199">208199</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1095369&amp;view=rev">1095369</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when closing a window (via javascript, i.e. self-destruct) that has a JSError statusbar label. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=228255">228255</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1096006&amp;view=rev">1096006</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when switching to Plasma Netbook shell. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=221868">221868</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1096414&amp;view=rev">1096414</a>. </li>
        <li class="bugfix crash">Fix crash in walk through desktops. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=223432">223432</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1096424&amp;view=rev">1096424</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_4_1/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="knewstuff3">KNewStuff3</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">"Enter" in search line closed the dialog and search filter did not work in some cases. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227150">227150</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1090976&amp;view=rev">1090976</a> and <a href="http://websvn.kde.org/?rev=1090977&amp;view=rev">1090977</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">KIO</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">The HTTP kioslave now has much better support for Content-Disposition headers. See SVN commit <a href="http://websvn.kde.org/?rev=1094741&amp;view=rev">1094741</a>. </li>
        <li class="bugfix ">Fix KDirWatch crash when modifying files while the "file changed" dialog is shown. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=220153">220153</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=224229">224229</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=208486">208486</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=226674">226674</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=222547">222547</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1088537&amp;view=rev">1088537</a> and <a href="http://websvn.kde.org/?rev=1090729&amp;view=rev">1090729</a>. </li>
        <li class="bugfix ">Fix KDirWatch crash when saving over a watched file, notably in digikam. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=222974">222974</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1091941&amp;view=rev">1091941</a>. </li>
        <li class="bugfix ">Fix KDirWatch detection of a watched file being deleted and recreated immediately, the existing code was non-working and crashing when unmounting a device. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=222204">222204</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=228092">228092</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093006&amp;view=rev">1093006</a>. </li>
        <li class="bugfix ">Add missing connection which made the "change link target" feature non-working unless something else in the dialog was changed too. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=228325">228325</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1096316&amp;view=rev">1096316</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Painting optimizations for CSS Text-shadow. See SVN commits <a href="http://websvn.kde.org/?rev=1087713&amp;view=rev">1087713</a> and <a href="http://websvn.kde.org/?rev=1087753&amp;view=rev">1087753</a>. </li>
        <li class="optimize">Avoid doing any unneeded work in scrollToOffset when the offset ends up being unchanged. See SVN commit <a href="http://websvn.kde.org/?rev=1094493&amp;view=rev">1094493</a>. </li>
        <li class="optimize">Critical performance fix - avoid repainting fully the page when scrolling due to a change of behaviour of Qt 4.6. See SVN commit <a href="http://websvn.kde.org/?rev=1089160&amp;view=rev">1089160</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't allow any synchronous update of scrollbars during layout as that could potentially lead to crashes in the engine. See SVN commit <a href="http://websvn.kde.org/?rev=1088982&amp;view=rev">1088982</a>. </li>
        <li class="bugfix ">Fix printing ending up incomplete and/or with gaps in the text on some pages. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=191999">191999</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=214352">214352</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=197402">197402</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1095342&amp;view=rev">1095342</a>. </li>
        <li class="bugfix ">Fix crashes due to wrong behaviour when releasing allocated inline boxes. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=206832">206832</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=193717">193717</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094495&amp;view=rev">1094495</a>. </li>
        <li class="bugfix ">Avoid table rendering failure due to overflow of a short integer. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=219920">219920</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094490&amp;view=rev">1094490</a>. </li>
        <li class="bugfix ">Only enable the HTML 4.01 meaning of colspan/rowspan zero in standard/almost-standard mode matching Gecko engines. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227109">227109</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094488&amp;view=rev">1094488</a>. </li>
        <li class="bugfix ">rowspan="" should map to rowspan=1, not rowspan=0 avoids a crash on xmpp.org pages. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204297">204297</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094488&amp;view=rev">1094488</a>. </li>
        <li class="bugfix ">Avoid hitting an assert when drawing a dashed or dotted border arc with a radius that's smaller than or equal to the border width. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227765">227765</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094032&amp;view=rev">1094032</a>. </li>
        <li class="bugfix ">Fix scrolling while access keys are displayed producing artifacts. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172870">172870</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093042&amp;view=rev">1093042</a>. </li>
        <li class="bugfix ">Fix access keys being displayed only for the visible part of the page. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=219306">219306</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093042&amp;view=rev">1093042</a>. </li>
        <li class="bugfix ">Don't let a form widget keep a blinking cursor when the focus is changed to an external widget (e.g. the location bar.) Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=155941">155941</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093041&amp;view=rev">1093041</a>. </li>
        <li class="bugfix ">Fix access keys not being de-activable when a form element has input focus. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=210029">210029</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093040&amp;view=rev">1093040</a>. </li>
        <li class="bugfix ">Mitigate textarea selection extension problem Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=156574">156574</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093039&amp;view=rev">1093039</a>. </li>
        <li class="bugfix ">Fix arrow down keypress skipping entries in google's main search page. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=192135">192135</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093038&amp;view=rev">1093038</a>. </li>
        <li class="bugfix ">Do not force vertical scrollbar on textarea when not necessary. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=221830">221830</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1090000&amp;view=rev">1090000</a>. </li>
        <li class="bugfix ">Fix innerHTML value of script element (affects JIRA dashboard.) Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=187403">187403</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032495&amp;view=rev">1032495</a>. </li>
        <li class="bugfix ">Fix occasional crash when laying out inline flow children. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=220360">220360</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1089162&amp;view=rev">1089162</a>. </li>
        <li class="bugfix ">Fix cursor not blinking in LineEdit form widget with Qt 4.6.1+. See SVN commit <a href="http://websvn.kde.org/?rev=1089161&amp;view=rev">1089161</a>. </li>
      </ul>
      </div>
      <h4><a name="kjs">kjs</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix extremely inefficient regular expressions causing javascript failures or slowness (affects slashdot.org). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=191736">191736</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1089179&amp;view=rev">1089179</a> and <a href="http://websvn.kde.org/?rev=1094496&amp;view=rev">1094496</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_4_1/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="analog-clock applet">analog-clock applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Time was at 12 at start, settings were not kept, painting problem at start. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=226969">226969</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1092156&amp;view=rev">1092156</a>. </li>
      </ul>
      </div>
      <h4><a name="luna applet">luna applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Remove luna background. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203413">203413</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1087881&amp;view=rev">1087881</a>. </li>
      </ul>
      </div>
        <h4><a name="picture-frame applet">picture-frame applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Crash in slideshow mode when the dir had no pictures. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=226898">226898</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1091033&amp;view=rev">1091033</a>. </li>
      </ul>
      </div>
      <h4><a name="unit-converter applet">unit-converter applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Sort the combobox. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=198115">198115</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1091625&amp;view=rev">1091625</a>. </li>
      </ul>
      </div>
        <h4><a name="virus wallpaper">virus wallpaper</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make "Apply" button be active after selecting a wallpaper. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227140">227140</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1090870&amp;view=rev">1090870</a>. </li>
      </ul>
      </div>
        <h4><a name="converter runner">converter runner</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Display most common units in results. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204292">204292</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1091644&amp;view=rev">1091644</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_4_1/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/kgeography" name="kgeography">kgeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix typo in Guyana map. See SVN commit <a href="http://websvn.kde.org/?rev=1092358&amp;view=rev">1092358</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/kstars" name="kstars">kstars</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Shortcuts to zoom in Main toolbar are now working. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=226159">226159</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1090668&amp;view=rev">1090668</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_4_1/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Close annotation windows on document change/reload. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=224191">224191</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093333&amp;view=rev">1093333</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_4_1/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Sometimes decryption of files never gave a result. See SVN commit <a href="http://websvn.kde.org/?rev=1094090&amp;view=rev">1094090</a>. </li>
        <li class="bugfix ">Deleting keys might have left some stale references that would cause problems on next try. See SVN commit <a href="http://websvn.kde.org/?rev=1094449&amp;view=rev">1094449</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_4_1/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://userbase.kde.org/Kmail" name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix regression that caused long delays when sending mails. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=219687">219687</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1092906&amp;view=rev">1092906</a>, <a href="http://websvn.kde.org/?rev=1092907&amp;view=rev">1092907</a>, <a href="http://websvn.kde.org/?rev=1094467&amp;view=rev">1094467</a> and <a href="http://websvn.kde.org/?rev=1092911&amp;view=rev">1092911</a>. </li>
        <li class="bugfix ">Don't send STARTTLS when not advertised by the server, when connecting to Sieve. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=212951">212951</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1092921&amp;view=rev">1092921</a>. </li>
        <li class="bugfix ">Disable Nepomuk error dialogs at startup, which sometimes were shown by mistake. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=218206">218206</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=217111">217111</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1093976&amp;view=rev">1093976</a>. </li>
        <li class="bugfix ">Fix speed regression in the filter editor dialog, switching filters there was slow. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=224037">224037</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1094501&amp;view=rev">1094501</a>. </li>
      </ul>
      </div>
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Disable calendars containing only the wrong alarm types for the calendar type. See SVN commits <a href="http://websvn.kde.org/?rev=1093838&amp;view=rev">1093838</a> and <a href="http://websvn.kde.org/?rev=1093903&amp;view=rev">1093903</a>. </li>
      </ul>
      </div>
    </div>