------------------------------------------------------------------------
r1107865 | scripty | 2010-03-27 15:34:18 +1300 (Sat, 27 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1108606 | tokoe | 2010-03-30 01:22:50 +1300 (Tue, 30 Mar 2010) | 2 lines

Backport of bugfix #232576

------------------------------------------------------------------------
r1108687 | smartins | 2010-03-30 04:29:25 +1300 (Tue, 30 Mar 2010) | 10 lines

Forwardport r1107557 by smartins from e35 to branch 4.4:

addIncidence() correctly handles the case when there are two incidences with the same UID, it generates a new UID and stores the original one with setSchedulingID(). (Common case is when a user has a 
subfolder for viewing a friend's agenda, and that agenda contains an event organized by us, so it'll be on our calendar too, and we'll see them both).

But, fromKMailDelIncidence() wasn't aware of this so deleted the wrong incidence, now it will use the previously generated UID to make the deletion.


Fixes kolab/issue4012

------------------------------------------------------------------------
r1108836 | mlaurent | 2010-03-30 10:49:19 +1300 (Tue, 30 Mar 2010) | 4 lines

Start to fix bug #230502
(Remove info that we can't add in a journal)
CCBUG: 230502

------------------------------------------------------------------------
r1108997 | smartins | 2010-03-30 23:15:51 +1300 (Tue, 30 Mar 2010) | 8 lines

Forwardport r1107772 by smartins from e35 to branch 4.4:

Trunk is very different, and I can't create subfolders in korganizer yet.

Don't crash when creating folders with invalid characters in korganizer's resource view.

Fixes kolab/issue4271

------------------------------------------------------------------------
r1109155 | smartins | 2010-03-31 06:32:21 +1300 (Wed, 31 Mar 2010) | 8 lines

Backport r1109127 by smartins from trunk to branch 4.4:

When converting a todo to XML, don't add a 00h time component if the todo floats.

Fixes kolab/issue4280.

MERGE: 4.4, trunk

------------------------------------------------------------------------
r1109310 | smartins | 2010-03-31 13:26:38 +1300 (Wed, 31 Mar 2010) | 6 lines

Don't crash when cutting the parent to-do, instead, make the children independent before cutting.

Better would be asking the user if he wants to cut everything or only the parent, but wont do this for branch 4.4

MERGE: trunk

------------------------------------------------------------------------
r1109474 | mlaurent | 2010-03-31 20:28:39 +1300 (Wed, 31 Mar 2010) | 3 lines

Fix Bug 230502 -  Wrong information layout when printing a journal entry
BUG: 230502

------------------------------------------------------------------------
r1109534 | smartins | 2010-04-01 01:09:53 +1300 (Thu, 01 Apr 2010) | 8 lines

Fwdport r1109520 by smartins from e35 to branch 4.4:

Don't crash when trying to reply after saving a message.

Fixes kolab/issue3099.

BUG: 216981

------------------------------------------------------------------------
r1110178 | mlaurent | 2010-04-02 21:01:48 +1300 (Fri, 02 Apr 2010) | 2 lines

Disable preview action when we don't have a preview component

------------------------------------------------------------------------
r1110180 | mlaurent | 2010-04-02 21:14:59 +1300 (Fri, 02 Apr 2010) | 2 lines

Disable print preview when we don't have print preview component

------------------------------------------------------------------------
r1110188 | mlaurent | 2010-04-02 22:13:27 +1300 (Fri, 02 Apr 2010) | 2 lines

Fix bug #233010

------------------------------------------------------------------------
r1110218 | mlaurent | 2010-04-03 01:58:37 +1300 (Sat, 03 Apr 2010) | 4 lines

Backport:
Fix crash
MERGE: e5/e4/3.5 ?

------------------------------------------------------------------------
r1110429 | scripty | 2010-04-03 14:51:38 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110784 | scripty | 2010-04-04 13:46:54 +1200 (Sun, 04 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1111193 | smartins | 2010-04-05 14:27:45 +1200 (Mon, 05 Apr 2010) | 15 lines

Backport r1111192 by smartins from trunk to 4.4:

SVN_MERGE:
Merged revisions 1110114 via svnmerge from 
svn+ssh://smartins@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim

........
  r1110114 | smartins | 2010-04-01 18:57:29 -0400 (Thu, 01 Apr 2010) | 5 lines
  
  
  Added some comments to calendarview's mNavigatorBar.
  
  MERGE: trunk
........

------------------------------------------------------------------------
r1111737 | tmcguire | 2010-04-07 03:23:20 +1200 (Wed, 07 Apr 2010) | 12 lines

Forwardport r1102400 by winterz from trunk to the KDE 4.4 branch:

fix a crash when deleting a folder in the following test case:
1. Create a subfolder x2 of x1.
2. Copy all mails form x1 to x2.
3. Sync and while syncing delete folder x1
kolab/issue3902

MERGE: 4.4
(Thomas, can you check if this is needed in trunk?)


------------------------------------------------------------------------
r1111739 | tmcguire | 2010-04-07 03:27:55 +1200 (Wed, 07 Apr 2010) | 9 lines

Forwardport r1106702 by winterz from trunk to the KDE 4.4 branch:

in ldapSearchResult(), use KPIM::splitEmailAddrList() to split the list
of addresses instead of just splitting on the comma character.
this is better because addresses can be of the form "last, first".
part of the fix for kolab/issue4149
MERGE: trunk,4.4


------------------------------------------------------------------------
r1111742 | tmcguire | 2010-04-07 03:34:53 +1200 (Wed, 07 Apr 2010) | 8 lines

Forwardport r1106707 by winterz from trunk to the KDE 4.4 branch:

in selectedEMails(), quote the names returned from the search as needed.
kolab/issue4149
MERGE: trunk,4.4



------------------------------------------------------------------------
r1111752 | tmcguire | 2010-04-07 03:54:18 +1200 (Wed, 07 Apr 2010) | 7 lines

Forwardport r1106714 by winterz from trunk to the KDE 4.4 branch:

Change the text on the button that starts the address selector
from "..." to "Select...". and give it an accelerator too.
MERGE: trunk,4.4


------------------------------------------------------------------------
r1111915 | scripty | 2010-04-07 13:44:33 +1200 (Wed, 07 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1112068 | smartins | 2010-04-07 20:54:49 +1200 (Wed, 07 Apr 2010) | 10 lines

Backport r1112066 by smartins from trunk to branch 4.4:

My first commit only fixed to-do to xml conversion.
This fixes xml to to-do, mFloatingStatus is set now.
Uses same method as event.cpp for handling allday vs time.

Fixes kolab/issue4280.



------------------------------------------------------------------------
r1112102 | smartins | 2010-04-07 23:21:06 +1200 (Wed, 07 Apr 2010) | 6 lines

Backport r1112101 by smartins from trunk to branch 4.4:

allday journals weren't working, they always appeared at 00h

MERGE: 4.4

------------------------------------------------------------------------
r1112326 | djarvie | 2010-04-08 09:24:42 +1200 (Thu, 08 Apr 2010) | 2 lines

Bug 232353: Fix audio files playing silently when no volume level has been specified.

------------------------------------------------------------------------
r1113677 | tokoe | 2010-04-12 01:31:49 +1200 (Mon, 12 Apr 2010) | 2 lines

Backport of bugfix #234032

------------------------------------------------------------------------
r1113745 | djarvie | 2010-04-12 04:34:01 +1200 (Mon, 12 Apr 2010) | 2 lines

Workaround for Xine backend clipping end off playback

------------------------------------------------------------------------
r1114962 | scripty | 2010-04-15 13:42:40 +1200 (Thu, 15 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115091 | mlaurent | 2010-04-15 20:21:11 +1200 (Thu, 15 Apr 2010) | 3 lines

Fix bug #233880
Disable Help button until doc will created.

------------------------------------------------------------------------
r1115602 | scripty | 2010-04-17 13:43:11 +1200 (Sat, 17 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1116348 | mlaurent | 2010-04-19 21:12:21 +1200 (Mon, 19 Apr 2010) | 2 lines

Fix bug #233880, we don't have help for it => remove help button in this dialogbox

------------------------------------------------------------------------
r1117336 | scripty | 2010-04-22 09:38:36 +1200 (Thu, 22 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117837 | mlaurent | 2010-04-23 20:23:52 +1200 (Fri, 23 Apr 2010) | 5 lines

Disable "delete attachment" in tree view when we can't do it (as in readonly folder)
(found on my kdab account).
Will forward port to trunk
MERGE: e3/e4

------------------------------------------------------------------------
r1117846 | mlaurent | 2010-04-23 20:40:21 +1200 (Fri, 23 Apr 2010) | 4 lines

Not necessary to show "Show all attachment" action when we don't have attachments
MERGE: e3/e4
Will merge in trunk if necessary

------------------------------------------------------------------------
r1117847 | mlaurent | 2010-04-23 20:51:55 +1200 (Fri, 23 Apr 2010) | 4 lines

Disable "Save All Attachment" when we don't have select attachment
MERGE: e3/e4
Will merge in trunk if necessary

------------------------------------------------------------------------
r1117850 | mlaurent | 2010-04-23 20:56:35 +1200 (Fri, 23 Apr 2010) | 2 lines

Revert last commit it was not good

------------------------------------------------------------------------
r1117870 | mlaurent | 2010-04-23 21:52:57 +1200 (Fri, 23 Apr 2010) | 4 lines

Disable "Move to trash" action when we can't do it (readonly folder)
MERGE: e3/e4
Will merge in trunk if necessary/possible

------------------------------------------------------------------------
r1117885 | mlaurent | 2010-04-23 22:45:39 +1200 (Fri, 23 Apr 2010) | 2 lines

Remove duplicate 'copy_to' entry in Messages menu

------------------------------------------------------------------------
r1117898 | mlaurent | 2010-04-24 00:05:29 +1200 (Sat, 24 Apr 2010) | 2 lines

Add separator, to be consistent with kmreaderwin menu

------------------------------------------------------------------------
r1117916 | mlaurent | 2010-04-24 00:45:07 +1200 (Sat, 24 Apr 2010) | 4 lines

Don't allow to move messages when we are in readonly folder
MERGE: e3/e4
Not necessary to merge in trunk

------------------------------------------------------------------------
r1117927 | mlaurent | 2010-04-24 01:16:12 +1200 (Sat, 24 Apr 2010) | 2 lines

forgot to commit it

------------------------------------------------------------------------
r1117952 | mlaurent | 2010-04-24 01:58:26 +1200 (Sat, 24 Apr 2010) | 3 lines

Fix bug #210411  Favorite folders don't show new mail in brackets
BUG: 210411

------------------------------------------------------------------------
r1118225 | scripty | 2010-04-24 14:04:03 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118539 | scripty | 2010-04-25 13:58:29 +1200 (Sun, 25 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118622 | mlaurent | 2010-04-26 00:49:34 +1200 (Mon, 26 Apr 2010) | 3 lines

Fix bug # 224862 " trash folder setting is ignored with imap"
BUG: 224862

------------------------------------------------------------------------
r1118625 | winterz | 2010-04-26 01:42:38 +1200 (Mon, 26 Apr 2010) | 6 lines

backport SVN commit 1115753 by winterz:

Do not truncate start/endtime of truncated events in KOrganizer's print styles.
Patch provided by Reinhold.
http://reviewboard.kde.org/r/3625

------------------------------------------------------------------------
r1120051 | nlecureuil | 2010-04-28 21:08:39 +1200 (Wed, 28 Apr 2010) | 4 lines

Backport commit 1120050
CCBUG:230599


------------------------------------------------------------------------
r1120091 | nlecureuil | 2010-04-28 22:51:40 +1200 (Wed, 28 Apr 2010) | 2 lines

SVN_SILENT Use 2 spaces instead of 4

------------------------------------------------------------------------
r1120172 | wstephens | 2010-04-29 02:46:41 +1200 (Thu, 29 Apr 2010) | 21 lines

Backport ContactGroupSearchJob changes from trunk and
NepomukFeederAgentBase fixes enabling successful feeding
of contact groups into Nepomuk and their querying later
by kmail when expanding distribution lists.

Add setLimit(1) on the group search.

Also backport prerequisite changes
including #include fixes, ItemSearchJob::akonadiItemIdUri() and
ContactGroupSearchJob::setLimit()

Backported commits to NepomukFeederAgentBase:
1095503
1109633
1109634

This does not allow selecting distribution lists in the recipients
picker, but they can be entered by name in the To:, Cc: etc fields.

CCBUG: 223034

------------------------------------------------------------------------
r1120604 | cgiboudeaux | 2010-04-30 00:26:05 +1200 (Fri, 30 Apr 2010) | 1 line

Bump versions.
------------------------------------------------------------------------
