commit 3ae6ae390f6df7c4f98e4a50fa1db0ef6a58e03c
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Jun 2 19:23:43 2011 +0200

    bump version

commit 70f21938c24df1ac02f9b5bbbe7030a6a98838ca
Author: Vishesh Handa <handa.vish@gmail.com>
Date:   Wed May 25 23:49:11 2011 +0530

    Fix Search excerpts
    
    This new Literal Term never added the search variable via QBD::addFullTextSearchTerm, and therefore search excerpts were never queried.
    
    I've only added the terms which we query via bif:contains(), not the regex ones.

commit 465ec0c76958657cdb75616f87d4e160f8615bcb
Author: Matthias Fuchs <mat69@gmx.net>
Date:   Sat May 28 01:17:40 2011 +0200

    Disallows names of files containing '/' or being equal to "." or "..".
    CCBUG:211751
    REVIEW:101456
    (cherry picked from commit def6e86db16246e12660d6ba66f607a9ea6d9a58)

commit dd4ff99421fc0aa96d74bc616e64137c11862f20
Author: Frank Reininghaus <frank78ac@googlemail.com>
Date:   Sat May 28 17:00:50 2011 +0200

    Fix crash in KDirOperator::Private::_k_slotExpandToUrl(...)
    
    When entering '/' in the 'Name' line of the file open dialog, pressing
    backspace and then pasting 'a/b', where a is a subfolder of the current
    folder and b a file in that folder, KDirOperator may crash because it
    accesses a null KFileItem. This commit fixes the crash. Unit test
    included.
    BUG: 187066
    FIXED-IN: 4.6.4
    (cherry picked from commit 8b5c38c8d9fae85e002fae9f325277b5b200d44d)

commit 8d885c97b483b21ee13b6bf9539a1cb7e529102a
Author: Frank Reininghaus <frank78ac@googlemail.com>
Date:   Sun May 29 15:41:14 2011 +0200

    Fix possible crash in KDirModel if the root item is deleted or moved
    
    If the root item of the dir model is deleted, but it is not the first
    item in the list that KDirModelPrivate::_k_slotDeleteItems(...) gets
    from the dir lister, a crash may result because
    KDirModelPrivate::removeFromNodeHash(...) calls isDir() for a null
    KFileItem. This commit extends the protection agains this kind of crash
    that has been introduced in c8939409eed00420fb43ff22cfc6c9092e4da7e5 for
    the first item to the rest of the list.
    BUG: 196695
    FIXED-IN: 4.6.4
    (cherry picked from commit 83538b4339a65c90764975f01a4b9bafbabd9595)

commit aa705f2a16d437aad24df8846476bcc5e44c73bd
Author: Script Kiddy <scripty@kde.org>
Date:   Sun May 29 17:37:01 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit ac6790ebf207c566b14a9041974d60df7741d415
Author: Matthias Fuchs <mat69@gmx.net>
Date:   Tue May 24 19:53:51 2011 +0200

    Ignore dots at the beginning of a file name when suggesting a file name.
    
    This ensures that ".aFile.tar.gz" becomes ".aFile 1.tar.gz" instead of " 1.aFile.tar.gz".
    REVIEW:101429
    (cherry picked from commit f2a6626f9fc1a5ed07d7115ef79d05273d8522b7)

commit 143a2d7b8e3f720a8535191a50c520268a8f3195
Author: Nicolas Lécureuil <neoclust.kde@free.fr>
Date:   Sun May 22 22:19:38 2011 +0200

    Add check on corona to avoid crash
    BUG: 264844

commit ae257e38c3dc090216346d7b787a951f26e1b8f3
Author: Stephen Kelly <stevire@gmail.com>
Date:   Tue May 17 22:27:48 2011 +0200

    Fix the handling of drops in the identity proxy model.
    
    Ported from akonadi_next version.
    
    BUG: 273485

commit ee1b344a1c8d2de5be5ae3ab7a661e49f2148ce9
Author: Burkhard Lück <lueck@hube-lueck.de>
Date:   Tue May 17 21:59:36 2011 +0200

    add help button + action to Spell Checking Configuration dialog lanched from apps (kmail, konqui etc)
    CCBUG:213817
    REVIEW:101373
    (cherry picked from commit dc3f5b9228f474e0d693564751e2c432af2c9ea7)

commit 6f6a4933975e85ebbee2c3354096c1c3a4889a3f
Author: Maks Orlovich <maksim@kde.org>
Date:   Tue May 17 08:34:37 2011 -0400

    Fix cloning of checkboxes.
    
    ... And with it element-cloning method in jQuery 1.5, which was
    making us hit IE-specific path due to the bug. Showed up in particular
    on Doodle wizard, and kde-look thumbs.
    
    CCBUG:269175
    BUG:272208

commit 92db24adfa941003db1d885df01157056617f30b
Author: Maks Orlovich <maksim@kde.org>
Date:   Sun May 8 14:39:03 2011 -0400

    Fix the job-on-hold reuse logic, which caused the double-POST problem)
    
    adawit, could you please at least READ what you're backporting if you are
    going to be this aggressive? Or better yet, please don't backport anything
    that's not fixing a critical bug or is trivial, as per:
    http://techbase.kde.org/Policies/Minor_Point_Release_Policy
    
    CCMAIL: adawit@kde.org
    BUG: 272466

commit 67957a46156d67d0039d9043fba0c0ab75cad692
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun May 15 21:09:13 2011 +0200

    unbreak KGlobalSettings::createApplicationPalette()
    
    BUG: 272184
    FIXED-IN: 4.6.4
    
    (cherry picked from commit 7e4d0322aa8952664c79cf95d7d467605b740f87)

commit 80bfc3439530aad9f29eba07d71b190f08aa1509
Author: Burkhard Lück <lueck@hube-lueck.de>
Date:   Thu May 12 22:29:13 2011 +0200

    make shortcut key names translated, reviewed by tsdgeos

commit 1984d840d250d52406837a677f718537908145ba
Author: Kevin Kofler <kevin.kofler@chello.at>
Date:   Mon May 2 00:20:22 2011 +0200

    Solid: Also mask out SSE3 and SSE4 when the SSE OS support check failed.

commit f759907f8e6a76185a286ac17a99e50fd186aff5
Author: Kevin Kofler <kevin.kofler@chello.at>
Date:   Sun May 1 23:43:58 2011 +0200

    Solid: More efficient SSE3/SSE4 checks.
    
    This moves the SSE3 and SSE4 checks from the udev backend to the shared code,
    reusing the existing cpuid testing framework. It also avoids the overhead of
    parsing /proc/cpuinfo, which was being done every single time
    Processor::instructionSets was called (because the code was not part of the
    initialization of the cached static variable).
    
    I changed the flag internally used for Altivec from 0x1 to 0x2 to avoid overlap
    with the new x86 flags.
    
    Discussed on IRC with Lukáš Tinkl.

commit aca3bf291a59d521854e49cd0958a5c924796ca2
Author: Andreas Hartmetz <ahartmetz@gmail.com>
Date:   Mon May 9 14:17:06 2011 +0200

    Plasma Wallpaper: Fix targetSize updating logic.
    
    As discussed with Aaron a few weeks ago on IRC.
    (cherry picked from commit 09c6e09534bcd6622908a6d7002a84f0f8ac8095)

commit 428a1ab8629230fea32da1c65a23dc5a42caeca2
Author: Sebastian Trueg <trueg@kde.org>
Date:   Mon May 9 17:42:46 2011 +0200

    Fixed ComparisonTermPrivate::equals.
    
    Members introduced during the last releases were not taken into account
    when comparing two instances of ComparisonTerm.

commit 76e53125d22db9151c345fa1d3e3b467eab512af
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun May 8 23:10:28 2011 -0400

    Properly rename a file when it conflicts with an existing file during a move
    operation. Fixes the remaining issue in bug# 256650.
    
    BUG: 256650
    FIXED-IN: 4.6.4

commit 1ca33c08620d22a5e00e97fda79dd9bb7574e8ed
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat May 7 22:11:30 2011 -0400

    Workaround QtWebKit's incorrect handling redirection of a POST operation.
    See http://webkit.org/b/60440 for the details.
    
    CCBUG: 269694

commit ebd556b0435d390f01383f159877834dc675409a
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat May 7 15:31:53 2011 -0400

    Ensure HTTP headers from kio_http are always set regardless of error condition.

commit 38eefc85fa7182f89e7dc7905d41e745c275fce4
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat May 7 15:26:02 2011 -0400

    Do not send content-type header on the subsequent GET request that occurs as a
    result of a POST operation redirection.
    
    Fixes bug for khtml. For kdewebkit, the fix needs to be done upstream and as
    such as ticket has been opened there. See http://webkit.org/b/60440.
    
    BUG: 269694
    FIXED-IN: 4.6.4

commit e672faca8d22f97c5246a9aae5475b73bcd75ea0
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri May 6 17:27:09 2011 -0300

    Fill in vendor and model names using Solid's udev backend when possible.
    
    REVIEW: 101270
    (cherry picked from commit fdf93b29ffd000b1b45b4d49b8ad3651c85c173f)

commit a944a0972db03bd81c076ae42d1a29ccf321ffb3
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri May 6 11:40:33 2011 +0200

    set the images/ dir to be required depending on being a "full" package or a single file

commit a8d16682c31ef523ffebba6e19283a19cd5f5627
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri May 6 15:19:09 2011 +0200

    use a QWeakPointer on the KIconLoader passed in as there are no lifetime guarantees
    
    usually KGlobal::iconLoader() is used, so this isn't an issue seen very often.
    however, when a local KIconLoader is created, it is easy to get QIcons with a
    KIconEngine that has a bad KIconLoader pointer in them. particularly as QIcon
    is implicitly shared and easily passed around. the StatusNotifier Plasma DataEngine
    was triggering this, though it would be trivial to run into this problem again
    anytime a KIconLoader is created locally
    
    thankfully, QWeakPointer does the job and is very fast and light. (confirmed
    both with my own testing and confirmation from Thiago).
    
    massive thanks to Michael Pyne for detecting the cause of the problem via Valgrind.
    
    BUG:258706

commit 3d2d3998c493a72b052eeec70d07285f27982b86
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed May 4 20:58:00 2011 -0400

    When the server responds with a '204 No Content' status, disregard any
    content-type header it may have included in the response header.
    
    Make KRun treat a KIO::ERR_NO_CONTENT error message as an indicator that
    no further action needs to be taken instead of an actual error.
    
    BUG: 30667
    FIXED-IN: 4.6.4

commit f7612de0460514975071971936b861f34cc652cb
Author: Albert Astals Cid <aacid@kde.org>
Date:   Wed May 4 19:42:47 2011 +0100

    Parameters need to be a number for i18ncp to work
    (cherry picked from commit 15a02e74312003bfe64b09a542082451b0c0cf13)

commit f79aacfcb658a57d9b7785018dfe4f970e524644
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed May 4 14:29:54 2011 -0400

    By default support only NTLMv1 like other browsers. However, make it possible
    for users to manually enable the usage of NTLMv2 authentication by adding a
    "EnableNTLMv2Auth=true" flag to either one of the following to configuration files:
    
    $KDEHOME/share/config/kioslaverc OR
    $KDEHOME/share/config/kio_httprc
    
    Note this setting like every other ioslave setting can be set on a per site
    basis by adding the parameter to a host specific section instead of the global
    one. For example,
    
    [foo.bar]
    EnableNTLMv2Auth=true
    
    BUG: 232626
    FIXED-IN: 4.6.4

commit 36bf56dd909c4e195821f3265840d5c8a3079eb4
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed May 4 13:27:05 2011 -0400

    Re-read the remote chosen by the user when configuration change notification
    is received.
    
    BUG: 144046
    FIXED-IN: 4.6.4

commit 20d033f1b818d23e0e91bd17b95079bc48243070
Author: Script Kiddy <scripty@kde.org>
Date:   Wed May 4 15:57:32 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 1cf98e860829f0163a40c8300ffabc9fa60e979a
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue May 3 09:38:26 2011 -0400

    Fixed logic problem: when Force_V1 is set, do not try NTLM v2 becaue there is a target info.

commit 6531e99661e4e4f8827bc88bac8b9a0e79fb6ce4
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue May 3 09:24:21 2011 -0400

    - Force NTLM v1 mode since KNTLM does not yet have support for "NTLM2 Session Response"
      which is different from NTLM v2. We cannot do NTLM v2 because its setup and use is
      rather complicated. See http://davenport.sourceforge.net/ntlm.html.

commit e7721a8bf35da49770a07c1ed9d1122f89024f00
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Apr 29 17:07:46 2011 -0400

    - Do not reset m_needCredentials needlessly, especially since it already gets
      reset everytime setChallenge is called.

commit c2a6b87f08d6d2fdb8b81d25075432db56097f61
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Mon May 2 11:10:25 2011 -0400

    Do not leak memory.

commit c43c55c72da354ac878cc35a643c89b5cee14419
Author: Dirk Mueller <mueller@kde.org>
Date:   Mon May 2 22:32:18 2011 +0200

    fix typo

commit ada82e514482d91eafd63880b768d014680ec778
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun May 1 13:48:09 2011 -0400

    Because KIO::Connection::disconnected emits readyRead, do not print out error
    message when we cannot read from the connection to the ioslave.
    
    BUG:264067
    FIXED-IN: 4.6.4

commit 47c4d69d1bb0e852869bc1957a96fce625149094
Author: Michel Ludwig <michel.ludwig@kdemail.net>
Date:   Sat Apr 30 13:04:52 2011 +0200

    Don't use the qobject_cast macro for detecting interface inheritance.
    
    The CodeCompletionModelControllerInterface4 interface extends the interface
    CodeCompletionModelControllerInterface3 but qobject_cast cannot detect this
    inheritance. Hence, the CALLCI macro in katecompletionwidget.cpp always uses
    the default implementation for CodeCompletionModelControllerInterface3
    instead of the potentially overridden methods that may be present in a code
    completion model implementing CodeCompletionModelControllerInterface4.
    
    Backport of Kate commit aa2f5d967ecc18f4088ad2b7f2465a51cdd19385.

commit ee83ed4fba9ac925c607856655d4351e07acd576
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 30 09:32:48 2011 +0200

    use the id() to build the config off of
    
    this falls back to objectName but uses the pluginName first. more sensible.

commit a53aac070a28c63fbe36a171df1fe1de40b73fb7
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Apr 28 21:48:43 2011 +0200

    initialize the value of applet
    
    BUG:227453
