2008-03-31 19:03 +0000 [r792278]  annma

	* branches/KDE/4.0/kdemultimedia/kmix/kmixdockwidget.cpp: patch
	  from Zajec, thanks a lot! CCBUG=156744

2008-04-16 11:13 +0000 [r797555]  mlaurent

	* branches/KDE/4.0/kdemultimedia/libkcddb/cddb.cpp: Backport: Fix
	  crash when list is empty

2008-04-18 08:28 +0000 [r798395]  mueller

	* branches/KDE/4.0/kdemultimedia/kmix/kmix.cpp,
	  branches/KDE/4.0/kdemultimedia/kmix/kmixerwidget.h: merge
	  -r786548, which also fixes a crash on exit

2008-04-27 16:25 +0000 [r801750]  mueller

	* branches/KDE/4.0/kdemultimedia/kmix/mixer.cpp: fix return value

