2007-05-25 15:09 +0000 [r668231]  mlaurent

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/encoderoutput.ui: It
	  must be a readonly qtextedit

2007-06-07 08:16 +0000 [r672460]  mlaurent

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/main.cpp: Fix mem
	  leak

2007-06-13 10:18 +0000 [r674869]  hasso

	* branches/KDE/3.5/kdemultimedia/kmix/kmix-platforms.cpp,
	  branches/KDE/3.5/kdemultimedia/kscd/libwm/plat_freebsd.c,
	  branches/KDE/3.5/kdemultimedia/oggvorbis_artsplugin/oggPlayObject_impl.h,
	  branches/KDE/3.5/kdemultimedia/mpeglib/configure.in.in,
	  branches/KDE/3.5/kdemultimedia/mpeglib/lib/util/render/dither2YUV/rgb2yuvdefs.h,
	  branches/KDE/3.5/kdemultimedia/kscd/libwm/include/wm_config.h:
	  Make kdemultimedia compile and work on DragonFly BSD.

2007-06-19 23:59 +0000 [r677818]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/googlefetcher.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/googlefetcherdialog.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/googlefetcher.h,
	  branches/KDE/3.5/kdemultimedia/juk/googlefetcherdialog.h,
	  branches/KDE/3.5/kdemultimedia/juk/pics/Makefile.am,
	  branches/KDE/3.5/kdemultimedia/juk/playlist.h,
	  branches/KDE/3.5/kdemultimedia/juk/pics/yahoo_credit.png (added):
	  Finally fix bug 116181 (Getting cover from Internet doesn't
	  work). Many patches have already been committed for this bug and
	  yet this feature still kept breaking. So I've switched from
	  screen scraping Google Images search (which is against their
	  terms of use anyways :( ) to using a API designed for the purpose
	  of searching images. The code changes are rather more extensive
	  than I like (especially for a bugfix-only branch) but the feature
	  as it stands now is completely broken and the new code is much
	  cleaner. The search uses the Yahoo Image Search web API, and
	  includes attribution of the results to Yahoo per their web API
	  usage policy. This change does not add any strings but does add a
	  .png attribution graphic. (Why didn't I use Amazon's? We (scott
	  and I) find the usage terms too restrictive) Next step is to
	  rename some of the files to something less web-search-provider
	  specific and to forward port to KDE 4. CCBUG:116181

2007-06-20 00:18 +0000 [r677822-677821]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/googlefetcher.cpp (removed),
	  branches/KDE/3.5/kdemultimedia/juk/googlefetcherdialog.cpp
	  (removed),
	  branches/KDE/3.5/kdemultimedia/juk/playlistcollection.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/webimagefetcher.cpp (added),
	  branches/KDE/3.5/kdemultimedia/juk/webimagefetcherdialog.cpp
	  (added), branches/KDE/3.5/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/googlefetcher.h (removed),
	  branches/KDE/3.5/kdemultimedia/juk/googlefetcherdialog.h
	  (removed), branches/KDE/3.5/kdemultimedia/juk/webimagefetcher.h
	  (added),
	  branches/KDE/3.5/kdemultimedia/juk/webimagefetcherdialog.h
	  (added), branches/KDE/3.5/kdemultimedia/juk/playlist.h,
	  branches/KDE/3.5/kdemultimedia/juk/covericonview.h,
	  branches/KDE/3.5/kdemultimedia/juk/Makefile.am: Change references
	  to Google to something more generic in JuK. The "googleCover"
	  action cannot be renamed to maintain DCOP compatibility.
	  CCBUG:116181

	* branches/KDE/3.5/kdemultimedia/juk/main.cpp: Bump JuK version
	  before I forget.

2007-06-20 11:01 +0000 [r677986]  jriddell

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Patch from Sune Vuorela, fix powerpc support
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdemultimedia/debian/patches/11_audiocd_no_-x_in_lame.diff?op=file&rev=0&sc=0

2007-06-21 10:43 +0000 [r678417]  orzel

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  fix previous commit by removing "^+". (patch applied in a
	  reaaaaal strange way). always test compilation before commiting.

2007-06-21 15:09 +0000 [r678504]  binner

	* branches/KDE/3.5/kdemultimedia/arts/builder/artsbuilder.desktop,
	  branches/KDE/3.5/kdemultimedia/kaboodle/kaboodle.desktop,
	  branches/KDE/3.5/kdemultimedia/kmid/kmid.desktop,
	  branches/KDE/3.5/kdemultimedia/noatun/noatun.desktop: fix invalid
	  .desktop files

2007-06-24 19:01 +0000 [r679721]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/upcomingplaylist.cpp: Fix bug
	  126032 (Last item in play queue is played twice) in KDE 3.5, will
	  commit forward port shortly. CCBUG:126032

2007-06-24 20:56 +0000 [r679762]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/playlist.cpp: Fix bug 131238
	  (Right click in History playlist shows wrong column) in KDE 3.5.
	  CCBUG:131238

2007-07-05 03:15 +0000 [r683614]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/tag.cpp: Fix bug 134312 (empty
	  flac file crashes JuK) in KDE 3.5 CCBUG:134312

2007-07-22 19:59 +0000 [r691037]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/ripconfig.ui:
	  Ripping 0 files at a time doesn't make sense

2007-08-15 11:44 +0000 [r700375]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encoder.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encoder.h: Use
	  correct EncoderPrefs, so the extension is displayed correctly
	  when changing the encoder

2007-08-18 09:37 +0000 [r701441]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/ripper.cpp:
	  Backport: Fix crash when removing jobs, don't use the iterator
	  after the item is removed (doesn't crash for me with 3.5, but I
	  think it's the cause of bug #119600, at least valgrind shows an
	  Invalid Read) BUG: 119600

2007-09-11 13:14 +0000 [r711111]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/encoder.cpp: Allow
	  creating group writable directories if umask permits it BUG:
	  144752

2007-09-18 13:54 +0000 [r714001]  lunakl

	* branches/KDE/3.5/kdemultimedia/libkcddb/kcmcddb/libkcddb.desktop:
	  I give up, enable the kcm again (bnc:254175). There's no way to
	  configure this for audiocd:/ and it's not kcontrol's fault some
	  apps roll their own cddb code.

2007-09-18 18:43 +0000 [r714096]  jriddell

	* branches/KDE/3.5/kdemultimedia/arts/runtime/artsbuilderloader_impl.cc,
	  branches/KDE/3.5/kdemultimedia/noatun/library/noatunarts/Equalizer_impl.cpp,
	  branches/KDE/3.5/kdemultimedia/mpeglib/lib/mpegplay/decoderClass.cpp,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_cdelay_impl.cc,
	  branches/KDE/3.5/kdemultimedia/noatun/library/noatunarts/FFTScopes.cpp,
	  branches/KDE/3.5/kdemultimedia/noatun/modules/winskin/vis/winSkinFFT_impl.cpp,
	  branches/KDE/3.5/kdemultimedia/mpeglib/lib/splay/mpegAudioInfo.cpp:
	  gcc 4.3 fixes

2007-10-05 09:17 +0000 [r721454]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mixer_oss.cpp: A fix for
	  issues with mutual exclusive recording sources. If setting the
	  record mask fails, try again by exclusively setting the current
	  control.

2007-10-08 11:05 +0000 [r722968]  coolo

	* branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm: updating lsm

