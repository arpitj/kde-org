2007-05-24 18:26 +0000 [r667998]  dselby

	* branches/KDE/3.5/kdeadmin/kcron/ktview.h,
	  branches/KDE/3.5/kdeadmin/kcron/ktapp.cpp,
	  branches/KDE/3.5/kdeadmin/kcron/ktview.cpp: Deselects appropreate
	  icons on app start avoiding crash. ie start kcrom without this
	  patch and click on cut - boom crash :) - this patch solves this
	  problem.

2007-05-28 10:17 +0000 [r668998]  mueller

	* branches/KDE/3.5/kdeadmin/lilo-config/common/Disks.cc,
	  branches/KDE/3.5/kdeadmin/lilo-config/common/lilo.cc,
	  branches/KDE/3.5/kdeadmin/lilo-config/common/lilo.h,
	  branches/KDE/3.5/kdeadmin/lilo-config/common/String.h: the usual
	  "daily unbreak compilation"

2007-09-18 18:33 +0000 [r714094]  jriddell

	* branches/KDE/3.5/kdeadmin/ksysv/PreferencesDialog.cpp: build fix
	  for gcc 4.3

2007-10-08 11:05 +0000 [r722960]  coolo

	* branches/KDE/3.5/kdeadmin/kdeadmin.lsm: updating lsm

