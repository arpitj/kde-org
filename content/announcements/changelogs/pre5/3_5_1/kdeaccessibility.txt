2005-12-03 22:15 +0000 [r485352]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/chat-de.xml
	  (added),
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/Makefile.am:
	  German chat filter by Jürgen Zdero.

2005-12-19 07:33 +0000 [r489584]  lueck

	* branches/KDE/3.5/kdeaccessibility/doc/kmag/index.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmag/man-kmag.1.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmouth/man-kmouth.1.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmousetool/index.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmousetool/man-kmousetool.1.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmouth/index.docbook:
	  documentation update (menus, checked guiitems, sanitized)
	  BUG:96464 BUG:112579 BUG:112580 CCMAIL:gunnar@schmi-dt.de

2005-12-19 15:13 +0000 [r489707]  mhunter

	* branches/KDE/3.5/kdeaccessibility/kmag/kmag.cpp: Typographical
	  corrections and changes CCMAIL:kde-i18n-doc@kde.org

2006-01-03 23:18 +0000 [r494041]  mueller

	* branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/player.h: avoid
	  compiler warning in a better way

2006-01-07 01:37 +0000 [r495082]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/hadifix/hadifixconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgr.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/TODO,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/filtermgr.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgr.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/talkercode.cpp:
	  BUG:118016 Not loading plugins when desktop language is not
	  ISO-8859-1

2006-01-07 01:45 +0000 [r495085]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/kttsmgr/kttsmgr.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/main.cpp: Bump
	  version to 0.3.5.1

2006-01-09 16:16 +0000 [r496062]  mueller

	* branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/speaker.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/speaker.h: remove
	  broken getrealpath() implementation

2006-01-10 22:59 +0000 [r496660]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplayer.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/akodeplayer/akodeplayer.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/gstplayer/gstreamerplayer.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/artsplayer/artsplayer.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/player.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/speaker.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/TODO,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/akodeplayer/akodeplayer.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/speaker.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplayer.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/artsplayer/artsplayer.cpp:
	  BUG:119753 plus other fixes. See ChangeLog for details.

2006-01-11 02:20 +0000 [r496706]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgrwidget.ui:
	  Restore signal/slot connections on Interruption tab. When did
	  these disappear?

2006-01-11 20:12 +0000 [r497052]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgr.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/speaker.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/TODO,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplayer.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgrwidget.ui,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgr.cpp:
	  Let users specify a custom ALSA device name for playback.

2006-01-11 21:23 +0000 [r497072]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplayer.cpp:
	  Fix memory leak.

2006-01-12 03:30 +0000 [r497149]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/speaker.cpp: Minor
	  bug fix. Pausing when popping up status in systray.

2006-01-12 20:16 +0000 [r497444]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplayer.cpp:
	  Permit easy selection of default:n ALSA device.

2006-01-14 04:38 +0000 [r497882]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/epos/eposproc.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/epos/eposconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/TODO,
	  branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/epos/eposproc.h:
	  BUG:120083 BUG:115795 Fix buffer length problem and do not
	  repeatedly start EPOS server. Also change default EPOS commands
	  to eposd and say-epos.

2006-01-14 21:26 +0000 [r498183]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/command/commandproc.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog: Fix buffer
	  length problem in Command plugin.

2006-01-15 02:30 +0000 [r498236]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/command/commandproc.cpp:
	  Fix crash clicking Test button in Command configuration dialog.

2006-01-17 23:56 +0000 [r499513]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/players/akodeplayer/akodeplayer.cpp:
	  Fix crash when using akode.

