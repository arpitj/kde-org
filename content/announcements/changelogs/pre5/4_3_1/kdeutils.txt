------------------------------------------------------------------------
r1004349 | schwarzer | 2009-07-29 21:59:55 +0000 (Wed, 29 Jul 2009) | 4 lines

typo fix

CCMAIL: kubito@gmail.com

------------------------------------------------------------------------
r1004419 | rkcosta | 2009-07-30 06:56:11 +0000 (Thu, 30 Jul 2009) | 6 lines

Backport r1004012, r1004013 and r1004014.

Drop support for the --subfolder command-line option. As a side effect, the code for creating subfolders has been moved to ExtractionDialog itself.

CCMAIL: haraldhv@stud.ntnu.no

------------------------------------------------------------------------
r1004426 | rkcosta | 2009-07-30 07:05:11 +0000 (Thu, 30 Jul 2009) | 7 lines

Backport r1004413.

Move ark.kcfg and settings.kcfgc to kerfuffle/, so it can be used by any
code that needs access to KConfigXT.

CCBUG: 201854

------------------------------------------------------------------------
r1004429 | rkcosta | 2009-07-30 07:12:27 +0000 (Thu, 30 Jul 2009) | 14 lines

Backport r1004414.

Save and remember settings in the extraction dialog.

The code to load and remember the 'open destination' and 'preserve
paths' has been moved to ExtractionDialog itself.

A new <label> tag has been included in ark.kcfgc. I don't know if this
is a string freeze violation (in this case, to fix a bug), so I'm ccing
kde-i18n-doc.

CCBUG: 201854
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r1004430 | rkcosta | 2009-07-30 07:14:11 +0000 (Thu, 30 Jul 2009) | 4 lines

Forgot to commit this part of the backport of r1004414.

CCBUG: 201854

------------------------------------------------------------------------
r1004437 | rkcosta | 2009-07-30 07:28:18 +0000 (Thu, 30 Jul 2009) | 6 lines

Backport r1004415 and r1004431.

Open the destination folder in batch extraction mode.

CCBUG: 201854

------------------------------------------------------------------------
r1004623 | rkcosta | 2009-07-30 16:05:54 +0000 (Thu, 30 Jul 2009) | 4 lines

Backport r1003372.

Only change the destination folder if the new one is valid.

------------------------------------------------------------------------
r1004628 | rkcosta | 2009-07-30 16:21:50 +0000 (Thu, 30 Jul 2009) | 4 lines

Backport r1003374 and parts of r1001726.

Use destinationFolder() instead of directly accessing m_destinationFolder and return the current directory if none has been set.

------------------------------------------------------------------------
r1004717 | rkcosta | 2009-07-30 18:56:59 +0000 (Thu, 30 Jul 2009) | 4 lines

Backport r1004715.

Write the configuration back to disk.

------------------------------------------------------------------------
r1004912 | scripty | 2009-07-31 03:16:49 +0000 (Fri, 31 Jul 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1005290 | rkcosta | 2009-07-31 18:38:49 +0000 (Fri, 31 Jul 2009) | 7 lines

Backport r1004781.

Fail when the subfolder already exists as a non-directory (eg as a file)
and warn the user about it.

CCBUG: 188926

------------------------------------------------------------------------
r1007506 | rkcosta | 2009-08-05 21:07:58 +0000 (Wed, 05 Aug 2009) | 8 lines

Backport r1006977 and r1006978.

Do not call KUrl::path() because it doesn't work the way we (wrongly) expected on Windows; we should call KUrl::pathOrUrl() or KUrl::toLocalFile() instead.

The confirmation and path creation code has also been removed, as KDirSelectDialog will always return a valid path in this context.

CCBUG: 202470

------------------------------------------------------------------------
r1009843 | darioandres | 2009-08-10 23:50:08 +0000 (Mon, 10 Aug 2009) | 10 lines

Backport to 4.3 fof:
SVN commit 1009838 by darioandres:

- Fix the usage of the custom icons as the "_mount"/"_unmount" suffixes are not
  used anymore in the current icon naming scheme

- Fix a small bug about an icon change doesn't trigger a save in the settings

CCBUG: 178824

------------------------------------------------------------------------
r1009907 | cvandonderen | 2009-08-11 08:07:22 +0000 (Tue, 11 Aug 2009) | 2 lines

Backport fix for crash on Windows when enabling logic buttons in KCalc.

------------------------------------------------------------------------
r1011807 | kossebau | 2009-08-15 21:27:42 +0000 (Sat, 15 Aug 2009) | 2 lines

backport of #1011806: fixed: possibly outdated offcursor bitmap was painted if view is unfocused

------------------------------------------------------------------------
r1011811 | kossebau | 2009-08-15 21:39:49 +0000 (Sat, 15 Aug 2009) | 1 line

update: next version is 0.3.1
------------------------------------------------------------------------
r1012074 | rkcosta | 2009-08-16 18:35:35 +0000 (Sun, 16 Aug 2009) | 11 lines

Backport r1012071.

Remember to delete AddDialog after using it.

There's nothing like forgetting that we _need_ to delete QPointers ;)
This makes KConfigGroup::sync be called and the dialog configurations be
remembered.

CCBUG: 203763


------------------------------------------------------------------------
r1012096 | rkcosta | 2009-08-16 19:34:33 +0000 (Sun, 16 Aug 2009) | 4 lines

Backport r1012086.

Remember to delete our dialog QPointers.

------------------------------------------------------------------------
r1012176 | scripty | 2009-08-17 03:02:30 +0000 (Mon, 17 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1012397 | lueck | 2009-08-17 16:01:24 +0000 (Mon, 17 Aug 2009) | 1 line

change catalog name to match export macro
------------------------------------------------------------------------
r1012454 | dakon | 2009-08-17 17:29:11 +0000 (Mon, 17 Aug 2009) | 5 lines

Don't be too strict in disabling network actions - Unknown signifies that we don't know if the network is up or down

Backport of r1010954 by wstephens


------------------------------------------------------------------------
r1012790 | scripty | 2009-08-18 03:20:44 +0000 (Tue, 18 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013165 | scripty | 2009-08-19 03:21:23 +0000 (Wed, 19 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013345 | dakon | 2009-08-19 16:20:08 +0000 (Wed, 19 Aug 2009) | 7 lines

Use proper singular/plural descriptions for context menu entries. Also adds some icons to context menu actions.

Backport of r1003238. No objections in two days on kde-i18n-doc

CCBUG:200713
CCMAIL:kde-i18n-doc@kde.org

------------------------------------------------------------------------
r1014747 | mzanetti | 2009-08-23 17:50:17 +0000 (Sun, 23 Aug 2009) | 3 lines

backport of commit 1014739


------------------------------------------------------------------------
r1014868 | scripty | 2009-08-24 03:32:21 +0000 (Mon, 24 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1016421 | dakon | 2009-08-27 19:42:19 +0000 (Thu, 27 Aug 2009) | 1 line

bump KGpg version number for release
------------------------------------------------------------------------
