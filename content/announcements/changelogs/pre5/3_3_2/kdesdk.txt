Dir: kdesdk
----------------------------
new version numbers



Dir: kdesdk/cervisia
----------------------------
backport fix of BR #92576



Dir: kdesdk/cervisia/cvsservice
----------------------------
backport fix of BR #92576



Dir: kdesdk/debian
----------------------------
Bleh.
----------------------------
More manpage updates before I sleep.
----------------------------
Updates for 3.3.1.
----------------------------
Fix source overrides (see #261435).
----------------------------
Manpages, icons and rules, oh my.
----------------------------
Added manpages for kcachegrind converters.
----------------------------
Install the example kde-buildrc.
----------------------------
Finished license/author audit.
----------------------------
First round of packaging changes for kdesdk 3.3.



Dir: kdesdk/debian/patches
----------------------------
First round of packaging changes for kdesdk 3.3.



Dir: kdesdk/kbabel/catalogmanager
----------------------------
tiny changes, big effect: kbabel compiles w/ KDE 3.2 again...



Dir: kdesdk/kbabel/commonui
----------------------------
tiny changes, big effect: kbabel compiles w/ KDE 3.2 again...
----------------------------
missing i18n()
----------------------------
Backport fix mem leak



Dir: kdesdk/kbabel/kbabel
----------------------------
suspend DCOP client when handling DCOP call

BUG: 90096



Dir: kdesdk/kbugbuster/gui
----------------------------
Bug 90398: KBugBuster: Spelling error in description when app is running.



Dir: kdesdk/kbugbuster/kresources
----------------------------
messages target missing :(



Dir: kdesdk/kompare/komparepart
----------------------------
don't delay the scrolling of the listviews, because they need to be scrolled to their new position before the connectwidget paints itself. need to forwardport this to HEAD and komapre3_branch.
BUG: 85744



Dir: kdesdk/kompare/libdialogpages
----------------------------
missing i18n()



Dir: kdesdk/poxml
----------------------------
correctly descape literals
BUG: 90112



Dir: kdesdk/scripts
----------------------------
add cvsforwardport



Dir: kdesdk/umbrello
----------------------------
Update version number for KDE 3.3.2



Dir: kdesdk/umbrello/umbrello
----------------------------
Backport of bugfix #77645
