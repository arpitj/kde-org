------------------------------------------------------------------------
r1030495 | mueller | 2009-10-02 11:46:41 +0000 (Fri, 02 Oct 2009) | 2 lines

bump version

------------------------------------------------------------------------
r1031269 | adawit | 2009-10-04 14:38:55 +0000 (Sun, 04 Oct 2009) | 2 lines

Always send the KioError attribute regardless of error type.

------------------------------------------------------------------------
r1031638 | lunakl | 2009-10-05 16:59:11 +0000 (Mon, 05 Oct 2009) | 3 lines

Make the option for not checking ksycoca at startup actually work.


------------------------------------------------------------------------
r1031746 | dfaure | 2009-10-05 21:45:13 +0000 (Mon, 05 Oct 2009) | 3 lines

Backport for kde 4.3.3
CCBUG: 208418

------------------------------------------------------------------------
r1031766 | adawit | 2009-10-05 23:15:51 +0000 (Mon, 05 Oct 2009) | 3 lines

- Corrected the use of QNetworkRequest::User to KIO::AccessManager::MetaData.
- Renamed  KIO::AccessManager::KioError to  KIO::AccessManager::ErrorCode before it is set in stone. 

------------------------------------------------------------------------
r1031802 | adawit | 2009-10-06 06:59:50 +0000 (Tue, 06 Oct 2009) | 1 line

Revert previous renaming of KioError to ErrorCode since 4.3.2 has already been tagged.
------------------------------------------------------------------------
r1031935 | berendsen | 2009-10-06 13:26:45 +0000 (Tue, 06 Oct 2009) | 1 line

forgot tenor clef
------------------------------------------------------------------------
r1032185 | dfaure | 2009-10-07 08:36:48 +0000 (Wed, 07 Oct 2009) | 7 lines

Backport r1030572: Use the -same- mutex for KLocale and KLocalizedString, since they both call each other.
This fixes a deadlock in KLocale. My fault for forgetting to backport the fix for KDE 4.3.2 :-(
BUG: 209712

Packagers, please include this fix in 4.3.2 packages.
CCMAIL: kde-packager@kde.org

------------------------------------------------------------------------
r1032192 | dfaure | 2009-10-07 09:19:16 +0000 (Wed, 07 Oct 2009) | 2 lines

backport: add missing license header

------------------------------------------------------------------------
r1032366 | dfaure | 2009-10-07 16:16:09 +0000 (Wed, 07 Oct 2009) | 7 lines

Backport r1028147, which fixes 204139:
Sort list of mimetypes before randomly picking one (when we don't have the file content to find out more).
This way, we get a deterministic result rather than depending on qHash(QString),
and as a nice side effect, *.doc defaults to application/msword rather than text/plain, which
restores the kde3 and shared-mime-info behavior (despite our addition of "*.doc
can be text/plain for local files", compared to shared-mime-info).

------------------------------------------------------------------------
r1032497 | dfaure | 2009-10-07 23:30:07 +0000 (Wed, 07 Oct 2009) | 2 lines

Backport fix for "copy" in frames (#187403)

------------------------------------------------------------------------
r1032707 | ilic | 2009-10-08 09:39:18 +0000 (Thu, 08 Oct 2009) | 1 line

i18n: filtering message.
------------------------------------------------------------------------
r1032787 | dfaure | 2009-10-08 14:04:22 +0000 (Thu, 08 Oct 2009) | 6 lines

Backport fix for 207173/209876: Hide the whole widget, not just the view and the
viewport. Otherwise the findbar stays around, receives focus, and this makes the
partmanager activate the part that we're currently deleting...
Forgot to do this before 4.3.2, so the fix will be in 4.3.3 :(
CCBUG: 207173

------------------------------------------------------------------------
r1032809 | dfaure | 2009-10-08 15:13:19 +0000 (Thu, 08 Oct 2009) | 4 lines

Backport r1032808, fix for bug 203069:
Allow to focus readonly form elements which makes it possible to copy their text, too.
On the other hand, don't allow Ctrl+X and Ctrl+V to work in such widgets.

------------------------------------------------------------------------
r1032895 | lunakl | 2009-10-08 20:27:45 +0000 (Thu, 08 Oct 2009) | 5 lines

backport r1032892
kdeglobals no longer stores Desktop and Document paths,
they are accessed using QDesktopServices/xdg-user-dirs


------------------------------------------------------------------------
r1032896 | lunakl | 2009-10-08 20:27:53 +0000 (Thu, 08 Oct 2009) | 5 lines

backport r1032892
kdeglobals no longer stores Desktop and Document paths,
they are accessed using QDesktopServices/xdg-user-dirs


------------------------------------------------------------------------
r1033582 | ossi | 2009-10-10 14:12:01 +0000 (Sat, 10 Oct 2009) | 18 lines

change the way global settings are propagated

instead of KGlobalSettings' c'tor propagating them automatically,
introduce an activate() method which must be called explicitly (usually
by KApplication's c'tor).

this undoes gof's change to skip applying the settings if qApp is no
KApplication - it is unnecessary now.

while this is an incompatible change of (undocumented) behavior, the
collateral damage expected from it is zero.

this is necessary to fix a bug in kdm (currently the most frequently
reported one), so committing to the stable branch.

CCMAIL: faure@kde.org, ogoffart@kde.org


------------------------------------------------------------------------
r1033587 | ossi | 2009-10-10 14:19:26 +0000 (Sat, 10 Oct 2009) | 2 lines

fix mem leak and crash

------------------------------------------------------------------------
r1033708 | ilic | 2009-10-10 21:01:52 +0000 (Sat, 10 Oct 2009) | 1 line

New version of compiled pmaps, with loading time O(entries) rather than O(entries*properties). (bport: 1033706)
------------------------------------------------------------------------
r1033735 | dhaumann | 2009-10-10 22:14:36 +0000 (Sat, 10 Oct 2009) | 7 lines

backport SVN commit 1033721 by pletourn:

Don't change the indentation mode in the middle
of changing the highlighting mode

CCBUG:200989

------------------------------------------------------------------------
r1033737 | dhaumann | 2009-10-10 22:16:53 +0000 (Sat, 10 Oct 2009) | 6 lines

backport SVN commit 1032077 by pletourn:

Take into account horizontal scrolling

CCBUG:209636

------------------------------------------------------------------------
r1033947 | orlovich | 2009-10-11 16:34:37 +0000 (Sun, 11 Oct 2009) | 6 lines

Don't lose imagelike object's on reattach.
This hack isn't needed anyway since a RenderImage is a 
RenderReplaced, which doesn't permit kids.

BUG:210147

------------------------------------------------------------------------
r1033984 | orlovich | 2009-10-11 17:35:30 +0000 (Sun, 11 Oct 2009) | 14 lines

Don't crash if the widget gets destroyed directly,
which happens a lot in apps like kopete and dikigam
(This does not directly destroy the view as it's not 
actually a child object)

@packagers:
If any 4.3.x packages include 1032787 --- which looks like 
some do --- then this change should also be included.

Thanks to Dario Andres for pointing out the bug to me.
BUG:209960
CCMAIL: kde-packager@kde.org


------------------------------------------------------------------------
r1034391 | yurchor | 2009-10-12 15:27:09 +0000 (Mon, 12 Oct 2009) | 1 line

Some updates
------------------------------------------------------------------------
r1034403 | segato | 2009-10-12 16:02:32 +0000 (Mon, 12 Oct 2009) | 3 lines

backport of commit 1034399
remove the lib suffix only with msvc otherwise mingw fails to load some plugins like khtml, since every mingw libraries and plugins have the lib prefix

------------------------------------------------------------------------
r1034844 | winterz | 2009-10-13 17:11:13 +0000 (Tue, 13 Oct 2009) | 3 lines

include <locale.h>
not sure why this wasn't needed before.. perhaps because I'm building against Qt4.6 now??

------------------------------------------------------------------------
r1034864 | winterz | 2009-10-13 19:36:10 +0000 (Tue, 13 Oct 2009) | 3 lines

define -DQT_GUI_LIB for Qt4.6


------------------------------------------------------------------------
r1035437 | aseigo | 2009-10-15 02:12:58 +0000 (Thu, 15 Oct 2009) | 3 lines

if the script fails to initialize, then delete the script plugin and clean up after ourselves. should help prevent scriptengines from ending up in a bad state
BUG:209697

------------------------------------------------------------------------
r1035438 | aseigo | 2009-10-15 02:35:26 +0000 (Thu, 15 Oct 2009) | 4 lines

* lump all actions over the max at the end
* don't use magic numbers but the enum
* don't leak icon items when the action is destroyed

------------------------------------------------------------------------
r1035439 | aseigo | 2009-10-15 02:36:58 +0000 (Thu, 15 Oct 2009) | 2 lines

backport svg object leakage fixes from trunk where they've been for a couple months without harm

------------------------------------------------------------------------
r1035523 | dfaure | 2009-10-15 09:43:11 +0000 (Thu, 15 Oct 2009) | 4 lines

Backport: Don't show an error message in KDirOperator::Private::openUrl when the user types a HTTP url;
we just skip listing the directory (but KFileWidget still has to set the url in KDirOperator
since it's used later). BUG 197945

------------------------------------------------------------------------
r1035539 | dfaure | 2009-10-15 10:14:38 +0000 (Thu, 15 Oct 2009) | 2 lines

Backport 1035538: Restrict XMLHttpRequest so that it only talks to http, https or webdav

------------------------------------------------------------------------
r1035593 | dfaure | 2009-10-15 12:56:47 +0000 (Thu, 15 Oct 2009) | 2 lines

Fix invokeBrowser for the case where the configured webbrowser is a command with multiple arguments. BUG 210529.

------------------------------------------------------------------------
r1035706 | dfaure | 2009-10-15 17:32:50 +0000 (Thu, 15 Oct 2009) | 2 lines

I forgot to remove the kFatal, despite the TODO next to it :-) Sorry Andras ;)

------------------------------------------------------------------------
r1035728 | aacid | 2009-10-15 18:36:57 +0000 (Thu, 15 Oct 2009) | 3 lines

honor the confirm overwrite flag
acked by dfaure

------------------------------------------------------------------------
r1036221 | helio | 2009-10-16 20:43:44 +0000 (Fri, 16 Oct 2009) | 8 lines

- Anonymous is a valid user, even in the situation that server doesn't allow
anonymous login, so we should honor the checkbox choice, validating url as
anoymous@ instead of previous user name set.
- User login field was not editable, so if a typo is made in a premade url
ftp://<user>@<host>, user had the only option to cancel and type again. 
- Checkbox for keep passwd should be disabled as well in anonymous option


------------------------------------------------------------------------
r1036731 | trueg | 2009-10-17 17:24:44 +0000 (Sat, 17 Oct 2009) | 1 line

Backport: default to nfo:FileDataObject for file urls
------------------------------------------------------------------------
r1036775 | dfaure | 2009-10-17 19:22:18 +0000 (Sat, 17 Oct 2009) | 2 lines

Backport compilation fix HAVE_GETGROUPLIST

------------------------------------------------------------------------
r1036907 | trueg | 2009-10-18 08:15:03 +0000 (Sun, 18 Oct 2009) | 1 line

compile++
------------------------------------------------------------------------
r1037102 | dhaumann | 2009-10-18 12:06:35 +0000 (Sun, 18 Oct 2009) | 7 lines

backport SVN commit 1036862 by pletourn:

Render the tabstops non-antialiased
Look as good as before and much faster (on X11)

CCBUG:170322

------------------------------------------------------------------------
r1037255 | aacid | 2009-10-18 18:01:45 +0000 (Sun, 18 Oct 2009) | 3 lines

Make sure we return the names of the correct translators
BUGS: 210530

------------------------------------------------------------------------
r1037439 | dafre | 2009-10-19 06:26:52 +0000 (Mon, 19 Oct 2009) | 2 lines

Backport: fixing QNetworkReply::downloadProgress signal and making it consistent with QNetworkAccessManager's one

------------------------------------------------------------------------
r1038148 | aseigo | 2009-10-20 17:56:08 +0000 (Tue, 20 Oct 2009) | 3 lines

backport fix for BR211217
BUG:211217

------------------------------------------------------------------------
r1038801 | dhaumann | 2009-10-21 21:24:44 +0000 (Wed, 21 Oct 2009) | 6 lines

Makefiles always use tabs -> add predefined mode:
kate: tab-width 8; indent-width 8; replace-tabs off; replace-tabs-save off;

Far too much bla bla in the bug report. Why don't you just fix it. Moo.
CCBUG:163220

------------------------------------------------------------------------
r1039098 | dhaumann | 2009-10-22 18:23:54 +0000 (Thu, 22 Oct 2009) | 6 lines

backport SVN commit 1038816 by pletourn:

Don't crash

BUG:183691

------------------------------------------------------------------------
r1039102 | kossebau | 2009-10-22 18:38:35 +0000 (Thu, 22 Oct 2009) | 2 lines

backport of 1039099: changed: emit signal mouseMiddleClick() on release, not press of the mouse button (fixes bug 188587)

------------------------------------------------------------------------
r1039321 | dhaumann | 2009-10-23 09:03:09 +0000 (Fri, 23 Oct 2009) | 1 line

try to fix compile
------------------------------------------------------------------------
r1039910 | mpyne | 2009-10-24 22:06:48 +0000 (Sat, 24 Oct 2009) | 9 lines

Backport compile fix for Cygwin usage of posix_madvise to KDE 4.3.3.

Note: This code was wrong but happened to work on non-Cygwin, this is not a
Cygwin-specific change.

Patch provided, and issue reported by yselkowitz.

CCBUG:195262

------------------------------------------------------------------------
r1040312 | scripty | 2009-10-26 04:16:06 +0000 (Mon, 26 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1040869 | jferrer | 2009-10-26 23:04:14 +0000 (Mon, 26 Oct 2009) | 2 lines

Added a new translator (Manuel Tortosa)

------------------------------------------------------------------------
r1041528 | scripty | 2009-10-28 04:27:24 +0000 (Wed, 28 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1041878 | aseigo | 2009-10-28 18:30:27 +0000 (Wed, 28 Oct 2009) | 2 lines

backport fix to call init in all runner plugins, not just scripted ones

------------------------------------------------------------------------
r1041987 | shaforo | 2009-10-28 22:27:19 +0000 (Wed, 28 Oct 2009) | 3 lines

backport 'use specific debug area' so i can get it for free with the next minor KDE release ;)


------------------------------------------------------------------------
r1042044 | orlovich | 2009-10-29 00:41:44 +0000 (Thu, 29 Oct 2009) | 12 lines

Better handling of the case where we get editting input events w/o a proper
caret set --- such as when facebook apparently rewrites textareas into 
content-editable divs in a middle of handling of a click.... Either
grab the caret, or drop the event. Should hopefully finally fix 
the prepareForTextInsertion crasher on facebook

BUG:189173    
BUG:211449
BUG:212138
BUG:212175


------------------------------------------------------------------------
r1042272 | orlovich | 2009-10-29 14:04:43 +0000 (Thu, 29 Oct 2009) | 4 lines

Fix unaligned accesses (since they crash on many archs).

BUG: 210574

------------------------------------------------------------------------
r1042466 | mueller | 2009-10-29 21:22:13 +0000 (Thu, 29 Oct 2009) | 2 lines

bump version

------------------------------------------------------------------------
