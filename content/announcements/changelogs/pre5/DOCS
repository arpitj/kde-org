KDE Changelogs in XML
=====================
To generate the PHP version of the docs from an XML file, use the command:
# xsltproc --stringparam outputversion "4.8.1" -o changelog4_8_0to4_8_1.php changelog.xsl changelog_branch_4_8.xml

If you only want the changes for a particular version, use:
# xsltproc --stringparam outputversion <version> -o outputfile changelog.xsl changelog_branch_3_5.xml
where <version> is the version you want.

Details on writing changes in the XML format are below:

Quick start
-----------
Here's a mini example to show the use of the XML format, if you want to get started quickly. Detailed documentation is below:

---- 8< ---- 8< ----
<changelog>
<release version="3.5.3" date="2006-04-20">

<module name="kdelibs">

<product name="KHTML" homepage="http://khtml.kde.org">
<component name="Invented Component">
	<bugfix bugno="95319 654321">Don't crash when triple-clicking on a line that has :after or :before content, or anything that isn't in the DOM.</bugfix>
	<feature bugno="123456">Support new stuff.</feature>
</component></product></module>

<module name="kdebase">
<product name="Kicker">
<bugfix rev="527108 527109">Bookmarks: Look up konsole-bookmarks in the correct directory.</bugfix>
<optimize>Load much, much faster. See <a href="http://my.blog.org">my blog</a>.</optimize>
<improvement>Better documentation and whatsthis help</improvement>
</product>
</module>
</release>
</changelog>
---- 8< ---- 8< ----

Detailed descriptions of tags
=============================

Optional attributes are in [ ]

Generalities
------------

<changelog>
Root element. Required, but does nothing interesting.

<release version="" date="">
One for each release detailed in the changelog, and the date on which it was released.

Divisions
---------

<module name="" [reponame=""]>
An SVN module. If possible, make sure the lowercase form of the "name" attribute is the exact name of the module in SVN (this makes the 'rev' attribute work - see below).

<product name="" [homepage=""] [reponame=""]>
An application or other component which forms the toplevel in an SVN module. Again, make sure that the lowercase form of the name attribute is as it appears in SVN if you want the 'rev' attribute to work. The homepage attribute is an optional URL of the product's homepage.

<component name="" [reponame=""]>
A component within a <product>.

Change types
------------
<bugfix [bugno="x y z"] [rev="x y z"] [class="crash"]>
A fix of a bug. The bug number(s)  on bugs.kde.org can be placed in the bugno attribute if appropriate, and rev can be used for the SVN revision number of any relevant commit. If multiple bugs or revisions are relevant, separate them with spaces. Use class="crash" to highlight a fixed crash.

<feature [bugno=""] [rev=""]>
A new feature. bugno and rev work as for <bugfix>.

<optimize [bugno=""] [rev=""]>
An optimization. bugno and rev work as for <bugfix>.

<improvement [bugno=""] [rev=""]>
An improvement that doesn't fit into one of the other categories, for example, updated documentation, usability or accessibility improvements, new artwork, etc. bugno and rev work as for <bugfix>.

Other things
------------

<a href="">
Just like the HTML element for making links.
