------------------------------------------------------------------------
r946277 | shaforo | 2009-03-29 00:25:39 +0000 (Sun, 29 Mar 2009) | 3 lines

match better


------------------------------------------------------------------------
r947236 | scripty | 2009-03-31 07:01:55 +0000 (Tue, 31 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r951398 | scripty | 2009-04-09 07:21:57 +0000 (Thu, 09 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r953429 | shaforo | 2009-04-13 22:42:47 +0000 (Mon, 13 Apr 2009) | 4 lines

BUG: 174310
fix in branch. trunk doesn't contain the bug since some time.


------------------------------------------------------------------------
r958331 | shaforo | 2009-04-23 19:43:01 +0000 (Thu, 23 Apr 2009) | 4 lines

another one Qt 4.5 fix.
read QScrollArea description in docs


------------------------------------------------------------------------
r958334 | aacid | 2009-04-23 20:05:16 +0000 (Thu, 23 Apr 2009) | 4 lines

Fix settings menu
Acked by apaku
BUGS: 179836

------------------------------------------------------------------------
r958342 | shaforo | 2009-04-23 20:37:14 +0000 (Thu, 23 Apr 2009) | 3 lines

layouting seems to be much cooler this way


------------------------------------------------------------------------
r959775 | scripty | 2009-04-27 07:24:26 +0000 (Mon, 27 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r960764 | kkofler | 2009-04-29 01:54:19 +0000 (Wed, 29 Apr 2009) | 1 line

Kompare: Backport yet another KUrl::path() -> KUrl::toLocalFile() fix by Christian Ehrlicher (part of trunk revision 952058).
------------------------------------------------------------------------
r960896 | scripty | 2009-04-29 08:18:18 +0000 (Wed, 29 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
