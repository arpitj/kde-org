------------------------------------------------------------------------
r1042752 | orlovich | 2009-10-30 15:21:50 +0000 (Fri, 30 Oct 2009) | 14 lines

Implement custom scrollHeight for RenderTextArea --- it needs to be 
able to look inside its scrollbar usage, so normal RenderObject/RenderLayer 
stuff is insufficient.

Fixes growing suggestion box on reviewboard.

(Though, I really do wonder why the test scrollHeight and height for inequality --- 
it'd make perfect sense for scrollHeight to be smaller than element height when one 
has enough room, and at least some Opera versions behave that way, too; so testing 
for scrollHeight > height would be better. Not that it'd help us before... but, well,
brittle code...)

BUG:199986

------------------------------------------------------------------------
r1043538 | cfeck | 2009-11-01 19:28:24 +0000 (Sun, 01 Nov 2009) | 4 lines

Use iconSize from QStyleOptionButton (backport r1043537)

CCBUG: 212643

------------------------------------------------------------------------
r1043541 | mrybczyn | 2009-11-01 19:37:04 +0000 (Sun, 01 Nov 2009) | 2 lines

Adding underLGPL in Polish

------------------------------------------------------------------------
r1043950 | trueg | 2009-11-02 18:23:44 +0000 (Mon, 02 Nov 2009) | 1 line

Backport: fixed Entity comparison opertators
------------------------------------------------------------------------
r1043977 | dfaure | 2009-11-02 19:23:38 +0000 (Mon, 02 Nov 2009) | 2 lines

backport 1024398, qt-4.6-crash-fix. Since people seem to like running old KDEs with new Qts...

------------------------------------------------------------------------
r1044096 | djarvie | 2009-11-03 01:26:24 +0000 (Tue, 03 Nov 2009) | 2 lines

Bug 212237: Allow use of zoneinfo time zones which are not in zone.tab.

------------------------------------------------------------------------
r1044278 | trueg | 2009-11-03 10:45:59 +0000 (Tue, 03 Nov 2009) | 1 line

Backport: changed link order
------------------------------------------------------------------------
r1044429 | aseigo | 2009-11-03 18:42:48 +0000 (Tue, 03 Nov 2009) | 3 lines

backport: cache whether or not the wallaper is actually a full package (dir + metadata) on render hints changing just look for the best paper if we are a full package; preserves rendering for single image papers in the wallpaper config
CCBUG:212930

------------------------------------------------------------------------
r1045272 | mikearthur | 2009-11-05 16:53:50 +0000 (Thu, 05 Nov 2009) | 2 lines

Qt Cocoa libraries can be installed in lib so look there for Phonon too.

------------------------------------------------------------------------
r1045315 | aseigo | 2009-11-05 19:12:57 +0000 (Thu, 05 Nov 2009) | 2 lines

in case this makes a difference

------------------------------------------------------------------------
r1046014 | scripty | 2009-11-07 04:19:12 +0000 (Sat, 07 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1046199 | mpyne | 2009-11-07 20:17:19 +0000 (Sat, 07 Nov 2009) | 5 lines

Backport compilation fix for glibc 2.11 (we weren't including enough headers) to
KDE 4.3.4.  Originally committed in r1045794.

BUG:213223

------------------------------------------------------------------------
r1046581 | berendsen | 2009-11-09 08:58:08 +0000 (Mon, 09 Nov 2009) | 1 line

backport update from trunk
------------------------------------------------------------------------
r1046830 | ilic | 2009-11-09 20:04:47 +0000 (Mon, 09 Nov 2009) | 1 line

More thourough coverage of casing in ijek->ek mappings. Latin mappings added. (bport: 1046829)
------------------------------------------------------------------------
r1047595 | helio | 2009-11-11 15:08:42 +0000 (Wed, 11 Nov 2009) | 1 line

- Fix compilation against API changes in openssl 1.0.
------------------------------------------------------------------------
r1047708 | djarvie | 2009-11-11 21:34:50 +0000 (Wed, 11 Nov 2009) | 1 line

apidox: add missing values
------------------------------------------------------------------------
r1048275 | dfaure | 2009-11-13 01:14:31 +0000 (Fri, 13 Nov 2009) | 5 lines

Fix porting bug (yes, even in 2009) - KMainWindow::internalStatusBar() would look for
a KStatusBar recursively, so it would find a konqueror per-view-statusbar and hide it erroneously.
That lookup is not supposed to be recursive (and wasn't in kde3).
BUG: 193072

------------------------------------------------------------------------
r1048549 | dfaure | 2009-11-13 14:18:01 +0000 (Fri, 13 Nov 2009) | 4 lines

Another porting bug: show the bookmark url in the statusbar when the mouse is over the menu item, like in kde3.
Fixed for: 4.3.4
BUG: 153960

------------------------------------------------------------------------
r1048626 | dfaure | 2009-11-13 16:33:14 +0000 (Fri, 13 Nov 2009) | 2 lines

<mat69> I have a problem with the KUrlRequester, when I set the filter after opening the dialog the filter-text changes, yet the view does not seem to update.

------------------------------------------------------------------------
r1048704 | dfaure | 2009-11-13 19:51:52 +0000 (Fri, 13 Nov 2009) | 3 lines

Backport: apply patch by Tim Fechtner to improve the apidox for session management handling in kmainwindow
(e.g. RESTORE and kRestoreMainWindows). Thanks!

------------------------------------------------------------------------
r1049147 | dhaumann | 2009-11-14 15:17:46 +0000 (Sat, 14 Nov 2009) | 5 lines

backport SVN commit 1006031 by pletourn to KDE 4.3. branch:
Don't allow to set an empty selection

CCBUG: 202083

------------------------------------------------------------------------
r1049202 | orlovich | 2009-11-14 18:59:19 +0000 (Sat, 14 Nov 2009) | 6 lines

Don't crash when ::statusBar is called after the widget destruction 
(when widget was destroyed before the part) if we never had the 
statusbar in the first place.

BUG: 214538

------------------------------------------------------------------------
r1049711 | beschow | 2009-11-15 17:56:45 +0000 (Sun, 15 Nov 2009) | 5 lines

backport SVN commit 1049708 to KDE 4.3 branch:
Always use leading whitespace of previous line in normal indentation mode.

BUG:214147
REVIEW:2138
------------------------------------------------------------------------
r1049773 | dhaumann | 2009-11-15 20:18:06 +0000 (Sun, 15 Nov 2009) | 11 lines

backport SVN commits 1026541,1024099 by pletourn:
- preserve x cursor position when moving up and down
- fix navigation with a non-wraping cursor

If you find any regressions please report asap.
CCMAIL:kwrite-devel@kde.org

CCBUG:172323
CCBUG:203339
CCBUG:130366

------------------------------------------------------------------------
r1051522 | adawit | 2009-11-19 18:14:33 +0000 (Thu, 19 Nov 2009) | 1 line

Backport of commit r1051515 from trunk
------------------------------------------------------------------------
r1052100 | mueller | 2009-11-20 19:35:00 +0000 (Fri, 20 Nov 2009) | 2 lines

fix array overrun (CVE-2009-0689)

------------------------------------------------------------------------
r1052374 | jferrer | 2009-11-21 12:04:10 +0000 (Sat, 21 Nov 2009) | 2 lines

Add new entities (partman and translator)

------------------------------------------------------------------------
r1053171 | dfaure | 2009-11-23 14:13:40 +0000 (Mon, 23 Nov 2009) | 4 lines

Fix kio_ftp downloading files as "foo.part" instead of "foo" when disabling(!) the "Mark partially uploaded files" feature.
Fixed for: 4.3.4
BUG: 204605

------------------------------------------------------------------------
r1053873 | cfeck | 2009-11-24 22:42:37 +0000 (Tue, 24 Nov 2009) | 2 lines

Fix effects on 16/24 bpp images (backport r1053871)

------------------------------------------------------------------------
r1054004 | ervin | 2009-11-25 10:17:31 +0000 (Wed, 25 Nov 2009) | 3 lines

Make sure UUIDs are always returned lower case.
(Backport of r1054000 from trunk)

------------------------------------------------------------------------
r1054454 | cfeck | 2009-11-26 02:12:39 +0000 (Thu, 26 Nov 2009) | 2 lines

Fix missing assignment from QImage::convertToFormat (backport r1054453)

------------------------------------------------------------------------
r1054914 | cfeck | 2009-11-27 07:21:16 +0000 (Fri, 27 Nov 2009) | 4 lines

Fix reading newer XCF colormapped files (backport r1054913)

CCBUG: 214384

------------------------------------------------------------------------
r1055144 | mueller | 2009-11-27 12:37:13 +0000 (Fri, 27 Nov 2009) | 2 lines

bump version

------------------------------------------------------------------------
