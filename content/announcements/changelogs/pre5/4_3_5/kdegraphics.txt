------------------------------------------------------------------------
r1056681 | mm | 2009-11-30 15:32:47 +0000 (Mon, 30 Nov 2009) | 3 lines

Use UDS_DISPLAY_NAME instead of UDS_TARGET_URL, as this seems to break less.
Use direct filename instead of the info.name

------------------------------------------------------------------------
r1056688 | mm | 2009-11-30 15:47:59 +0000 (Mon, 30 Nov 2009) | 4 lines

reviewed and backported from TRUNK
- Qt4 porting
- crash fix on "remove camera"

------------------------------------------------------------------------
r1060849 | scripty | 2009-12-10 04:16:25 +0000 (Thu, 10 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1061138 | sars | 2009-12-10 19:36:46 +0000 (Thu, 10 Dec 2009) | 1 line

Add a workaround for the epson2 preview resolution bug, in case there is no new release soon. 
------------------------------------------------------------------------
r1063169 | gateau | 2009-12-17 10:08:13 +0000 (Thu, 17 Dec 2009) | 1 line

Work around for the evil "invisible crop handles" bug
------------------------------------------------------------------------
r1064496 | scripty | 2009-12-21 04:22:33 +0000 (Mon, 21 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1065308 | pino | 2009-12-22 23:58:59 +0000 (Tue, 22 Dec 2009) | 4 lines

backport SVN commit 1041420 by aacid:

do not accept new pixmap requests if we are shutting down

------------------------------------------------------------------------
r1066856 | scripty | 2009-12-28 04:21:41 +0000 (Mon, 28 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1071266 | sars | 2010-01-07 19:34:32 +0000 (Thu, 07 Jan 2010) | 3 lines

Fix crash on unchecked pointer. 

BUG:221589
------------------------------------------------------------------------
r1072377 | scripty | 2010-01-10 04:09:55 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1075688 | sars | 2010-01-16 14:53:42 +0000 (Sat, 16 Jan 2010) | 5 lines

Workaround for bugs in backends that make them not return the amount of bytes stated in sane_get_parameters()

BUG:222274

This workaround is already present in trunk and 4.4.
------------------------------------------------------------------------
r1076369 | scripty | 2010-01-18 04:08:56 +0000 (Mon, 18 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1076809 | pino | 2010-01-18 22:00:17 +0000 (Mon, 18 Jan 2010) | 2 lines

bump version to 0.9.5

------------------------------------------------------------------------
