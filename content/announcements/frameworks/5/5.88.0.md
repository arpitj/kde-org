---
qtversion: 5.15.2
date: 2021-11-13
layout: framework
libCount: 83
---


### Attica

* Add Android CI

### BluezQt

* Add GATT Client functionality
* Generate pkgconfig file

### Breeze Icons

* Bug 404810 add flatpak packages mimetype icon
* Bug 444452 Custom folder icons
* Use reference to a QString to avoid copying, fixing a warning
* BUG: 395569 places icon use ColorScheme colors (bug 395569 places icon use ColorScheme colors)
* Add RTL logout icons
* Missing color scheme usage at some icons
* BUG: 174203 add mail-download-now and mail-download-later icon (bug 174203 add mail-download-now and mail-download-later icon)
* BUG: 436931 update anacondainstaller icon to respect fedora logo (bug 436931 update anacondainstaller icon to respect fedora logo)
* Fix the dark install directory for installing colored icons (bug 444095)
* Install colored icons sizes into breeze-dark them as well (bug 444095)
* Make some of the 22 px places icons actually be 22 again
* Add Kongress icon fix grid alignment
* Fix color of 16px im-user-offline icon (bug 443658)
* Optimize places 16 and 22px icons
* BreezeIcon additional app icons most are for math apps
* BreezeIcons add task-process icons for kalendar app task view
* Add Joplin icon
* Add syncthing icon
* Move all of /preferences to icons, remove it from icons_dark
* Fix scalable test to check breeze for breeze-dark
* Make breeze-dark inherit from breeze

### Extra CMake Modules

* Fix Wayland_DATADIR when cross-compiling
* [android] Extend NDK workaround to version 22
* msvc: Fix __cplusplus value
* Fix ECM on systems without PyQt5 on Python 2
* ECMFindQMLModule: Use QT_HOST_BINS to find qmlplugindump
* FindQtWaylandScanner: Use QT_HOST_BINS to find qtwaylandscanner
* ecm_add_test: add -DQT_FORCE_ASSERTS to compile flags by default

### KDE Doxygen Tools

* Handle uppercase directory names (bug 441560)
* Remove whitespace in generic type definitions

### KArchive

* Add Android CI
* This framework is now LGPL-2.0-or-later

### KBookmarks

* Correct platform specification - we can be built on Android as well

### KCalendarCore

* Dirty fields and update observers in Event and FreeBusy
* Dirty fields and update observers in Incidence
* Dirty updates in IncidenceBase
* Move IncidencePrivate and IncidenceBasePrivate to headers
* Simplify IncidenceBasePrivate constructors
* Rename incidence classes from Foo::Private to FooPrivate
* Copy improvements in recent changes to Event etc
* cmake: Port to using libical's cmake scripts

### KCMUtils

* KPluginSelector: Make sure to check if metaData/moduleInfo is valid before accessing it
* KPluginSelector: Allow KCMs to be loaded using X-KDE-ConfigModule keyword
* Remove unused internal method in KPluginSelector
* Deprecate KCModuleInfo in favor of KPluginMetaData API
* KCMultiDialog: Use deprecation macros to wrap internal KCModuleInfo API usage
* KCModuleLoader: Deprecate overloads using KCModuleInfo
* KPluginSelector: Port internally deprecated KCModuleInfo usage
* KCModuleProxy: Deprecate KService/KCModuleInfo overloads for constructor

### KCompletion

* New KCompletion::setSorterFunction() to allow custom sorting by providing function (bug 442717)

### KConfig

* Exclude new enum-overload from python bindings build
* Enforce KAuthorized enums being not 0
* Create enum to to authorize common keys
* Do not try to generate python bindings for KConfigGroup::moveValuesTo
* Create utility method for moving entries from one group to another

### KConfigWidgets

* Fix auto color scheme switching
* [kcolorschememodel] Expose color scheme id
* [KCommandBar] Additionally sort commands by name (bug 443731)
* [KCommandBar] Don't display an indent for an icon if no item has an icon (bug 443382)
* [KCommandBar] Skip actions with empty text (bug 443732)
* Read proper color setting on Windows (bug 443300)
* [KCommandBar] Show information about a command in a tooltip (bug 438735)

### KContacts

* Adressee: use a vector instead of a QHash with ~8 elements
* VCard: Replace QMap with a vector of struct
* Addressee: deprecate insertEmail() and add a new method instead
* VCardTool::addParamter() should take by pointer
* Deprecate parameters/setParameters() from the public API
* Deprecate the unused Field class
* Replace country <-> iso code mapping with the new KI18nLocaleData
* VCardParser: less temporary allocations
* Don't use QTime::fromString() with format being a string
* Don't use QDate/QTime::fromString() with a string format arg
* Perf: Manually parse timezone offset
* VCardTool::createVCards(): split Adressee-related code to a separate method
* VCardTool::createVCards(): split Adress-related code to a separate method

### KCoreAddons

* KPluginFactory: Provide more context for warning
* Add comments describing checks made before registering plugin
* Move utility method to read translated json values to dedicated header
* Improve deprecation docs of KAboutData::programIconName
* kcoreaddons_add_plugin: Avoid clash if app name and plugin namepace are the same
* K_PLUGIN_CLASS_WITH_JSON: Use name provided by kcoreaddons_add_plugin for factory
* Port internal readStringList usage
* Deprecate KPluginMetaData::readStringList in favor of value overload
* Discard KPluginMetaData::value overloads for python bindings
* KPluginMetaData: Add overload to interpret char array as string
* KPluginMetaData: Add overloads to read int and bool value
* merge the util's config.h

### KCrash

* Move more Linux-specific functions under #ifdef Q_OS_LINUX
* Add CMake option to build without X11
* Use imported target for X11

### KDeclarative

* GridDelegate: don't let labels overflow (bug 444707)
* Use a readonly property to control KCM margins
* [configmodule] Deprecate aboutData
* [ConfigModule] Allow creating KCMs without KAboutData
* [ConfigModule] Deprecate ctors that take KAboutData and KPluginMetaData
* Don't explicitly build shared libs
* Expose KAuthorized enum as dynamic properties to QML
* KCM GridDelegate: Display a blue line when the delegate has active focus

### KDED

* Bump KF_DISABLE_DEPRECATED_BEFORE_AND_AT value
* Utilize KPluginMetaData::value overloads

### KFileMetaData

* Fix wrong testcase
* [FFmpegExtractor] Remove FFmpeg 2.x/3.0 support, handle deprecations
* Fix version checks in FindFFmpeg.cmake
* [DublinCore] Also test terms namespace
* [PopplerExtractor] directly use getters provided by poppler

### KGlobalAccel

* Properly split exec line args (bug 444730)

### KDE GUI Addons

* Remove unneeded lambda capture, fixing a compiler warning

### KHolidays #

* Correct Name of last day of daylight saving time (bug 444615)
* Fixed typo on holiday_mx_es
* various holiday_* - assign explicit names for subregions
* holidays/holidays.qrc - remove trailing white-space
* Remove unneeded fstream include
* removed shabs
* added pakistan holidays

### KI18n

* Don't discard non-conflicting substring matches
* Fall back to the compile-time detected iso-codes location
* Use unique_ptr instead of managing memory manually
* Load Qt translations even if some catalogs are missing
* Add KCountry[Subdivision]::operator!=
* Forward declaration needs a matching export macro on Windows
* Extend README to cover the new features
* Move the new country/country subdivision/timezone code into its on library
* Add QML API for KTimeZone
* Add QML bindings for country/country subdivision API
* Fix offset overflows for ISO 3166-2 name lookups
* Integrate the Unicode normalization and prefix matching from KContacts
* Cache iso codes cache sizes
* Implement KCountry::fromName()
* Add timezone lookup by geo coordinate, and timezone to country mapping
* Make use of the spatial index for countries and subdivisions
* Generate spatial index for timezones/countries/country subdivisions
* Implement timezone by country (subdivision) lookup
* QGIS Python scripts for generating country/timezone lookup tables
* Add country and country subdivision lookup and translation API

### KIconThemes

* Add QIcon::fallbackThemeName() as fallback
* Fix @since marker
* Include a test for properly testing icon recoloring
* Also highlight charged text colors (bug 442569)
* Use KIconColors to re-color icons
* Make icon colouring a per-icon property rather than a system (bug 442533)
* Create a d-pointer for KIconEngine
* [KIconDialog] Also apply edge padding vertically
* Ensure we retrieve our dependencies on Android

### KIdleTime

* Relicense framework from LGPL-2.0-only to LGPL-2.1-or-later

### KInit

* Remove code for loading libkdeinit5_foo.so modules

### KIO

* PasteDialog: fix data pasting in Wayland (bug 421974)
* Find LibMount when building statically
* kdeinit is gone, use KDE_SLAVE_DEBUG_WAIT in kioslave instead
* [KFilePlacesModel] Specify which data roles have actually been changed
* Deduplicate KCoreDirLister before going through them
* Use CMake to check if copy_file_range() is available
* file_unix: make the read/write loop the same as the copy_file_range one
* file_unix: let copy_file_range() manage the fd's offsets
* file_unix: emit processed size change after each copy_file_range() call
* file_unix: only update sizeProcessed once per iteration
* KDiskFreeSpaceInfo: Remove redundant deprecation #if
* Deprecate KDiskFreeSpaceInfo
* fix KTerminalLauncherJob working with exec
* file_unix: fix copying status report
* file_unix: Refactor copy()
* KMountPoint: revert to parsing /dev/disk/by-{uuid,label}/ manually (bug 442106)
* CopyJob: handle the no-op case of symlinks on FAT partitions (bug 442310)
* SkipDialog: adapt buttons to more use cases
* Port to enum values for KAuthorized::authorize
* KUrlCompletion: Disregard the trailing slash when sorting directory paths (bug 442717)
* Deprecate Scheduler::publishSlaveOnHold as well
* Remove the last use klauncher in slave.cpp
* Remove cmake option KIO_FORK_SLAVES
* KFilePropsPlugin: Allow to edit properties when setFileNameReadOnly is set (bug 441847)
* Increase KF_DISABLE_DEPRECATED_BEFORE_AND_AT version
* Add easy Invent repo search
* Add invent web search keyword
* kcm_cookies: Embed json metadata
* kcm_webshortcuts: Embed json metadata
* kcmproxy: Embed json metadata
* kcmtrash: Embed json metadata and drop X-KDE-PluginKeyword usage
* scheduler: reorder to avoid the need for fwd-decl functions
* [KUrlNavigator] Fix first button text for local paths
* kuriikwsfiltereng: Fix quitting match logic if accidentally bang syntax is used (bug 437660)
* kurifilter: Add test to check if bang syntax is preferred
* Deprecate KAutoMount/KAutoUnmount
* file_unix: Fix endless loop during xattr copy (bug 441446)
* Improve "trash is full" error message (bug 442865)
* Don't use kdoctools on Android
* KDesktopPropsPlugin: open "Advanced Options" dialog with show()
* Adjust dependencies for Android

### Kirigami

* Fix missing titlebar on ApplicationWindow modals in Windows
* NavigationTabButton: Set minimum width and wrap text
* Icon: Use icon-png as the default placeholder instead of icon-x-icon (bug 444884)
* Remove the no longer existing Units.qml file from the qrc files
* Add missing AboutItem to to the qrc files
* Cleanup FormLayout
* reliably drop component pools (bug 429027)
* [GlobalDrawerActionItem] Dim when disabled
* Small improvement in Api doc
* Pass properties when creating a dialog for pushDialogLayers
* Fix context drawer being inaccessible on secondary layers in mobile mode
* [PageRow] Fix variable shadowing
* [ActionTextField] Doc: Reword nullable field to an empty list
* [Breadcrumb] Fix breadcrumb position on content change
* Improve look of FormLayout section headers
* Fix punctuation/whitespace of runtime rate-limited deprecation warnings
* Always allow sidebar handles in header by default
* remove duplicated frile
* NavigationTabBar: Support actions that have mnemonics
* Heading: Lower all sizes
* Fix issues with setting components on tablet

### KItemModels

* Take into account a possible QLocale::setDefault() override

### KJobWidgets

* Handle all cases in switch(KJob::Unit) expressions to fix a compiler warning

### KJS

* Do no longer remove -Wsuggest-override flag

### KNewStuff

* Port to enum values for KAuthorized::authorizeAction
* Make sure that we query KService with lowercase desktop names since it seems that KService stores everything lowercase (bug 417575)

### KNotification

* Fix Windows build
* warn when notifybypopup has pending notifications on destruction
* Add KNotifications QML plugin
* KWindowSystem is also not required on Android - but unlike Phonon is also needed on Windows
* Phonon is not required for KNotifications on Windows or Android

### KPackage Framework

* Allow KCM kpackages to use metadata of C++ plugin
* Utilize std::optional to check if we have already searched for a KPluginMetaData object
* KPackage: Copy KPluginMetaData object when copying d-ptr

### KParts

* Deprecate unused & internal method
* PartLoader::createPartInstanceForMimeType: Port inlined code from hack
* Increase KF_DISABLE_DEPRECATED_BEFORE_AND_AT version
* Un-Overload KParts::BrowserExtension::selectionInfo signal

### Kross

* Emit deprecation warnings for central Kross::Manager class

### KRunner

* Deprecate K_EXPORT_PLASMA_RUNNER_WITH_JSON macro in favor or K_PLUGIN_CLASS_WITH_JSON
* Deprecate RunnerManager::allowedRunners method

### KService

* Use KSERVICE_ENABLE_DEPRECATED_SINCE in header instead of BUILD variant
* cmake: Remove intermediate copy targets
* kservice.h: Silence deprecation warnings in inlined, deprecated code

### KTextEditor

* ViMode: Dont respond to doc changes when vi mode is disabled for view
* Expand katepart metadata (bug 444714)
* Set metadata when creating katepart (bug 444714)
* TextFolding::importFoldingRanges: get rid of UB (bug 444726)
* KateFoldingTest: don't leak ViewPrivate
* Simplify DocumentCursor::setPosition
* Highlight Folding Markers
* #37 Improve the context menu for selections
* Make the regex search fast
* Improve Appearance > Borders Dialog
* KateRenderer: Check for m_view being null in more places
* Fix leaving selectionByUser state
* Do not cancel mouse selection when using the keyboard
* [KateIconBorder] Handle annotations context menu
* Enable enclose selection by default for brackets
* Dont indent the lines if line already has text + noindent
* Improve Open/Save Advanced Dialog
* Better General dialogue for text editing settings
* [KateIconBorder] Add context menu

### KWayland

* Correct the eglQueryWaylandBufferWL_func prototype

### KWidgetsAddons

* KTitleWidget: Lower all sizes
* KSqueezedTextLabel: Fix clipped characters (bug 442595)
* KMessageDialog: fix setButtons() being called twice
* KMessageDialog: disconnect default buttonbox signals as we emit done() manually (bug 442332)

### KWindowSystem

* Fix the compiler warning by casting int to unsigned
* Link privately against XCB when building statically (bug 441266)

### KXMLGUI

* Migrate "State" config entry to state config group if available
* Allow apps to opt-in storing state data in separate file (bug 397602)
* [ci] Remove unused kwindowsystem dep

### ModemManagerQt

* Introduce VoiceInterface Type
* Add signals to watch for interface changes
* Fix DBus signal connection

### NetworkManagerQt

* cmake: use imported targets

### Plasma Framework

* Deprecate KServiceTypeTrader containment action loading
* Deprecate standardInternal*Info methods
* Avoid creating a Theme object for every icon fetch
* Introduce dismissOnHoverLeave property and toolTipVisibleChanged signal (bug 444142)
* Deprecate plugin export macros in favor or K_PLUGIN_CLASS_WITH_JSON
* PC3 SpinBox: improve visuals and behavior, remove drag to change value
* Deprecate parentApp parameter in PluginLoader::listAppletMetaData
* PC2 ScrollViewStyle: fix scrollbar sizing
* PC3 TextField: Make focus frame not delayed
* PC3 Slider: get rid of HoverFocusSvgItem
* PC3 CheckIndicator: only show focus visuals for visualFocus
* PC3 Slider: fix tickmark positions
* PC3 ScrollView: remove ability to show background in onCompleted
* PC3 private/RaisedbuttonBackground: disable shadow when disabled
* PC3 private/ButtonBackground: reduce opacity when disabled
* Breeze button: Use more realistic shadow
* PC3/Breeze progressbar: update style, improve implicit sizing
* Breeze line: removed built-in blank space, simplify
* Breeze button: Use solid backgrounds for raised button pressed graphics
* PC3/Breeze sliders: add solid background, handle shadow, fix pixel alignment, fix vertical groove height
* PC3/Breeze scrollbar: update style and behavior
* PC3/Breeze radiobutton: improve style
* PC3 Control: use better implicit size calculation
* Fix crash in WindowThumbnail::updatePaintNode() (bug 444015)
* ExpandableListItem: Finally fix expanded height calculation permanently (bug 443755)
* Properly discard window texture provider when thumbnail item shows only icon
* Have Containment::restore also call its parent's Applet::restore
* Simplify code by using KPluginMetaData::value overloads
* Prevent tooltips from being incorrectly dismissed (bug 434657)
* Split window texture provider from window texture node (bug 439681)
* Make tab/right move focus to default action button or expand button
* Heading: sync with Kirigami version
* Ensure context menu is PC2.Menu, and open it relative to the item
* Fix context menu opening
* Add keyboard navigation to ExpandableListItem
* ScrollView: Do not overlay scrollbars over contents (bug 443173)
* PC3 Button/ToolButton: use down instead of pressed for background graphics
* PC3 TabButton: Add focus underline to label
* PC3 TabButton: Use IconLabel, set padding from theme, show background when no tabbar
* PC3 TabBar: improve implicit sizing, support spacing, remove pointless background, use 0 highlightResizeDuration
* breeze: increase tab top/bottom margins
* breeze: fix wrong element IDs for north tab margin hints
* PC3 Slider: set implicit handle size, separate hover/focus from shadow
* breeze: update slider style (bug 355889)
* PC3 private/IconLabel: simplify properties, add convenience display properties
* PC3 TextField/lineedit.svg: Add focus frame for visualFocus
* breeze: update button style
* breeze: update checkbox and radiobutton style (bug 355889)
* Use onPositionChanged instead of onContainsMouseChanged in ExpandableListItem
* Corona: save after ending edit mode
* Heading: Lower all sizes
* Add scroll support to calendar component in plasma frameworks
* PC3 checkbox/radiobutton/switch: left align IconLabel
* PC3 ToolTip: ceil label size
* PC3 ToolTip: fix pressed/hovered undefined error
* PC3 ToolBar: set spacing, remove pointless contentItem
* PC3 TabButton: set spacing, remove pointless background item
* PC3 DialogButtonBox: fix property undefined error
* PC3 ComboBox: make text field use parent palette, modernize code
* PC3 ComboBox: set cursor delegate to null instead of undefined
* PC3: Use shared Icon+Label implementation
* PC3 ComboBox: account for indicator and content size better
* PlasmaCore Units: Fix sizeForLabels icon size
* PC3: improve implicit sizing
* PC3 ButtonBackground: Don't load the background we aren't using
* PC3 Button: use same check for color group as ToolButton
* PC3 button backgrounds: remove TODO for required properties

### Prison

* Decrease the preferred size for PDF417 barcodes
* Add PDF417 barcode support

### Purpose

* cmake: Use custom command instead of custom target for copy
* Set a title for JobDialog (bug 444205)
* Improve job window buttons (bug 444145)
* Fix whitespace punctuation in a warning
* email: Ensure we don't crash when there's no mailto preferredService (bug 443788)
* Make sharing via Telegram work again

### QQC2StyleBridge

* Pane: add missing Kirigami import
* Add Pane control
* Remove some id, this should improve the performance
* Do not set the palette for every component (bug 406295)
* [TextFieldContextMenu] Fix null property accesses
* [TextFieldContextMenu] Free the action function after running it
* Only re-compute the icon when it's necessary
* Don't change MenuItem's arrow's color when selected (bug 443453)
* Use KIconColors to color icons
* Implement Spellchecking using Sonnet for TextArea

### Solid

* Implement ARM specific CPU info

### Sonnet

* Build examples only when BUILD_EXAMPLES is on
* Fix applying highlighter in quick document
* data/parsetrigrams.cpp: ensure the output is deterministic
* Don't crash on null textDocument if suggestions are invoked
* Add missing qmldir
* Implement QtQuick bindings for Sonnet

### Syntax Highlighting

* ' is a valid identifier part
* Fix the colors of modified and saved lines
* cmake.xml: Recognize CMake provided modules and functions/macros
* Remove *.nix files from Bash syntax
* Add Nix highlighting
* debchangelog: add Jammy Jellyfish
* update refs to improved Dockerfile highlighting
* bash.xml: Add Exherbo script file extensions recognized as `bash`
* dockerfile.xml: Use `bash` syntax for shell form of commands
* Specify byproducts for ExternalProject_Add call
* cmake.xml: Improvements to highlighting
* Add support of YAML in Fenced Code Blocks in Markdown
* Update GNU Assmebler syntax
* merge loadhighlightingdata_p.hpp with highlightingdata_p.hpp and rename KSyntaxHighlighting::loadContextData() to HighlightingContextData::load()
* add comments and inline in the header some functions
* loading rules in 2 parts to reduce the final memory used

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
