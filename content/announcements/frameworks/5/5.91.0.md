---
qtversion: 5.15.2
date: 2022-02-13
layout: framework
libCount: 83
---


### Baloo

* Static libs linked into shared libs need -fPIC

### Breeze Icons

* start-here-kde: Make icon adaptable to dark themes (bug 416302)
* Fix icon colors to be consistent across all sizes
* Check executables exist in PATH before passing them to QProcess
* Declare namespace for kongress icon
* Fix naming of brightness icons

### Extra CMake Modules

* kde_package_app_templates: drop atime,ctime, consistently set mtime
* kde_package_app_templates: use numeric owner/group with tar
* Set the right @since version for ECMQmlModule
* Only enable -bsymbolic-functions when Qt is built in a compatible way
* Introduce ECMFindQmlModule.cmake
* Add ecm_set_deprecation_versions cmake function

### Framework Integration

* Install plugin in kf<version>
* Fix wrong porting of KNSCore::Engine::configSearchLocations (bug 448237)

### KActivitiesStats

* reduce boost

### KActivities

* Use uppercase includes
* Unharcode Qt major version

### KAuth

* Move plugins in kf<version> directory
* Correct the dependencies specified for KAuth
* Bump minimum required version of Polkit-Qt-1 to 0.112.0
* Remove Polkit-Qt build support, Polkit-Qt-1 replaced it a long time ago
* Adjust CMake code to find PolkitQt{5,6}-1

### KCalendarCore

* Revert "Honour STATUS:CANCELLED"
* Serialize container sizes as 32bit integers
* Compare timeSpec() for the due date/time, too
* Fix generate pkg file
* T12270: create an inheritance hierarchy of private types

### KCMUtils

* Add KPluginWidget::load()
* Remove assertion for KPluginMetaData param in KCModuleLoader::loadModule
* KCModuleProxy: Use std::optional to check if we have a KPluginMetaData object
* KCModuleProxy: Replace space with underscore when registering DBus service
* Switch header style to ToolBar for system settings
* kpluginmodel: Also copy plugin Id from KService to json object
* KCMUtils can be built on Windows too

### KConfig

* Introduce StandardShortcutWatcher to watch for runtime changes (bug 426656)
* Extract isNonDeletedKey() helper function
* Look for entries with common group prefix in entryMap's subrange
* KConfigPrivate::copyGroup: remove redundant entryMap lookup
* groupList: convert each group name from UTF-8 once
* Exclude deleted groups from groupList() (bug 384039)

### KConfigWidgets

* KLanguageButton: Don't insert duplicates
* KLanguageButton: Adapt to new .desktop filenames
* Change shortcuts of standard actions if standard shortcut changes (bug 426656)
* Use BUILD* deprecation wrapper for virtual method (bug 448680)
* Simplify KHamburgerMenu menu items
* Only require KAuth on Linux/FreeBSD
* Don't use KAuth on Windows

### KCoreAddons

* Search in kf<version> plugin
* kcoreaddons_target_static_plugins: Use private linking for plugin registration file
* klibexec helper to resolve libexec path relative
* KPluginMetaDataTest: Fix check for service type querying error essage
* desktopfileparser: Avoid a run-time string concatenation
* KF5CoreAddonsConfig: check desktoptojson version when cross-compiling
* desktoptojson: Further improvements to cross-compilation mode
* Fix kcoreaddons_desktop_to_json when cross-compiling
* Add KPluginMetaData::fromJsonFile()
* KPluginMetadata: store all paths as absolute ones
* KPluginMetaData::metaDataFileName: Fix broken check if we have the metaDataFileName value set
* kcoreaddons_add_plugin: Throw error when we have unparsed args
* Improve formatRelativeDateTime
* Improve error message for KPluginFactory::loadFactory
* Add missing copying of error text in KPluginFactory::instantiatePlugin

### KDAV

* Fix header installation path to include module prefix
* Use uppercase "KDAV" name as qmake identifier, to be match KF standards
* Fix pri file to also note KCoreAddons dependency
* Fix non-existing include path set in pri file

### KDeclarative

* proper position for top separator
* [KeySequenceItem] Make keySequence «non-null», «non-undefined» and «non-""»
* [KeySequenceItem] Fix code style and bump QML imports
* Make QML code depend less on the `kcm` context property
* Fix QT_NO_OPENGL build after 66c5bb0efa
* kquickcontrols: Improve accessibility in ColorButton.qml (bug 449282)
* GridDelegate: Focus on the delegate after a menu is closed
* GridDelegate: Open menu when Menu key is pressed
* Exclude epoxy in qt6 => exclude plotter class
* Deprecate KDeclarative::ConfigPropertyMap in favor of KConfigPropertyMap
* It's not necessary now as "the Frameworks that actually require
* Don't use KGlobalAccel on Windows

### KDED

* install plugins in kf<version>
* Set order of kded launching with systemd boot
* kded supports building on Windows, make sure we have our dependencies available

### KDELibs 4 Support

* Link against KF5::Auth
* Also add the KAuth dependency in the CI metadata

### KDESU

* Check executables exist in PATH before passing them to QProcess
* Port from KToolInvocation::kdeinitExecWait (deprecated) to QProcess
* kdesud: close all file descriptors > 3 on exec()

### KDocTools

* Check executables exist in PATH before passing them to QProcess
* Don't hardcode "kf5" for the catalog search path

### KFileMetaData

* make TypInfo comparison const
* Install plugins in kf<version>
* deprecate image extraction via embeddedimagedata

### KGlobalAccel

* Fix action registration and unregistration order (bug 448369)
* Check executables exist in PATH before passing them to QProcess

### KDE GUI Addons

* Add Google Maps Geo URI handler
* Add Qwant Maps to CMakeLists
* Add Qwant Maps Geo URI handler
* Add a fallback handler for the geo: URI scheme
* Install plugins in kf<version>

### KHolidays #

* Update Taiwanese holidays
* UK holiday change 2022 (Spring bank holiday) (bug 448305)

### KHTML

* fix khtml crash in wayland session
* [ci] Fix phonon dep

### KI18n

* Default initialize QVariants as such, not as a QString
* install plugins in kf<version>
* Fix warning info
* KuitSetup: fix setting classification of tags

### KIconThemes

* Check executables exist in PATH before passing them to QProcess
* Don't create a new KColorScheme for each KIconColors

### KIdleTime

* Change plugin install dir on macOS
* Install plugins in kf<version>

### KImageFormats

* Check executables exist in PATH before passing them to QProcess

### KIO

* [KFilePlacesView] Use helpEvent for teardown action tooltip
* KPropertiesDialog: only load plugins suitable for mime type of file
* Use new KLibexec to locate kioslave5
* Consider slow files as remote files in previewjob (bug 372849)
* [KFilePlacesView] Elide section header if needed
* exec line is not necessary now
* [desktopexecparser] Consider associations from mimeapps.list to determine whether an app supports a scheme (bug 439132)
* [kopenwithdialog] Fix filtering (bug 449330)
* [KFilePlacesView] Adjust entire delegate height during (dis)appear animation
* kdirmodel: Allow using full path for Icon in .desktop file (bug 448596)
* Deprecate class KNTLM & methods, no users known outside of kio-http
* [previewjob] Don't recreate same regex
* KDirOperator: on first show, don't display files in dir-only mode
* Group configure_file and kcoreaddons_add_plugin
* Install plugins in kf<version>
* Officially deprecate kcoredirlister methods that use QRegExp
* Utilize KPluginMetaData::value overloads
* Install kio_version.h in the KIO include prefix
* Add option of using DBus activation for ApplicationLauncherJob
* [KFilePlacesView] Don't highlight delegate when hovering header area
* [KFilePlacesView] Call teardownFunction from inline eject button, too
* [KFilePlacesView] Don't hide hidden entries when clicking one
* [KFilePlacesView] Add inline eject button
* [KFilePlacesModel] Introduce TeardownAllowedRole
* [KFilePlacesView] Wire up QAbstractItemView::iconSize
* [KFilePlacesView] Add getter and change signal for showAll
* [KFilePlacesView] Allow to provide a teardown function
* [KFilePlacesView] Add middle click support and signals for item activation
* [KFilePlacesView] Refactor context menu
* file_unix: Do not try to preserve ownership when permission is -1 (bug 447779)
* KPropertiesDialog: use the KFileItem when checkig if the url is local (bug 444624)
* Deprecate the "connected slave" feature

### Kirigami

* OverlaySheet: Accommodate the left and right padding
* OverlaySheet: Initialise the view with sound values
* FormLayout: Replace heuristic approach with an instanceof check
* Fix install plugin in kf<version>
* libkirigami: Port to target_sources and other target-specific properties
* Disable BUILD_EXAMPLES/TESTING when BUILD_SHARED_LIBS is off
* Add Chip Component
* Add android related files to template
* Add BasicListItem.iconSelected property and use it in CheckableListItem
* Take in count difference in bottom and top padding in search icon
* API dox: document CamelCase include headers
* KF5Kirigami2: install headers to path prefix matching C++ namespace Kirigami
* Introduce "InputMethod" singleton and supporting code to detect virtual keyboard
* Properly document ShadowedRectangle and co
* Deprecate TabBar toolbar style
* Dialog: Smoothen enter/exit animation, and ensure themes don't add footer background
* Improve search icon inside SearchField
* Give the NavigationTabBar component an ocean style
* Add a search icon to the SearchField
* template: remove title from AboutPage
* examples/PageRouter: don't introduce applicationWindow id
* Make enter/key presses "click" BasicListItems
* Improve default template
* Dialog: Use performant shadow
* Use version-less/non-deprecated install location variables
* Fix find_dependency
* change color when card is highlighted
* Kirigami supports Windows, ensure we have our dependencies there too

### KItemModels

* Rework removeParentMappings() to not  rely on stable QHash iterators
* Don't cache end iterator when modifying the container

### KJobWidgets

* Check executables exist in PATH before passing them to QProcess

### KJS

* Revert "Install kjs_version.h in /usr/include/KF5/KJS/"

### KNewStuff

* Update code snippet for NewStuff.Action
* Register KNSCore::Entry under name "Entry" as a Q_GADGET to QML
* Consume string views before they become invalid
* Focus search field by default unless doing so would show the virtual keyboard (bug 444727)
* Deprecate KNS3::Button class in favor of KNSWidgets::Button
* Introduce KNSWidgets::Button class for better compatibility with KF6 changes
* KNSWidgets::Action add compatibility for unified entry class in KF6
* Unify how kpackageType is determined for the addons
* Button.qml Fix binding for engine property
* Button.qml: Use QML loaders for dialog component
* Fix broken usage example for NewStuff.Action
* Remove QProcess::ExitStatus parameter from signal handler parameter list if we don't need it

### KNotification

* Remove Phonon from Linux CI
* Add KStatusNotifierItem::hideAssociatedWidget()
* KStatusNotifierItem: use actions instead of Yes/No in Quit confirm dialog

### KPackage Framework

* Simplify code using KPluginMetaData::fromJsonFile
* Explicitly call KPluginMetaData::fromJsonFile when constructing metadata objects
* Fix unintended result in readKPackageTypes (bug 448625)
* Use ecm_mark_nongui_executable() for mock:// handler
* PackageJob: Use QStandardPaths::findExecutable() to find resolvers
* QueryTest: validate the install() result
* Build static libs which are linked into shared libs as PIC

### KQuickCharts

* ModelSource: Don't use QPointer for storing the model
* ModelSource: Cache minimum/maximum properties of the model

### KRunner

* search plugins in kf<version>
* Put aliased classes in "KRunner"-namespace
* Fix flickering in Application Launcher for every character typed (bug 423161)
* Port to KDEDeprecationSettings cmake function
* KF5RunnerConfig.cmake.in: Explicitly require QtGui and KCoreAddons, exclude Plasma on deprecation free builds
* Add compatibility type definitions for classes without 'Plasma' namespace
* Fixups for building without including KService/KIO/Plasma-Frameworks (bug 447908)
* Silence deprecation warning for KPluginInfo/KServiceTypeTrader in compatibility blocks

### KService

* Install kservice_version.h in KService include prefix
* Make parseLayoutNode function const
* Make "missing merge tag" error actionable

### KTextEditor

* Abort completion on view scroll on wayland
* Avoid flicker on refresh
* install plugin in kf<version>
* Use QDir::temp().filepath() instead of manually creating temp file paths
* KateModOnHdPrompt::slotDiff: Create diff file in temp folder (bug 448257)
* avoid flicker for border on size changes (bug 435361)
* Fix undo manager leaks more properly
* When using tabs, use tabs to auto indent (bug 367966 375529)
* Fix @since for aboutToSave
* Fix leaks in undo manager
* Don't scroll on select all (bug 435855)
* Fix auto-reloading files in git repositories
* Handle single statement condition blocks better (bug 385472)
* Fix cstyle for cases where there is a func in param
* Reload doc on OnDiskCreated
* Allow disabling focus frame (bug 418657)
* Fix selection scrolling from line border works only downwards (bug 419076)
* Fix camel cursor when last word is of one letter only (bug 448692)
* only execute diff in path
* only start programs in user's path
* completion: Use scored fuzzy matching
* Do proper fuzzy matching in completion (bug 401509)
* Fix undo history wrongfully restored in some cases
* Revert "Do not cancel mouse selection when using the keyboard" (bug 446189)
* don't let auto-detection overwrite user settings
* Introduce auto indent detection
* Restore undo history when document is same (bug 297883)
* Fix drag pixmap highlight sometimes does not match original text (bug 438567)
* Find: Update working range when replacing (bug 447972)
* set QClipboard::Selection for select all (bug 428099)
* Fix rash when switching between tabs while search is running (bug 445683)
* improve wording for modified on disk warnings (bug 372638)
* KTextEditor has a hard dependency on KAuth - ensure it is available
* Introduce Document::aboutToSave signal
* Fix creative QString constructions from numerical values
* Use version-less install target default arguments
* Use non-deprecated style option initialization
* Combine keys and modifiers using the | operator
* Disambiguate QStringBuilder to QByteArray conversion for hashing
* Make comparison operator const
* Don't pass a QString pointer to QString::compare
* Automatically determine iterator type
* Fix QChar conversion ambiguities

### KWallet Framework

* Fix install headers

### KWayland

* Ensure when unmapped is emitted, ::windows() will not contain unmapped window
* Don't use hard-coded versions with targets and variables
* Install pkg-config file
* Deprecate PlasmaWindowModel::requestVirtualDesktop()
* kwayland server has been moved to plasma kwayland-server since 5.73
* src/client: wrap deprecated methods in deprecation macros

### KWindowSystem

* Check executables exist in PATH before passing them to QProcess
* install plugins in kf<version>
* Avoid XKeycodeToKeysym in KKeyServer::initializeMods (bug 426684)
* Remove placeholder wayland platform plugin
* [kwindowinfo] Add support for reading _GTK_APPLICATION_ID
* Add KWindowSystem::updateStartupId(QWindow *window)

### KXMLGUI

* Check executables exist in PATH before passing them to QProcess
* Use uppercase includes
* Allow KToolBar to be in other places than MainWindow's ToolBarArea
* Fix i18n comment not being properly extracted

### ModemManagerQt

* Explicitly register QDBusObjectPath type

### NetworkManagerQt

* Don't write to QByteArray out of bounds
* Make de/serializing of the parity option symmetric
* Add support for WPA3-Enterprise 192-bit mode

### Oxygen Icons

* Check executables exist in PATH before passing them to QProcess

### Plasma Framework

* Check executables exist in PATH before passing them to QProcess
* ExpandableListItem: Deprecate custom isEnabled property and alias it to enabled (bug 449539)
* PC3 ButtonContent: Move property defaultIconSize to the top-level component
* Identify containments using X-Plasma-ContainmentType instead of service types
* Native interface always available
* ModelContextMenu: drop Accessible.role property
* Do not use Control for PC3::IconLabel (bug 445899)
* Corona::containmentForScreen: Ignore activities when an empty string is used (bug 448590)
* Change where add_feature_info(EGL) is called
* Fix detection of GLX support in QtGui
* PC3 ScrollView: set step sizes based on devicePixelRatio
* ConfigModel: Expose Roles enum to QML
* Adapt build system for building against qt6 + fix some compile errors
* Don't fallback to EGL::EGL, just don't link to EGL when it's not found
* Always sync the setPanelBehavior to wayland (bug 448373)
* PlasmaQuick::Dialog - Fix flickering issues when resizing (specially in krunner) (bug 427672)
* Always sync the setPanelBehavior to wayland (bug 426969)
* PC3 ScrollView: Avoid importing QtQuick.Controls.2
* Doc: Improve descriptions of Plasma::DataSource methods
* Don't crash when a screen gets disabled and enabled again (bug 447752)
* PC3 RadioIndicator: use radiobutton.svg for breeze-light and breeze-dark
* KWayland does not make sense outside of Linux/FreeBSD, so don't try to depend on it there
* Add Yakuake panel icon (bug 427485)
* Deprecated PlasmaExtras.ScrollArea component
* PC3 SwipeView: use longDuration for highlight animation

### Prison

* Prison supports Windows

### QQC2StyleBridge

* Avoid needlessly reading font settings
* StyleItem: emit signal on style changes
* install plugins in kf<version>
* take icon width into account
* adapt width of combobox to its content (bug 403153)
* Theme placeholderTextColor
* Make menu items taller in Tablet Mode
* Fix find_package
* Remove QStyle::State_Horizontal when it's not horizontal
* Adapt build system for building against qt6
* Slider: handle scrolling (bug 417211)
* Use upstream scroll implementations for Combobox and SpinBox
* Use metrics from Breeze for menu items (bug 447289)

### Solid

* Convert some connect to new signature style
* [UDisks2 Backend] Don't do media check for loop devices
* [upower] Properly round up battery's capacity (bug 448372)
* [UPower Backend] Check for Bluez for any unknown battery type
* [UDisks2] Ignore file systems mounted with x-gdu.hide option

### Sonnet

* Allow to install plugin in kf5 or kf6 directory

### Syntax Highlighting

* Python: fix line continuation starting with a string
* CSS: add some values and functions
* CSS: fix nested function call (bug 444506)
* Zsh: fix line-break and pattern in double bracket condition
* Bash: fix line-break in double bracket condition (bug 448126)
* Bash: fix parameter expansion replacement with extended glob
* [R] Add support for new pipe from R 4.1
* Update Stan highlighting

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
