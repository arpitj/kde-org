---
aliases:
- ../../plasma-5.6.4
changelog: 5.6.3-5.6.4
date: 2016-05-10
layout: plasma
youtube: v0TzoXhAbxg
figure:
  src: /announcements/plasma/5/5.6.0/plasma-5.6.png
  class: mt-4 text-center
asBugfix: true
---

- Make sure kcrash is initialized for discover. <a href="http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=63879411befc50bfd382d014ca2efa2cd63e0811">Commit.</a>
- Build Breeze Plymouth and Breeze Grub tars from correct branch
- [digital-clock] Fix display of seconds with certain locales. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a7a22de14c360fa5c975e0bae30fc22e4cd7cc43">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/127623">#127623</a>
