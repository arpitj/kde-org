---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE Ships KDE Applications 15.08.1
layout: application
title: KDE Ships KDE Applications 15.08.1
version: 15.08.1
---

{{% i18n_var "September 15, 2015. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../15.08.0" %}}

More than 40 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark and umbrello.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.12" %}}