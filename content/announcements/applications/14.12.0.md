---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE Ships Applications 14.12.
layout: application
title: KDE Ships KDE Applications 14.12
version: 14.12.0
---

{{% i18n_var "December 17, 2014. Today KDE released KDE Applications 14.12. This release brings new features and bug fixes to more than a hundred applications. Most of these applications are based on the KDE Development Platform 4; some have been converted to the new <a href='%[1]s'>KDE Frameworks 5</a>, a set of modularized libraries that are based on Qt5, the latest version of this popular cross-platform application framework." "https://dot.kde.org/2013/09/25/frameworks-5" %}}

{{% i18n_var "<a href='%[1]s'>Libkface</a> is new in this release; it is a library to enable face detection and face recognition in photographs." "http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html" %}}

{{% i18n_var "The release includes the first KDE Frameworks 5-based versions of <a href='%[1]s'>Kate</a> and <a href='%[2]s'>KWrite</a>, <a href='%[3]ss'>Konsole</a>, <a href='%[4]ss'>Gwenview</a>, <a href='%[5]ss'>KAlgebra</a>, <a href='%[6]s'>Kanagram</a>, <a href='%[7]s'>KHangman</a>, <a href='%[8]s'>Kig</a>, <a href='%[9]s'>Parley</a>, <a href='%[10]s'>KApptemplate</a> and <a href='%[11]s'>Okteta</a>. Some libraries are also ready for KDE Frameworks 5 use: analitza and libkeduvocdocument." "http://www.kate-editor.org" "https://www.kde.org/applications/utilities/kwrite/" "https://www.kde.org/applications/system/konsole/" "https://www.kde.org/applications/graphics/gwenview/" "http://edu.kde.org/kalgebra" "http://edu.kde.org/kanagram" "http://edu.kde.org/khangman" "http://edu.kde.org/kig" "http://edu.kde.org/parley" "https://www.kde.org/applications/development/kapptemplate/" "https://www.kde.org/applications/utilities/okteta/" %}}

{{% i18n_var "The <a href='%[1]s'>Kontact Suite</a> is now in Long Term Support in the 4.14 version while developers are using their new energy to port it to KDE Frameworks 5" "http://kontact.kde.org" %}}

Some of the new features in this release include:

+ {{% i18n_var "<a href='%[1]s'>KAlgebra</a> has a new Android version thanks to KDE Frameworks 5 and is now able to <a href='%[2]s'>print its graphs in 3D</a>" "http://edu.kde.org/kalgebra" "http://www.proli.net/2014/09/18/touching-mathematics/" %}}
+ {{% i18n_var "<a href='%[1]s'>KGeography</a> has a new map for Bihar." "http://edu.kde.org/kgeography" %}}
+ {{% i18n_var "The document viewer <a href='%[1]s'>Okular</a> now has support for latex-synctex reverse searching in dvi and some small improvements in the ePub support." "http://okular.kde.org" %}}
+ {{% i18n_var "<a href='%[1]s'>Umbrello</a> --the UML modeller-- has many new features too numerous to list here." "http://umbrello.kde.org" %}}

The April release of KDE Applications 15.04 will include many new features as well as more applications based on the modular KDE Frameworks 5.