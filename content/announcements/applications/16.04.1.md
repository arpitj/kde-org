---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE Ships KDE Applications 16.04.1
layout: application
title: KDE Ships KDE Applications 16.04.1
version: 16.04.1
---

{{% i18n_var "May 10, 2016. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../16.04.0" %}}

More than 25 recorded bugfixes include improvements to kdepim, ark, kate, dolphin. kdenlive, lokalize, spectacle, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.20" %}}