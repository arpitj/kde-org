---
aliases:
- ../announce-applications-18.04-rc
date: 2018-04-06
description: KDE Ships Applications 18.04 Release Candidate.
layout: application
release: applications-18.03.90
title: KDE Ships Release Candidate of KDE Applications 18.04
---

April 6, 2018. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

{{% i18n_var "Check the <a href='%[1]s'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release" "https://community.kde.org/Applications/18.04_Release_Notes" %}}

The KDE Applications 18.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.