---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE Ships KDE Applications 16.12.2
layout: application
title: KDE Ships KDE Applications 16.12.2
version: 16.12.2
---

{{% i18n_var "February 9, 2017. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../16.12.0" %}}

More than 20 recorded bugfixes include improvements to kdepim, dolphin, kate, kdenlive, ktouch, okular, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.29" %}}
